
# REPL


```
nix-shell -A shells.ghc --run 'ghci -icommon/src -ibackend/src'
```

```
nix-shell -A shells.ghc --run 'ghci -icommon/src -ifrontend/src'
```


# Build

```
nix-build -A ghc.backend
```

```
nix-build -A ghcjs.frontend
```
