{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}
module AdvancedQueries
  ( averageCommentsPerLink
  , subredditLinkCounts
  , subredditCommentCounts
  , doGraphs
  , mostSimilarSubreddits
  , commentPaths
  , commentChains
  ) where

import ClassyPrelude
import System.Directory
import Data.Aeson
import qualified Data.Map as Map (foldWithKey)
import qualified Data.Map.Strict as Map
import Data.GraphViz
import Data.GraphViz.Attributes.Complete
import Data.List (elemIndex, nub)
import DataAccess
import ForceConverter (toNormDirGraph)
import Snap.Snaplet.PostgresqlSimple.Opaleye
import qualified Thing.Subreddit as S

averageCommentsPerLink :: HasPostgres m => m [(Text, Float)]
averageCommentsPerLink = do
    comms <- Map.fromList <$> subredditCommentCounts
    links <- Map.fromList <$> subredditLinkCounts
    return (Map.toList $ average comms links)
  where average cs ls = Map.foldWithKey
            (\k commCount newMap ->
                    case Map.lookup k ls of
                        Nothing -> newMap
                        Just lc -> Map.insert k
                            (fromIntegral commCount / fromIntegral lc)  newMap
            ) Map.empty cs

subredditCommentCounts :: HasPostgres m => m [(Text, Int)]
subredditCommentCounts =
    query_ "select displayName, count(*) from comments \
                \right join subreddits on comments.subredditid = subreddits.id \
                \group by displayName order by count(*) desc"

subredditLinkCounts :: HasPostgres m => m [(Text, Int)]
subredditLinkCounts =
    query_ "select displayName, count(*) from links \
           \right join subreddits on links.subredditid = subreddits.id \
           \group by displayName order by count(*) desc"

-- Graph related
graphFileName :: IsString s => s
graphFileName = "graph.json"

doGraphs :: HasPostgres m => m ()
doGraphs = do
    exists <- liftIO $ doesFileExist graphFileName
    subs <- getAllSubreddits
    sims <- if exists
              then do
                contents <- readFile graphFileName
                case eitherDecode (fromStrict contents) of
                  Right s -> return s
                  Left er -> error er
              else do
                s <- commentPaths
                writeFile graphFileName (toStrict (encode s))
                return s
    let (grNodes, grEdges) = toNormDirGraph sims
    withLabels <- forM grNodes $ \(n,_) -> do
          mType <- getSubType n
          let nSubs = case find (\sub -> S.displayName sub == n) subs of
                         Nothing -> error $ "Couldn't find sub " ++ unpack n
                         Just s -> S.subscribers s
              t = fromMaybe "n/a" mType
          return (n, NodeLabel nSubs t)
    putStrLn "Making Graph"
    void $ liftIO $ createGraph (withLabels, grEdges)
    putStrLn "Done making Graph"

nodeColors :: [Text] -> Text -> Attributes
nodeColors allTypes type_ =
    case elemIndex type_ allTypes of
       Nothing -> []
       Just i -> [ Color [WC (HSV (mkHue i) 0.25 0.75) Nothing] ]
  where countTypes = length allTypes
        delta = 1 / fromIntegral countTypes
        mkHue i = fromIntegral i * delta

data NodeLabel = NodeLabel
     { subCount :: !Int
     , subType  :: !Text
     } deriving Show

createGraph :: ([(Text, NodeLabel)], [(Text,Text,el)]) -> IO ()
createGraph (grNodes, grEdges) = void $ runGraphvizCommand Sfdp dot Svg "graph.svg"
  where params = nonClusteredParams
                     { isDirected = False
                     , globalAttributes =
                          [ GraphAttrs [ Overlap (PrismOverlap Nothing)
                                       -- , StyleSheet "http://localhost:8000/graph-style.css"
                                       ]
                          --, EdgeAttrs [ Color [WC (RGBA 0 0 0 0) Nothing]]
                          , NodeAttrs [ Style [SItem Filled []] ]
                          ]
                     , fmtNode = \(_, nl) ->
                          let subsDouble = fromIntegral $ subCount nl
                              sz = sqrt subsDouble / 1000
                          in [ Height sz
                             , Width sz
                             , FontSize (max (sz * 14) 10)
                             ] ++ nodeColors allSubTypes (subType nl)
                     }
        dot = graphElemsToDot params sNodes sEdges :: DotGraph String
        sNodes = map (\(n,a) -> (unpack n, a)) grNodes
        sEdges = map (\(a,b,v) -> (unpack a, unpack b, v)) grEdges
        allSubTypes = nub $ map (subType . snd) grNodes

{-
commpathsToGraph :: CommentPaths -> IO (Gr Text Float)
commpathsToGraph sims = do
    writeFile "node-labes" (concatMap (\(t,i) -> tshow i ++ ", " ++ t ++ "\n") nodes)
    return $ mkGraph (map swap nodes) graphEdges
  where nodes = zip (Map.keys sims) [0..]
        nodeIndices = Map.fromList $ nodes
        graphEdges = Map.foldWithKey (\n ns es -> mkEdges n ns ++ es) [] sims
        mkEdges :: Text -> Map Text Float -> [LEdge Float]
        mkEdges startNode adjs = Map.foldWithKey func [] adjs
          where func :: Text -> Float -> [LEdge Float] -> [LEdge Float]
                func n v es = case mkEdge startNode n v of
                                Nothing -> es
                                Just e  -> e : es
        mkEdge :: Text -> Text -> Float -> Maybe (LEdge Float)
        mkEdge nodeA nodeB ev = (,,) <$> Map.lookup nodeA nodeIndices
                                     <*> Map.lookup nodeB nodeIndices
                                     <*> Just ev
-}

type CommentPaths = Map.Map Text (Map.Map Text Float)

mostSimilarSubreddits :: CommentPaths -> [(Text, Text)]
mostSimilarSubreddits sims = removeMaybes maybeLargestVaules
  where subWithLargestValue :: Map Text Float -> Maybe (Text, Float)
        subWithLargestValue =
            Map.foldWithKey (\sub v mSubValue ->
                                case mSubValue of
                                    Nothing -> Just (sub,v)
                                    Just (ps,pv) -> if v > pv
                                                    then Just (sub,v)
                                                    else Just (ps,pv)
                            ) Nothing
        maybeLargestVaules :: [(Text, Maybe (Text, Float))]
        maybeLargestVaules = map
             (\(sub,values) -> (sub, subWithLargestValue values)) (Map.toList sims)
        removeMaybes :: [(Text, Maybe (Text, Float))] -> [(Text, Text)]
        removeMaybes ((_, Nothing):rest) = removeMaybes rest
        removeMaybes ((sub, Just (oSub,_)):rest) = (sub, oSub) : removeMaybes rest
        removeMaybes [] = []

commentPaths :: HasPostgres m => m CommentPaths
commentPaths = reduceCommentChains reducer Map.empty

reducer :: [(Text, Text, UTCTime)] -> CommentPaths -> CommentPaths
reducer chains subSim = force $ fst $
    foldl' (\(sim, maybePrev) (_,current,_) ->
                case maybePrev of
                   Nothing -> (sim, Just current)
                   Just prev ->
                    if prev == current
                      then (sim, Just current)
                      else (mapIns sim prev current, Just current)
           ) (subSim, Nothing) chains

mapIns :: Map.Map Text (Map.Map Text Float)
       -> Text
       -> Text
       -> Map.Map Text (Map.Map Text Float)
mapIns m prevSub nextSub =
    case Map.lookup prevSub m of
        Nothing -> Map.insert prevSub (Map.singleton nextSub 1) m
        Just innerMap -> Map.insert prevSub
                 (Map.insertWith (+) nextSub 1 innerMap) m

reduceCommentChains :: HasPostgres m => ([(Text, Text, UTCTime)] -> a -> a)
                    -> a
                    -> m a
reduceCommentChains = fullyExpand 0 0
  where chunkSize = 5000
        fullyExpand :: HasPostgres m => Int -> Int
                    -> ([(Text, Text, UTCTime)] -> a -> a)
                    -> a
                    -> m a
        fullyExpand !offset !iteration func !a = do
            putStrLn $ "Iteration : " ++ tshow iteration
            results <- commentChains chunkSize offset
            case results of
                [] -> return (func results a)
                _  -> fullyExpand (offset+chunkSize) (iteration+1) func
                                  (func results a)

commentChains :: HasPostgres m => Int -> Int -> m [(Text, Text, UTCTime)]
commentChains limit offset =
    query
        "select comments.userid, subreddits.displayName, comments.createutc \
        \from (select id from users order by modifiedUtc limit (?) offset (?)) as u \
        \left join comments on u.id = comments.userid \
        \inner join subreddits on comments.subredditid = subreddits.id \
        \where userid <> '[deleted]' order \
        \by comments.userid, comments.modifiedUtc" (limit, offset)

