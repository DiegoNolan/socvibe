{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module AppConfig where

import ClassyPrelude
import Data.Yaml

readAppConfig :: FilePath -> IO (Either ParseException AppConfig)
readAppConfig = decodeFileEither

data AppConfig = AppConfig
  { acConnectionString :: ByteString
  , acPort :: Int
  }

instance FromJSON AppConfig where
  parseJSON (Object o) =
    AppConfig
      <$> (encodeUtf8 <$> o .: "conn-string")
      <*> o .: "port"
  parseJSON _ = empty
