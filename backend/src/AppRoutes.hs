{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module AppRoutes
  ( routes
  ) where

import ClassyPrelude
import App
import Handlers
import Snap.Util.FileServe

routes :: [(ByteString, AppHandler ())]
routes =
  [ ("sitemap.xml", sitemapHandler)
  , ("robots.txt", robotsHandler)
  , ("js", serveDirectory "static/ghcjs/")
  , ("css", serveDirectory "static/css/")
  , ("images", serveDirectory "static/images/")
  , ("graph.svg", serveFileAs "image/svg+xml" "graph.svg")
  , ("graph", graphHandler)
  , ("reddit-thread", redditThreadHandler)
  , ("reddit-thread/info", redditThreadInfoHandler)
  , ("reddit-thread/:theadId", redditThreadHandler)
  ] ++
  subRoute "admin" adminRoutes
    ++
  [ ("/contact-submit", contactHandler)
  , ("/signup", signupHandler)
  , ("/urls", urlsHandler)
  , ("", indexHandler)
  -- , ("", fourOhFourHandler)
  ]

subRoute :: ByteString -> [(ByteString, AppHandler ())] -> [(ByteString, AppHandler ())]
subRoute pathPart rts = map (\(p, handler) -> (pathPart ++ "/" ++ p, handler)) rts

adminRoutes :: [(ByteString, AppHandler ())]
adminRoutes =
  [ ("crawler-dashboard", crawlerDashboardHandler)
  , ("contacts", adminContactHandler)
  , ("", adminHandler)
  ]

