{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module BarGraph
  ( BarGraph (..)
  , BarGraphEntry (..)
  , createBarGraph
  ) where

import ClassyPrelude

import Data.Aeson

data BarGraph = BarGraph
  { title         :: !Text
  , data_         :: [BarGraphEntry]
  } deriving Show

instance ToJSON BarGraph where
    toJSON BarGraph{..} =
        object  [ "title"   .= title
                , "data"    .= data_
                ]

data BarGraphEntry = BarGraphEntry
    { subreddit     :: !Text
    , value         :: !Float
    } deriving Show

instance ToJSON BarGraphEntry where
    toJSON BarGraphEntry{..} =
        object  [ "subreddit"   .= subreddit
                , "value"       .= value
                ]

createBarGraph :: Real n => Text -> [(Text,n)] -> BarGraph
createBarGraph t dat =
    BarGraph
        t
        (map (\(sub,v) -> BarGraphEntry sub (realToFrac v)) dat)

