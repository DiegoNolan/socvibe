{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
module Control.Monad.Throttled
  ( MonadThrottled(..)
  , ThrottleState(..)
  , getTPS'
  , numTrans'
  ) where

import ClassyPrelude
import Control.Lens
import Control.Concurrent (threadDelay)
import Data.Time.Clock

class MonadIO m => MonadThrottled m where
  getTPS :: m Rational
  numTrans :: Integer -> IO a -> m a

data ThrottleState = ThrottleState
  { tsThrottleVar :: TVar (UTCTime, Integer)
  , tsTargetTPS :: Rational
  }

getTPS' :: MonadIO m => ThrottleState -> m Rational
getTPS' ThrottleState{..} = do
  now <- liftIO getCurrentTime
  (startTime, nt) <- readTVarIO tsThrottleVar
  let count = toRational nt
      timeElasped = toRational (diffUTCTime now startTime)
  return $ if timeElasped == 0
            then 0
            else count / timeElasped

numTrans' :: MonadIO m => ThrottleState -> Integer -> IO a -> m a
numTrans' st@ThrottleState{..} i m = do
  now <- liftIO getCurrentTime
  (startTime, nt) <- readTVarIO tsThrottleVar
  tps <- getTPS' st
  let count = toRational nt
      diff = count / tsTargetTPS
      actualDiff = toRational (diffUTCTime now startTime)
  atomically $ modifyTVar tsThrottleVar (over _2 (+i))
  if tps == 0 || diff < actualDiff
    then do
      liftIO m
    else do
      liftIO $ threadDelay (floor $ (diff - actualDiff) * 1000000)
      liftIO m


