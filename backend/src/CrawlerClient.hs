{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
module CrawlerClient
  ( getRedditCrawlerState
  , setSubreddits
  , addSubreddits
  , removeSubreddits
  , getHackerNewsCrawlerState
  , setMode
  ) where

import ClassyPrelude
import Data.Proxy
import Servant.API
import Servant.Client
import Shared.CrawlerAPI

api :: Proxy CrawlerAPI
api = Proxy

getRedditCrawlerState :: ClientM RedditCrawlerState

setSubreddits :: [Text] -> ClientM RedditCrawlerState

addSubreddits :: [Text] -> ClientM RedditCrawlerState

removeSubreddits :: [Text] -> ClientM RedditCrawlerState

getHackerNewsCrawlerState :: ClientM HackerNewsCrawlerState

setMode :: HNMode -> ClientM HackerNewsCrawlerState

(      getRedditCrawlerState
  :<|> setSubreddits
  :<|> addSubreddits
  :<|> removeSubreddits
  )
  :<|>
   (    getHackerNewsCrawlerState
   :<|> setMode
   ) = client api
