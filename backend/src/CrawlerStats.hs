{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module CrawlerStats where

import ClassyPrelude

data CrawlerStats = CrawlerStats
     { lastSubrredit    :: !Text
     , completeCycleMS  :: !Int
     , currentTPS       :: !Float
     } deriving Show
