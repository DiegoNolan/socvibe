{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Crawlers.Cli ( runCli ) where

import           ClassyPrelude             hiding (getArgs)
import           Control.Lens              hiding (argument)
import           Crawlers.Reddit.Crawler
import qualified Data.Map.Strict           as Map
import           Data.Time.Clock
import           DataAccess.CrawlerState
import           Lib.Postgres
import           Options.Applicative
import qualified Options.Applicative.Help  as AH
import qualified Options.Applicative.Types as AT
import qualified Reddit.Subreddit          as RS
import           System.Environment        (getArgs, getProgName)
import           System.Exit               (ExitCode (..), exitWith)
import           Text.Layout.Table

data Command
  = ListHosts
  | AddHost String
  | AddHackerNews String
  | RemoveHackerNews String
  | BalanceTopNSubs Int
  | RemoveHost String
  | HostInfo String
  deriving Show

argsParser :: Parser Command
argsParser =
  subparser
    (  command "list-hosts" (info (pure ListHosts)
                            (progDesc "List all hosts in DB"))
    <> command "add-host"
         (info (AddHackerNews <$> argument str (metavar "HOSTNAME"))
          (progDesc "Add host to the DB"))
    <> command "add-hacker-news"
         (info (AddHackerNews <$> argument str (metavar "HOSTNAME"))
          (progDesc "Add hacker news to the host"))
    <> command "remove-hacker-news"
         (info (RemoveHackerNews <$> argument str (metavar "HOSTNAME"))
          (progDesc "Remove hacker news crawling from the host"))
    <> command "balance-subreddits"
         (info (BalanceTopNSubs <$> argument auto (metavar "NUMBER SUBREDDITS"))
          (progDesc "Balance the top n subreddits between the hosts"))
    <> command "remove-host"
         (info (RemoveHost <$> argument str (metavar "HOSTNAME"))
          (progDesc "Remove a host from the DB"))
    <> command "host-info"
         (info (HostInfo <$> argument str (metavar "HOSTNAME"))
          (progDesc "Get detailed information about a crawler host"))
    )

runCli :: IO ()
runCli = runCmd =<< customExecParser (prefs prefMods) opts
  where
    prefMods = showHelpOnEmpty <> showHelpOnError
    opts = info argsParser
             (  fullDesc
             <> header "crawler-cli - program to control crawler hosts"
             )

runCmd :: Command -> IO ()
runCmd ListHosts =
  printHostStates =<< runPG listHostStates
runCmd (AddHost hostname) =
  printHostStates =<< runPG (addEmptyHost hostname >> listHostStates)
runCmd (AddHackerNews hostname) =
  printHostStates =<< runPG (setCrawlHN hostname True >> listHostStates)
runCmd (RemoveHackerNews hostname) =
  printHostStates =<< runPG (setCrawlHN hostname False >> listHostStates)
runCmd (BalanceTopNSubs n) =
  printHostStates =<< runPG (balanceTopNSubs n >> listHostStates)
runCmd (RemoveHost hostname) =
  printHostStates =<< runPG (deleteHost hostname >> listHostStates)
runCmd (HostInfo hostname) =
  printHostInfo hostname

printHostInfo :: String -> IO ()
printHostInfo hostname = runPG $ do
  hss <- listHostStates
  case find (\hs -> hs ^.hs_hostName == hostname) hss of
    Nothing -> putStrLn "Could not find host"
    Just hs -> do
      let cs = hs ^. hs_crawlerState
          stTup = do
            subs <- view (cs_redditState . _Just . rs_Subreddits) <$> cs
            hns <- view (cs_hackerNewsState . _Just . hns_CycleTimes) <$> cs
            return (subs, hns)
      case stTup of
        Nothing -> putStrLn "Didn't find crawler state"
        Just (subs, hns) -> do
          let mkAvg :: [NominalDiffTime] -> NominalDiffTime
              mkAvg times = case times of
                              [] -> 0
                              _  -> sum times / fromIntegral (length times)
              averageHn = mkAvg hns
              averageReddit = mkAvg (map (\(_, redditTimes) -> mkAvg redditTimes)
                                     subs)
          putStrLn $ "Hacker News Time : " ++ tshow averageHn
          putStrLn $ "Reddit Time : " ++ tshow averageReddit

displayHostStates :: [HostState] -> Text
displayHostStates hss = pack $
                        tableString
                        [ column (fixed 26) center dotAlign def
                        , column (fixed 12) center dotAlign def
                        , fixedLeftCol 80
                        ] unicodeS
                        (titlesH [ "Host Name", "Hacker News", "Reddit"])
                        rows
  where
    rows = map (\hs -> colsAllG center
                      [ [ hs^.hs_hostName ]
                      , [ case ( do cs <- hs ^. hs_crawlerState
                                    cs ^. cs_hackerNewsState
                                ) of
                            Nothing -> ""
                            Just _  -> "hackernews"
                        ]
                      , case ( do cs <- hs ^. hs_crawlerState
                                  cs ^. cs_redditState
                              ) of
                          Nothing -> [ "" ]
                          Just rs -> justifyText 100 $
                                     intercalate " " (map (unpack . fst)
                                                      (rs^.rs_Subreddits))
                      ]
               ) hss

printHostStates :: [HostState] -> IO ()
printHostStates hs =
  if null hs
    then putStrLn "No hosts yet"
    else putStrLn $ displayHostStates hs

balanceTopNSubs :: HasPostgres m
                => Int
                -> m ()
balanceTopNSubs n = do
  hostStates <- listHostStates
  print hostStates
  topSubs <- liftIO (getTopNSubreddits n)
  print (map RS.display_name topSubs)
  let subredditTimes :: [(Text, [NominalDiffTime])]
      subredditTimes =
        concatMap (\hs -> maybe [] (\rs -> rs^.rs_Subreddits)
                    (hs ^? hs_crawlerState . _Just . cs_redditState . _Just)
                  ) hostStates
      -- Assume by default subreddit crawl time is 15 min
      defaultTime = 60 * 60 * 15
      meanTimes :: Map Text NominalDiffTime
      meanTimes = Map.fromList $
                  map (\(sn, times) ->
                         let len = length times
                         in if len == 0
                           then (sn, defaultTime)
                           else (sn, sum times / fromIntegral len)
                      ) subredditTimes
      neededSubsMap :: Map Text NominalDiffTime
      neededSubsMap =
        Map.fromList $ zip (map RS.display_name topSubs) (repeat defaultTime)
      allWithTimes :: Map Text NominalDiffTime
      allWithTimes = Map.filterWithKey
          (\subname _ -> subname `Map.member` neededSubsMap) neededSubsMap
          `Map.union` neededSubsMap
      orderedTimes :: [(Text, NominalDiffTime)]
      orderedTimes = sortOn (Down . snd) (Map.toList allWithTimes)
      numHosts = length hostStates
      groupedSubs :: [[(Text, NominalDiffTime)]]
      groupedSubs = foldl' (\gs (sn, t) ->
                              let calcTime = sum . map snd
                                  minTime = minimumEx (map calcTime gs)
                                  ins [last] = [(sn, t) : last]
                                  ins (x:xs)
                                    | calcTime x == minTime = ((sn, t) : x) : xs
                                    | otherwise  = x : ins xs
                                  ins _ = []
                              in ins gs
                           ) (replicate numHosts []) orderedTimes
  print groupedSubs
  forM_ (zip hostStates groupedSubs) $ \(hs, subs) -> do
    let st = (map (set _2 []) subs)
    print st
    void $ setSubreddits (hs^.hs_hostName) st
