{-# LANGUAGE Arrows #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
module Crawlers.HackerNews.Crawler
  ( runCrawler
  ) where

import ClassyPrelude
import Control.Arrow (returnA, (<<<))
import Control.Concurrent
import Control.Lens hiding ((.>))
import Control.Monad.Throttled
import Data.Aeson
import Data.Aeson.TH
import Data.Profunctor.Product
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Time.Clock (addUTCTime, diffUTCTime)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import DataAccess.HackerNews
import DataAccess.CrawlerState
import Lib.Http
import Lib.Postgres
import Network.HostName
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Opaleye hiding ( runQuery
                      , runQueryExplicit
                      , runDelete
                      , runInsertMany
                      , runInsertManyReturning
                      , runUpdate
                      , runUpdateReturning
                      , min
                      , null
                      , not
                      )
import SubscriptionExtractor
import qualified Web.HackerNews as HN
import Crawlers.HackerNews.CrawlerConfig
import API

data ItemLock' a b = ItemLock
  { _itemLockId :: a
  , _itemLockCreatedUtc :: b
  } deriving Show

type ItemLock = ItemLock' Int UTCTime

type ItemLockColumn = ItemLock' (Column PGInt4) (Column PGTimestamptz)

makeLenses ''ItemLock'
$(deriveJSON defaultOptions ''ItemLock')
$(makeAdaptorAndInstance "pItemLock" ''ItemLock')

itemLockTable :: Table ItemLockColumn ItemLockColumn
itemLockTable = Table "hn_item_lock" $ pItemLock ItemLock
  { _itemLockId = required "id"
  , _itemLockCreatedUtc = required "created_utc"
  }


itemLockQuery :: Query ItemLockColumn
itemLockQuery = queryTable itemLockTable

-- | Returns true on success

-- TODO: not sure this will work with multiple crawlers. We need to unlock all items
-- if any fails.
lockItems :: (HasPostgres m, MonadUnliftIO m) => [Int] -> m Bool
lockItems itemIds = do
  now <- liftIO getCurrentTime
  catch ( do
            runInsertMany itemLockTable
              (map (\i -> ItemLock (pgInt4 i) (pgUTCTime now)) itemIds)
            return True
        )
        (\(e :: SomeException) -> do
            putStrLn $ "Failed to lock items : " <> tshow e
            return False
        )

unlockItems :: HasPostgres m => [Int] -> m ()
unlockItems itemIds = do
  putStrLn "calling unlock"
  void $ runDelete itemLockTable
    (\columns -> in_ (map pgInt4 itemIds) (_itemLockId columns))

getLowestItemLockQuery :: Query ItemLockColumn
getLowestItemLockQuery = limit 1 . orderBy (asc _itemLockId) $ itemLockQuery

getLowestItemLock :: HasPostgres m => m (Maybe ItemLock)
getLowestItemLock = listToMaybe <$> runQuery getLowestItemLockQuery

-- | Retrieve `Item`
getItem :: (HasManager m, MonadThrottled m)
        => HN.ItemId -> m (Either HN.HackerNewsError HN.Item)
getItem itemId = do
  mgr <- getManager
  numTrans 1 $ HN.getItem mgr itemId

-- | Retrieve `User`
getUser :: (HasManager m, MonadThrottled m)
        => HN.UserId -> m (Either HN.HackerNewsError HN.User)
getUser userId = do
  mgr <- getManager
  numTrans 1 $ HN.getUser mgr userId

-- | Retrieve `MaxItem`
getMaxItem :: (HasManager m, MonadThrottled m)
           => m (Either HN.HackerNewsError HN.MaxItem)
getMaxItem = do
  mgr <- getManager
  numTrans 1 $ HN.getMaxItem mgr

-- | Retrieve `TopStories`
getTopStories :: (HasManager m, MonadThrottled m)
              => m (Either HN.HackerNewsError HN.TopStories)
getTopStories = do
  mgr <- getManager
  numTrans 1 $ HN.getTopStories mgr

-- | Retrieve `NewStories`
getNewStories :: (HasManager m, MonadThrottled m)
              => m (Either HN.HackerNewsError HN.NewStories)
getNewStories = do
  mgr <- getManager
  numTrans 1 $ HN.getNewStories mgr

-- | Retrieve `BestStories`
getBestStories :: (HasManager m, MonadThrottled m)
               => m (Either HN.HackerNewsError HN.BestStories)
getBestStories = do
  mgr <- getManager
  numTrans 1 $ HN.getBestStories mgr

-- | Retrieve `AskStories`
getAskStories :: (HasManager m, MonadThrottled m)
              => m (Either HN.HackerNewsError HN.AskStories)
getAskStories = do
  mgr <- getManager
  numTrans 1 $ HN.getAskStories mgr

-- | Retrieve `ShowStories`
getShowStories :: (HasManager m, MonadThrottled m)
               => m (Either HN.HackerNewsError HN.ShowStories)
getShowStories = do
  mgr <- getManager
  numTrans 1 $ HN.getShowStories mgr

-- | Retrieve `JobStories`
getJobStories :: (HasManager m, MonadThrottled m)
              => m (Either HN.HackerNewsError HN.JobStories)
getJobStories = do
  mgr <- getManager
  numTrans 1 $ HN.getJobStories mgr

-- | Retrieve `Updates`
getUpdates :: (HasManager m, MonadThrottled m)
           => m (Either HN.HackerNewsError HN.Updates)
getUpdates = do
  mgr <- getManager
  numTrans 1 $ HN.getUpdates mgr

runCrawler :: IO ()
runCrawler = do
  pg <- connect
  m <- newManager tlsManagerSettings
  now <- getCurrentTime
  tvar <- newTVarIO (now, 0)
  let cfg = CrawlerConfig
              { cPostgres = PostgresConn pg
              , cManager = m
              , cThrottleState = ThrottleState tvar 1
              }
  runReaderT (addHostNameIfNotExists >> crawlerCycle) cfg

crawlerCycle :: (HasManager m, MonadThrottled m, HasPostgres m, MonadUnliftIO m)
             => m ()
crawlerCycle = do
  hostName <- liftIO getHostName
  putStrLn "Pulling host state"
  mHostState <- getHostState hostName
  startTime <- liftIO getCurrentTime
  let mHNState = do
        hostState <- mHostState
        cs <- hostState ^. hs_crawlerState
        cs ^. cs_hackerNewsState
      doCycle = do
        endTime <- liftIO getCurrentTime
        addHNCycleTime hostName 10 (endTime `diffUTCTime` endTime)
        crawlerCycle
  case mHNState of
    -- TODO: make configurable?
    Nothing -> liftIO (threadDelay (1000*1000*60)) >> doCycle
    Just cs -> do
      putStrLn "Start cycle"
      tps <- getTPS
      let double = fromRational tps :: Double
      putStrLn $ "TPS : " <> tshow double
      newCount <- crawlNewStories
      if newCount > 0
        then doCycle
        else do
          putStrLn "No new stories to crawl"
          moreToCrawl <- crawlComments
          if moreToCrawl
            then doCycle
            else do
              putStrLn "Backfilling"
              mLowestItem <- getLowestItem
              mLowestItemLock <- getLowestItemLock
              let mLowest = case mLowestItem of
                              Nothing -> _itemLockId <$> mLowestItemLock
                              Just i -> case mLowestItemLock of
                                          Nothing -> Just $ i^.itemId
                                          Just il -> Just $ min (i^.itemId)
                                                                (il^.itemLockId)
              case mLowest of
                Nothing -> putStrLn "Could not find lowest (seems improbable)"
                Just lowestId -> do
                  putStrLn $ "Lowest id : " <> tshow lowestId
                  let itemIdsToLock = take 1000 [lowestId-1,lowestId-2..]
                  lockItems itemIdsToLock
                  forM_ (map HN.ItemId itemIdsToLock) (void . crawlItem)
                    `onException` unlockItems itemIdsToLock

                  doCycle

-- Returns true when there were comments to crawl
crawlComments :: (HasPostgres m, HasManager m, MonadThrottled m) => m Bool
crawlComments = do
    itemsUpdate <- getItemsToCrawlChildren 100
    putStrLn $ "items to get comments : " <> tshow (length itemsUpdate)
    if null itemsUpdate
      then return False
      else do
        forM_ itemsUpdate $ \toUpdate -> do
          eItem <- getItem (HN.ItemId (toUpdate^.itemId))
          case eItem of
            Left err -> putStrLn $ "Failed to getItem " <> tshow err
            Right item -> do
              crawlKids item
              now <- liftIO $ getCurrentTime
              runUpdate itemTable
                (\i -> i & itemModifiedUtc .~ pgUTCTime now)
                (\i -> i^.itemId .== pgInt4 (toUpdate^.itemId))
              return ()
        return True
  where
    crawlKids item = do
      case HN.itemKids item of
        Nothing -> return ()
        Just (HN.Kids kidIds) -> do
          kids <- catMaybes <$> forM kidIds crawlItem
          dbItems <- createItems kids
          putStrLn "run all subscriptions"
          runAllSubscriptionsOnItems dbItems
          forM_ kids crawlKids

-- returns the number of items crawled
crawlNewStories :: (HasPostgres m, HasManager m, MonadThrottled m) => m  Int
crawlNewStories = do
  eStories <- getNewStories
  handleErrors eStories "getNewStories" (return 500) -- Say there are items so we try again
    $ \(HN.NewStories itemIds) -> do
      items <- forM itemIds crawlItem
      let itemsCrawled = catMaybes items
      dbItems <- createItems (catMaybes items)
      runAllSubscriptionsOnItems dbItems
      return $ length itemsCrawled

crawlItem :: (HasPostgres m, HasManager m, MonadThrottled m)
          => HN.ItemId
          -> m (Maybe HN.Item)
crawlItem itemId = do
  mItem <- getItemById itemId
  case mItem of
    -- Already crawled
    Just _ -> return Nothing
    Nothing -> do
      eItem <- getItem itemId
      handleErrors eItem ("getItem crawlItem" ++ tshow itemId) (return Nothing)
        (\item -> return $ Just item)

handleErrors :: MonadIO m
             => Either HN.HackerNewsError a
             -> Text
             -> m b
             -> (a -> m b)
             -> m b
handleErrors e identifier failure success = do
  let addIdentifier s = identifier <> " : " <> s
  case e of
    Left HN.NotFound -> do
      putStrLn $ addIdentifier (identifier <> " not found")
      failure
    Left (HN.FailureResponseError i t s) -> do
      putStrLn $ tshow i <> " " <> t <> " " <> s
      failure
    Left (HN.HNConnectionError t) -> do
      putStrLn "connection"
      putStrLn t
      failure
    Left (HN.DecodeFailureError t s) -> do
      putStrLn "decode Failerure"
      putStrLn (addIdentifier $ t <> " " <> s) >> failure
    Left (HN.InvalidContentTypeHeaderError t s) -> do
      putStrLn (t <> " " <> s) >> failure
    Left (HN.UnsupportedContentTypeError t) -> do
      putStrLn t >> failure
    Right result -> success result

getItemsToCrawlChildrenQuery :: UTCTime -> Query ItemColumn
getItemsToCrawlChildrenQuery after = proc () -> do
  let asEpochUnix = round (realToFrac (utcTimeToPOSIXSeconds after))
  item <- itemQuery -< ()
  -- TODO: should probably get in range
  restrict -< item^.itemModifiedUtc .== item^.itemCreatedUtc
  -- TODO: add a delay
              -- .&& item^.itemTime .> maybeToNullable (Just (pgInt8 asEpochUnix))
  returnA -< item

getItemsToCrawlChildren :: HasPostgres m => Int -> m [Item]
getItemsToCrawlChildren lim = do
  now <- liftIO getCurrentTime
  runQuery (limit lim (getItemsToCrawlChildrenQuery (addUTCTime 86400 now)))
