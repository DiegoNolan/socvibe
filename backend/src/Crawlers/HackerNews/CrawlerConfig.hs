{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE DataKinds #-}
module Crawlers.HackerNews.CrawlerConfig where

import ClassyPrelude
import Control.Monad.Throttled
import qualified Database.PostgreSQL.Simple as PG
import Lib.Http
import Lib.Postgres
import Network.HTTP.Client
import Network.HTTP.Client.TLS

data CrawlerConfig = CrawlerConfig
  -- TODO: change to connection pool
  { cPostgres :: Postgres
  , cManager :: Manager
  , cThrottleState :: ThrottleState
  }

instance MonadIO m => MonadThrottled (ReaderT CrawlerConfig m) where
  getTPS = do
    cfg <- ask
    getTPS' (cThrottleState cfg)

  numTrans i m = do
    cfg <- ask
    numTrans' (cThrottleState cfg) i m

instance MonadIO m => HasPostgres (ReaderT CrawlerConfig m) where
  getPostgresState = cPostgres <$> ask

instance MonadIO m => HasManager (ReaderT CrawlerConfig m) where
  getManager = cManager <$> ask
