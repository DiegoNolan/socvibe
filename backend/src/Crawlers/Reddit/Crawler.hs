{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NoImplicitPrelude    #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE RecordWildCards      #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
module Crawlers.Reddit.Crawler
  ( runCrawler
  , getTopNSubreddits
  ) where

import           API
import           ClassyPrelude                       hiding (Handler, link,
                                                      (<>))
import           Control.Concurrent                  (threadDelay)
import           Control.Lens
import           Control.Monad.Catch
import           Control.Monad.RWS.Strict hiding (forM_)
import           Control.Monad.Throttled
import           Control.Retry
import           Crawlers.Reddit.RedditCrawlerConfig
import           Data.Monoid
import           Data.Time.Clock                     (NominalDiffTime,
                                                      diffUTCTime)
import           DataAccess.CrawlerState
import qualified DataAccess.Reddit                   as DA
import qualified Database.PostgreSQL.Simple          as PG
import           FromClient
import           Lib.Http
import           Lib.Postgres
import           Lucid                               hiding (with)
import           Network.HostName
import           Network.HTTP.Client                 hiding (Proxy)
import           Network.HTTP.Client.TLS
import           Reddit.Auth
import           Reddit.Client                       (Sort, hot)
import qualified Reddit.Comment                      as RC
import           Reddit.CommentTree
import qualified Reddit.Link                         as RL
import qualified Reddit.Subreddit                    as RS
import           Reddit.Throttled                    (MonadReddit)
import qualified Reddit.Throttled                    as R
import           Servant
import           SubscriptionExtractor
import           System.Log.Formatter
import           System.Log.Handler                  (setFormatter)
import           System.Log.Handler.Simple
import qualified System.Log.Logger                   as LOG
import qualified Thing.Comment                       as C
import qualified Thing.Link                          as L
import qualified Thing.User                          as U
import           Utility                             (uneededTime)

getTopNSubreddits :: Int -> IO [RS.Subreddit]
getTopNSubreddits n = do
    hostName <- getHostName
    authInfo <- readRedditAuth
    eToken <- getToken authInfo
    case eToken of
      Left er -> putStrLn (pack er) >> return []
      Right token -> do
        now <- getCurrentTime
        tvar <- newTVarIO (now, 0)
        manager <- newManager tlsManagerSettings
        conn <- connect
        view _1 <$> (runRWST (getSubs n Nothing)
          (RedditCrawlerConfig
            { rcPostgres = PostgresConn conn
            , rcUserAgent = "bot for u/zipf-bot"
            , rcManager = manager
            , rcAuthInfo = authInfo
            , rcThrottleState = ThrottleState tvar 0.5
            }) (RedditCrawlerState token now))
  where
    getSubs :: Int -> Maybe Text -> RedditCrawlerMonad [RS.Subreddit]
    getSubs n after
      | n <= 0    = return []
      | otherwise = do
          print after
          eSubs <- getSubreddits "popular" (Just n) Nothing after
          case eSubs of
            Left er -> print er >> return []
            Right subs -> do
              case lastMay subs of
                Nothing -> return []
                Just lastSub -> do
                  let ls = lastSub :: RS.Subreddit
                  restSubs <- getSubs (n - length subs) (Just $ RS.name lastSub)
                  return $ subs ++ restSubs

runCrawler :: IO ()
runCrawler = do
    hostName <- getHostName
    authInfo <- readRedditAuth
    eToken <- getToken authInfo
    case eToken of
      Left er -> putStrLn (pack er)
      Right token -> do
        now <- getCurrentTime
        tvar <- newTVarIO (now, 0)
        manager <- newManager tlsManagerSettings
        conn <- connect
        runRWST (addHostNameIfNotExists >> redditCycle hostName)
          (RedditCrawlerConfig
            { rcPostgres = PostgresConn conn
            -- TODO: configure user agent
            , rcUserAgent = "bot for u/zipf-bot"
            , rcManager = manager
            , rcAuthInfo = authInfo
            , rcThrottleState = ThrottleState tvar 1
            }) (RedditCrawlerState token now)
        return ()
  where
    crawlTimesToKeep = 10

    cycleThrough :: Maybe Text
                 -> [(Text, NominalDiffTime)]
                 -> [(Text, NominalDiffTime)]
    cycleThrough (Just lastSubreddit) (h@(sub, times):rest)
      | sub == lastSubreddit = rest ++ [h]
      | otherwise            = cycleThrough (Just lastSubreddit) rest ++ [h]
    cycleThrough Nothing _ = []
    cycleThrough _ subs = subs

    redditCycle hostName = do
      putStrLn "cycle"
      mHostState <- getHostState hostName
      let mRedditState = do
            hostState <- mHostState
            cs <- hostState ^. hs_crawlerState
            cs ^. cs_redditState
      case mRedditState of
        Nothing -> liftIO (threadDelay (1000*1000*60)) >> redditCycle hostName
        Just rs -> do
          forM_ (rs^.rs_Subreddits) $ \(subreddit, prevTimeTaken) -> do
            timeTaken <- crawlSubreddit subreddit
            void $ addSubredditTime hostName crawlTimesToKeep subreddit timeTaken
          redditCycle hostName

debugM :: MonadIO m => Text -> Text -> m ()
debugM logger msg = liftIO $ LOG.debugM (unpack logger) (unpack msg)

infoM :: MonadIO m => Text -> Text -> m ()
infoM logger msg = liftIO $ LOG.infoM (unpack logger) (unpack msg)

errorM :: MonadIO m => Text -> Text -> m ()
errorM logger msg = liftIO $ LOG.errorM (unpack logger) (unpack msg)

topLevelLogger :: IsString s => s
topLevelLogger = "Crawler"

dbLogger :: IsString s => s
dbLogger = "Crawler.Database"

redditLogger :: IsString s => s
redditLogger = "Crawler.Reddit"

getSubreddit :: (MonadReddit m, MonadUnliftIO m)
             => Text -> m (Either String RS.Subreddit)
getSubreddit subName = catchAndRetry (R.getSubreddit subName)

getSubreddits :: (MonadReddit m, MonadUnliftIO m)
              => String
              -> Maybe Int
              -> Maybe Text
              -> Maybe Text
              -> m (Either String [RS.Subreddit])
getSubreddits sorting limit before after =
  catchAndRetry (R.getSubreddits sorting limit before after)

getLinks :: (MonadReddit m, MonadUnliftIO m)
         => Text -> Sort -> Maybe Int -> m (Either String [RL.Link])
getLinks subreddit sorting limit = catchAndRetry (R.getLinks subreddit sorting limit)

getCommentTree :: (MonadReddit m, MonadUnliftIO m)
               => Text -> Sort -> Maybe Int -> m (Either String CommentTree)
getCommentTree article sorting limit =
  catchAndRetry (R.getCommentTree article sorting limit)

getMoreChildren :: (MonadReddit m, MonadUnliftIO m)
                => Text -> [Text] -> m (Either String [RC.Comment])
getMoreChildren linkId children = catchAndRetry (R.getMoreChildren linkId children)

insertIfNotExists :: (MonadIO m, MonadCatch m, MonadUnliftIO m)
                  => m (Maybe a) -> m Int64 -> m ()
insertIfNotExists get put = handleDBErrors (DA.insertIfNotExists get put)

handleErrors :: (MonadIO m, MonadCatch m, MonadUnliftIO m)
             => m (Either String a) -> m (Either String a)
handleErrors action = do
  e <- tryAny action
  case e of
    Left er -> do
      errorM redditLogger ("Caught Reddit exception : " ++ tshow er)
      return (Left (show er))
    Right v -> return v

handleDBErrors :: (MonadIO m, MonadCatch m, MonadUnliftIO m) => m () -> m ()
handleDBErrors action = do
    e <- tryAny action
    case e of
        Left er -> errorM dbLogger ("Caught db exception : " ++ tshow er)
        Right _ -> return ()

catchAndRetry :: (MonadCatch m, MonadIO m, MonadUnliftIO m)
              => m (Either String a) -> m (Either String a)
catchAndRetry action =
    retrying
        (exponentialBackoff 5000000 <> limitRetries 5)
        (\_ e -> case e of
                    Left _  -> return True
                    Right _ -> return False
        )
        (\_ -> handleErrors action)

crawlSubreddit :: Text
               -> RedditCrawlerMonad NominalDiffTime
crawlSubreddit subDisplayName = do
    start <- liftIO getCurrentTime
    tps <- R.getTPS
    let strTPS = tshow (fromRational tps :: Float)
    putStrLn $ "Crawling subreddit <" ++ subDisplayName ++
                         "> with TPS <" ++ strTPS ++ ">"
    eLinks <- getLinks subDisplayName hot Nothing
    case eLinks of
        Left er -> errorM redditLogger ("Getting links from subreddit <" ++
                          subDisplayName ++ "> failed with : " ++ pack er)
        Right links ->
          forM_ links $ \link -> do
            mLink <- crawlLink True (RL.id link)
            case mLink of
              Nothing -> return ()
              Just (subname, l, comms) -> do
                insertSubredditIfNotExists (L.subredditId l) subname
                insertLink l
                insertComments comms
    end <- liftIO getCurrentTime
    return $ end `diffUTCTime` end

insertSubredditIfNotExists :: (MonadReddit m, HasPostgres m, MonadUnliftIO m)
                           => Text -> Text -> m ()
insertSubredditIfNotExists subredditId subname = do
  mSub <- DA.getSubreddit subredditId
  case mSub of
    Nothing -> do
      eSub <- getSubreddit subname
      case eSub of
        Right sub -> void $ DA.putSubreddit (subredditFromClient sub)
        Left _    -> return ()
    _ -> return ()

insertLink :: (HasPostgres m, MonadCatch m, MonadUnliftIO m)
           => L.Link -> m ()
insertLink l = do
  insertIfNotExists (DA.getUser (L.userId l))
                    (DA.putUser (U.User (L.userId l) uneededTime))
  insertIfNotExists (DA.getLink (L.id_ l)) (DA.putLink l)
  runAllSubscriptionsOnLinks [l]

crawlLink :: (MonadReddit m, HasPostgres m, MonadUnliftIO m)
          => Bool -> Text -> m (Maybe (Text, L.Link, [C.Comment]))
crawlLink shouldRetry linkId = do
  shouldCrawl <- shouldCrawlLink linkId
  if shouldCrawl
    then do
      infoM redditLogger $ "Getting comments for link <" ++ linkId ++ ">"
      ect <- if shouldRetry then getCommentTree linkId hot Nothing
                            else R.getCommentTree linkId hot Nothing
      case ect of
        Left er -> do errorM redditLogger (pack $ "Getting commentTree failed with : "
                                           ++ er)
                      return Nothing
        Right ct -> do
          let l = linkFromClient (link ct)
              comms = comments ct
          allComms <- concat <$> mapM (fetchComments (L.name l) Nothing) comms
          return $ Just (RL.subreddit (link ct), l, allComms)
    else do infoM redditLogger $ "Not crawling link <" ++ linkId ++ ">"
            return Nothing

shouldCrawlLink :: HasPostgres m => Text -> m Bool
shouldCrawlLink linkId = do
  maybeLink <- DA.getLink linkId
  case maybeLink of
    Nothing -> return True
    Just link -> do
      now <- liftIO getCurrentTime
      return $ shouldCrawlTimes now (L.createdUtc link) (L.modifiedUtc link)

shouldCrawlTimes :: UTCTime -> UTCTime -> UTCTime -> Bool
shouldCrawlTimes now createdTime moddedTime
    | createdCrawledTime > 60 * 60 * 24 * 30 = nowCrawledTime > 60 * 60 * 24 * 182.5
    | createdCrawledTime > 60 * 60 * 24 * 7  = nowCrawledTime > 60 * 60 * 24 * 7
    | createdCrawledTime > 60 * 60 * 24      = nowCrawledTime > 60 * 60 * 24
    | createdCrawledTime > 60 * 60 * 4       = nowCrawledTime > 60 * 60 * 4
    | createdCrawledTime > 60 * 60           = nowCrawledTime > 60 * 60
    | otherwise                              = nowCrawledTime > 60 * 15
  where
    createdCrawledTime = abs (diffUTCTime createdTime moddedTime)
    nowCrawledTime = abs (diffUTCTime now moddedTime)

fetchComments :: (HasPostgres m, R.MonadReddit m, MonadUnliftIO m)
              => Text -> Maybe Text -> RC.Comment -> m [C.Comment]
fetchComments linkName parentComment (RC.FullComment fullComment) = do
    let comment = commentFromClient fullComment parentComment
    childComms <- concat <$> mapM (fetchComments linkName (Just (C.id_ comment)))
                                  (RC.replies fullComment)
    return $ comment : childComms
fetchComments linkName parentComment (RC.More RC.MR{..}) = do
    eComments <- getMoreChildren linkName children
    case eComments of
      Left er -> do errorM redditLogger $ "Getting more children failed : " ++ pack er
                    return []
      Right comments -> concat <$> mapM (fetchComments linkName parentComment) comments

-- TODO: Naive and also if the comments get reordered will fail
insertComments :: (HasPostgres m, MonadCatch m, MonadUnliftIO m)
               => [C.Comment] -> m ()
insertComments comments = do
  forM_ comments $ \comment -> do
    insertIfNotExists (DA.getUser (C.userId comment))
                      (DA.putUser (U.User (C.userId comment) uneededTime))
    insertIfNotExists (DA.getComment (C.id_ comment)) (DA.putComment comment)
  runAllSubscriptionsOnComments comments

runAllSubscriptionsOnLinks :: HasPostgres m => [L.Link] -> m ()
runAllSubscriptionsOnLinks links =
  runAllSubscriptions (\sub -> runSubscriptionOnLinks sub links)

runAllSubscriptionsOnComments :: HasPostgres m => [C.Comment] -> m ()
runAllSubscriptionsOnComments comments =
  runAllSubscriptions (\sub -> runSubscriptionOnComments sub comments)

runSubscriptionOnLinks :: HasPostgres m
                       => Subscription
                       -> [L.Link]
                       -> m ()
runSubscriptionOnLinks sub links = do
  let matchedLinks =
          filter (subMatchesOnLink' (sub^.subType) (sub^.subText)) links
  void $ createRedditLinkSubscriptionMatches $
    map (\link -> (sub^.subId, L.id_ link)) matchedLinks

runSubscriptionOnComments :: HasPostgres m
                          => Subscription
                          -> [C.Comment]
                          -> m ()
runSubscriptionOnComments sub comments = do
  let matchedComments =
          filter (subMatchesOnComment' (sub^.subType) (sub^.subText)) comments
  void $ createRedditCommentSubscriptionMatches $
    map (\comment -> (sub^.subId, C.id_ comment)) matchedComments

{-
startCrawler :: CrawlerConfig -> IO ()
startCrawler CrawlerConfig{..} = do

    manager <- newManager tlsManagerSettings
    conn <- connect

    runReaderT  -}
    {-
    Http.withHandle (Http.Config manager) $ \h ->
      Logger.withHandle $ \l ->
      Psql.withHandle (Psql.Config ccConnectionString 5 60 5) $ \p ->
      Throttle.withHandle (Throttle.Config (toRational ccCrawlerTPS)) $ \t ->
        crawlerCycle
          (LoggerThrottleHttpPsql
           { logger = l
           , http = h
           , throttle = t
           , psql = p
           }
          ) -}
      -- hnCycle
      {- bracket (forkIO $ R.startReddit (Just (ccAuthInfo cc)) (ccUserAgent cc) 1
                I (runReaderT redditCycle cfg))
            killThread
            (const $ runThrottledT 1 (runReaderT hnCycle cfg))
      -}
  -- where

    {-
    redditCycle = do
        csTVar <- getCrawlerState
        startTime <- liftIO getCurrentTime

        crawlerState <- liftIO $ atomically $ readTVar csTVar
        let subs = rcsSubreddits (redditState crawlerState)
        redditGo subs subs

        endTime <- liftIO getCurrentTime
        let dt = realToFrac $ endTime `diffUTCTime` startTime
        liftIO $ atomically $ modifyTVar csTVar
            (\cs -> cs { redditState = (redditState cs) { rcsCycleLength = Just dt } })

        redditCycle


    redditGo subs = do
      forM_ subs $ \s -> do
        csTVar <- getCrawlerState
        crawlerState <- liftIO $ atomically $ readTVar csTVar
        when (rcsSubreddits (redditState crawlerState) == allSubs) $ do
          tps <- R.getTPS
          liftIO $ atomically $ modifyTVar csTVar
            (\cs -> cs { redditState = (redditState cs)
                          { rcsTPS = Just tps
                          , rcsCurrentSubreddit = Just s
                          }
                      })
          crawlSubreddit s
      redditGo allSubs ss
-}
