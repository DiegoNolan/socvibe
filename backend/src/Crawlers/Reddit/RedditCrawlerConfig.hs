{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Crawlers.Reddit.RedditCrawlerConfig where

import           ClassyPrelude
import           Control.Lens
import           Control.Monad.Catch
import           Control.Monad.RWS.Strict
import           Control.Monad.Throttled
import           Data.Aeson
import           Data.Aeson.Lens
import qualified Database.PostgreSQL.Simple as PG
import           Lib.Postgres
import           Network.HTTP.Client        hiding (Proxy)
import           Reddit.Auth
import qualified Reddit.Throttled           as R

readRedditAuth :: IO AuthInfo
readRedditAuth = do
  bs <- readFile "config/reddit-auth.json"
  print bs
  let Just username = encodeUtf8 <$> bs ^? key "username" . _String
      Just password = encodeUtf8 <$> bs ^? key "password" . _String
      Just clientId = encodeUtf8 <$> bs ^? key "clientId" . _String
      Just clientSecret = encodeUtf8 <$> bs ^? key "clientSecret" . _String
  return AuthInfo{..}

data RedditCrawlerConfig = RedditCrawlerConfig
  { rcPostgres      :: Postgres
  , rcUserAgent     :: ByteString
  , rcManager       :: Manager
  , rcAuthInfo      :: AuthInfo
  , rcThrottleState :: ThrottleState
  }

data RedditCrawlerState = RedditCrawlerState
  { rcsAuthToken      :: AuthToken
  , rcsLastRefereshed :: UTCTime
  }

type RedditCrawlerMonad = RWST RedditCrawlerConfig () RedditCrawlerState IO

instance MonadUnliftIO RedditCrawlerMonad where
  askUnliftIO = do
    r <- ask
    st <- get
    stRef <- newIORef st
    let f = UnliftIO $ \m -> do
              (a, nst, _) <- runRWST m r st
              writeIORef stRef nst
              return a
    nst <- readIORef stRef
    return f

instance MonadThrottled RedditCrawlerMonad where
  getTPS = do
    cfg <- ask
    getTPS' (rcThrottleState cfg)

  numTrans i m = do
    cfg <- ask
    numTrans' (rcThrottleState cfg) i m

instance R.MonadReddit RedditCrawlerMonad where
  getTPS = getTPS
  numTrans = numTrans
  getUserAgent = rcUserAgent <$> ask
  getManager = rcManager <$> ask
  getToken = return Nothing
    -- TODO: fix
    {-
    st <- get
    let at = rcsAuthToken st
        lastT = rcsLastRefereshed st
    (ai, nAt, nT) <- liftIO $ R.getUpdatedAuthInfo (ai, at, lastT)
    put $ RedditCrawlerState nAt nT
    return (Just nAt)
-}

instance HasPostgres RedditCrawlerMonad where
  getPostgresState = rcPostgres <$> ask
