{-# LANGUAGE Arrows            #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE TemplateHaskell   #-}
module DataAccess.Contact where

import ClassyPrelude hiding (optional, groupBy)
import Control.Arrow (returnA, (<<<))
import Control.Lens hiding ((.>))
import Data.Aeson
import Data.Aeson.TH
import Data.Profunctor.Product
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Opaleye hiding ( runQuery
                      , runQueryExplicit
                      , runDelete
                      , runInsertMany
                      , runInsertManyReturning
                      , runUpdate
                      , runUpdateReturning
                      , null
                      )
import Snap.Snaplet.PostgresqlSimple.Opaleye

data Contact' a b c d e = Contact
  { _conId :: a
  , _conName :: b
  , _conEmail :: c
  , _conMessage :: d
  , _conCreatedUtc :: e
  } deriving Show

type Contact = Contact' Int Text Text Text UTCTime
type ContactColumn = Contact' (Column PGInt4) (Column PGText) (Column PGText)
                              (Column PGText) (Column PGTimestamptz)

type ContactInsertColumn =
  Contact' (Maybe (Column PGInt4)) (Column PGText) (Column PGText)
           (Column PGText) (Column PGTimestamptz)

makeLenses ''Contact'
$(deriveJSON defaultOptions ''Contact')
$(makeAdaptorAndInstance "pContact" ''Contact')

contactTable :: Table ContactInsertColumn ContactColumn
contactTable = Table "contacts" $ pContact Contact
  { _conId = optional "id"
  , _conName = required "name"
  , _conEmail = required "email"
  , _conMessage = required "message"
  , _conCreatedUtc = required "created_utc"
  }

createContact :: HasPostgres m => Contact -> m [Contact]
createContact con = runInsertManyReturning contactTable [contactColumn] retFunc
  where
    contactColumn =
      Contact Nothing (pgStrictText (con^.conName)) (pgStrictText (con^.conEmail))
      (pgStrictText (con^.conMessage)) (pgUTCTime (con^.conCreatedUtc))
    retFunc = id

contactQuery :: Query ContactColumn
contactQuery = queryTable contactTable

listContacts :: HasPostgres m => m [Contact]
listContacts = runQuery contactQuery
