{-# LANGUAGE Arrows                #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}
module DataAccess.CrawlerState where

import           ClassyPrelude
import           Control.Arrow                 (returnA, (<<<))
import           Control.Lens
import           Data.Aeson
import           Data.Aeson.TH
import           Data.Profunctor.Product
import           Data.Profunctor.Product.TH    (makeAdaptorAndInstance)
import           Data.Time.Clock
import           Lib.Postgres
import           Network.HostName
import           Opaleye                       hiding (min, not, null,
                                                runDelete, runInsertMany,
                                                runInsertManyReturning,
                                                runQuery, runQueryExplicit,
                                                runUpdate, runUpdateReturning,
                                                runUpdate_)
import           Opaleye.Internal.Manipulation (Returning (..))

data HackerNewsState = HackerNewsState
  { _hns_CycleTimes :: [NominalDiffTime]
  } deriving (Show, Generic)

makeLenses ''HackerNewsState

instance ToJSON HackerNewsState
instance FromJSON HackerNewsState

data RedditState = RedditState
  { _rs_Subreddits :: [(Text, [NominalDiffTime])]
  , _rs_lastSubreddit :: Maybe Text
  } deriving (Show, Generic)

instance ToJSON RedditState
instance FromJSON RedditState

makeLenses ''RedditState

data CrawlerState = CrawlerState
  { _cs_hackerNewsState :: Maybe HackerNewsState
  , _cs_redditState     :: Maybe RedditState
  } deriving (Show, Generic)

makeLenses ''CrawlerState

instance ToJSON CrawlerState
instance FromJSON CrawlerState

data HostState' a b = HostState
  { _hs_hostName     :: a
  , _hs_crawlerState :: b
  } deriving Show

type HostState = HostState' String (Maybe CrawlerState)
type HostStateColumn = HostState' (Column PGText) (Column (Nullable PGJson))

makeLenses ''HostState'
$(makeAdaptorAndInstance "pHostState" ''HostState')

hostStateTable :: Table HostStateColumn HostStateColumn
hostStateTable = Table "crawler_host_state" $ pHostState HostState
  { _hs_hostName = required "host_name"
  , _hs_crawlerState = required "crawler_state"
  }

hostStateQuery :: Query HostStateColumn
hostStateQuery = queryTable hostStateTable

listHostStates :: HasPostgres m => m [HostState]
listHostStates = do
  rows <- runQuery $ orderBy (asc _hs_hostName) hostStateQuery
  return $ catMaybes $ map convertHostState rows

addEmptyHost :: HasPostgres m => HostName -> m ()
addEmptyHost hostname =
    void $ createHostState HostState
            { _hs_hostName = hostname
            , _hs_crawlerState = Nothing
            }

createHostState :: HasPostgres m => HostState -> m Int64
createHostState hs =
  runInsertMany hostStateTable
    [ hs & hs_hostName %~ pgString
         & hs_crawlerState %~ maybeToNullable . fmap pgValueJSON
    ]

getHostState :: HasPostgres m => HostName -> m (Maybe HostState)
getHostState hostname =
    listToMaybe . catMaybes . map convertHostState <$> runQuery q
  where
    q = proc () -> do
          row <- hostStateQuery -< ()
          restrict -< row ^. hs_hostName .== pgString hostname
          returnA -< row

setSubreddits :: HasPostgres m
              => HostName
              -> [(Text, [NominalDiffTime])]
              -> m Int64
setSubreddits hostname subreddits = do
  modifyRedditState hostname
    (\case
       Nothing -> Just (RedditState subreddits Nothing)
       -- TODO: try to keep subreddit times
       Just rs -> Just $ rs & rs_Subreddits .~ subreddits
    )

addSubredditTime :: HasPostgres m
                 => HostName
                 -> Int
                 -> Text
                 -> NominalDiffTime
                 -> m Int64
addSubredditTime hostname maxTimes subreddit cycleTime = do
  modifyRedditState hostname
    (\case
        Nothing -> Just (RedditState [(subreddit, [cycleTime])] (Just subreddit))
        Just rs -> Just (rs & rs_Subreddits %~ insNewTime
                            & rs_lastSubreddit .~ Just subreddit
                        )
    )
  where
    insNewTime = insertWith (\new old -> take maxTimes (new ++ old))
                     subreddit [cycleTime]

modifyRedditState :: HasPostgres m
                  => HostName
                  -> (Maybe RedditState -> Maybe RedditState)
                  -> m Int64
modifyRedditState hostname f = do
  mHS <- getHostState hostname
  case mHS of
    Nothing -> return 0
    Just hs ->
      case hs^.hs_crawlerState of
        Nothing -> return 0
        Just cs -> do
          let ncs = cs & cs_redditState %~ f
              up = Update
                    { uTable = hostStateTable
                    , uUpdateWith = updateEasy $
                        \row -> row & hs_crawlerState .~
                                  Opaleye.toNullable (pgValueJSON ncs)
                    , uWhere = \row -> row ^. hs_hostName .== pgString hostname
                    , uReturning = Count
                    }
          runUpdate_ up


setCrawlHN :: HasPostgres m
           => HostName
           -> Bool
           -> m Int64
setCrawlHN hostname shouldCrawl = do
  modifyHNState hostname
    (\case
        Just hn -> if shouldCrawl then Just hn else Nothing
        Nothing -> if shouldCrawl
                  then Just (HackerNewsState [])
                  else Nothing
    )

addHNCycleTime :: HasPostgres m
               => HostName
               -> Int
               -> NominalDiffTime
               -> m Int64
addHNCycleTime hostname maxTimes cycleTime = do
  modifyHNState hostname
    (\case
        Just hn -> Just $ hn & hns_CycleTimes %~
                   (\cycleTimes -> take maxTimes (cycleTime : cycleTimes))
        Nothing -> Nothing
    )


modifyHNState :: HasPostgres m
              => HostName
              -> (Maybe HackerNewsState -> Maybe HackerNewsState)
              -> m Int64
modifyHNState hostname f = do
  mHS <- getHostState hostname
  case mHS of
    Nothing -> return 0
    Just hs ->
      case hs^.hs_crawlerState of
        Nothing -> return 0
        Just cs -> do
          let ncs = cs & cs_hackerNewsState %~ f
              up = Update
                    { uTable = hostStateTable
                    , uUpdateWith = updateEasy $
                        \row -> row & hs_crawlerState .~
                                  Opaleye.toNullable (pgValueJSON ncs)
                    , uWhere = \row -> row ^. hs_hostName .== pgString hostname
                    , uReturning = Count
                    }
          runUpdate_ up

convertHostState :: HostState' String (Maybe Value)
                 -> Maybe HostState
convertHostState row =
  case row ^. hs_crawlerState of
    Nothing -> Just $ row & hs_crawlerState .~ Nothing
    Just val -> case fromJSON val of
                  Error _    -> Just $ row & hs_crawlerState .~ Nothing
                  Success cs -> Just $ row & hs_crawlerState .~ Just cs

addHostNameIfNotExists :: HasPostgres m => m ()
addHostNameIfNotExists = do
  hostName <- liftIO getHostName
  mHostState <- getHostState hostName
  case mHostState of
    Just _ -> return ()
    Nothing -> void $ createHostState HostState
                  { _hs_hostName = hostName
                  , _hs_crawlerState = Nothing
                  }

deleteHost :: HasPostgres m => HostName -> m Int64
deleteHost hostname =
  runDelete hostStateTable (\cols -> cols^.hs_hostName .== pgString hostname)


