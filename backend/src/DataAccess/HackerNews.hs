{-# LANGUAGE Arrows #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
module DataAccess.HackerNews where

import ClassyPrelude
import Control.Arrow (returnA, (<<<))
import Control.Lens hiding ((.>))
import Control.Monad.Throttled
import Data.Aeson
import Data.Aeson.TH
import Data.Profunctor.Product
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Time.Clock (addUTCTime)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import Lib.Http
import Lib.Postgres
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Opaleye hiding ( runQuery
                      , runQueryExplicit
                      , runDelete
                      , runInsertMany
                      , runInsertManyReturning
                      , runUpdate
                      , runUpdateReturning
                      , min
                      , null
                      , not
                      )
import qualified Web.HackerNews as HN
import API

type ItemColumn =
  Item' (Column PGInt4) (Column (Nullable PGBool)) (Column PGInt4)
        (Column (Nullable PGText)) (Column (Nullable PGInt8))
        (Column (Nullable PGText)) (Column (Nullable PGBool))
        (Column (Nullable PGInt4)) (Column (Nullable PGText))
        (Column (Nullable PGInt4)) (Column (Nullable PGText))
        (Column PGTimestamptz) (Column PGTimestamptz)

type ItemInsertColumn =
  Item' (Column PGInt4) (Maybe (Column (Nullable PGBool)))
        (Column PGInt4) (Maybe (Column (Nullable PGText)))
        (Maybe (Column (Nullable PGInt8))) (Maybe (Column (Nullable PGText)))
        (Maybe (Column (Nullable PGBool))) (Maybe (Column (Nullable PGInt4)))
        (Maybe (Column (Nullable PGText))) (Maybe (Column (Nullable PGInt4)))
        (Maybe (Column (Nullable PGText)))
        (Column PGTimestamptz) (Column PGTimestamptz)

$(deriveJSON defaultOptions ''Item')
$(makeAdaptorAndInstance "pItem" ''Item')

itemTable :: Table ItemColumn ItemColumn
itemTable = Table "hn_item" $ pItem Item
  { _itemId = required "id"
  , _itemDeleted = required "deleted"
  , _itemType = required "type"
  , _itemBy = required "by"
  , _itemTime = required "time"
  , _itemText = required "text"
  , _itemDead = required "dead"
  , _itemParent = required "parent"
  , _itemURL = required "url"
  , _itemScore = required "score"
  , _itemTitle = required "title"
  , _itemModifiedUtc = required "modified_utc"
  , _itemCreatedUtc = required "created_utc"
  }

createItems :: HasPostgres m => [HN.Item] -> m [Item]
createItems hnItems = do
  now <- liftIO getCurrentTime
  let items = catMaybes (map (fmap itemToColumn . fromHnItem now now) hnItems)
  runInsertManyReturning itemTable items id

itemQuery :: Query ItemColumn
itemQuery = queryTable itemTable

getLowestItemQuery :: Query ItemColumn
getLowestItemQuery = limit 1 . orderBy (asc _itemId) $ itemQuery

getLowestItem :: HasPostgres m => m (Maybe Item)
getLowestItem = listToMaybe <$> runQuery getLowestItemQuery

getItemQuery :: HN.ItemId -> Query ItemColumn
getItemQuery (HN.ItemId i) = proc () -> do
  item <- itemQuery -< ()
  restrict -< item^.itemId .== pgInt4 i
  returnA -< item

getItems :: HasPostgres m => Int -> Int -> UTCTime -> m [Item]
getItems lim off before =
  runQuery (getItemsQuery lim off before)

getItemsQuery :: Int -> Int -> UTCTime -> Query ItemColumn
getItemsQuery lim off before =
       limit lim
     . offset off
     . orderBy (desc _itemTime)
     $ q
  where
    asEpochUnix = round (realToFrac (utcTimeToPOSIXSeconds before))
    q = proc () -> do
          item <- itemQuery -< ()
          restrict -< item^.itemTime .< maybeToNullable (Just (pgInt8 asEpochUnix))
          returnA -< item

-- Get items and go forward in time
getItemsForward :: HasPostgres m => Int64 -> Int -> m [Item]
getItemsForward t lim = runQuery fq
  where
    fq = limit lim . orderBy (asc _itemTime) $ q
    q = proc () -> do
          item <- itemQuery -< ()
          restrict -< item^.itemTime .> maybeToNullable (Just (pgInt8 t))
          returnA -< item

getItemById :: HasPostgres m => HN.ItemId -> m (Maybe Item)
getItemById itemId = listToMaybe <$> runQuery (getItemQuery itemId)

itemToColumn :: Item -> ItemColumn
itemToColumn item =
  item & itemId %~ pgInt4
       & itemDeleted %~ maybeToNullable . fmap pgBool
       & itemType %~ pgInt4 . fromEnum
       & itemBy %~ maybeToNullable . fmap pgStrictText
       & itemTime %~ maybeToNullable . fmap pgInt8
       & itemText %~ maybeToNullable . fmap pgStrictText
       & itemDead %~ maybeToNullable . fmap pgBool
       & itemParent %~ maybeToNullable . fmap pgInt4
       & itemURL %~ maybeToNullable . fmap pgStrictText
       & itemScore %~ maybeToNullable . fmap pgInt4
       & itemTitle %~ maybeToNullable . fmap pgStrictText
       & itemModifiedUtc %~ pgUTCTime
       & itemCreatedUtc %~ pgUTCTime

fromHnItem :: UTCTime -> UTCTime -> HN.Item -> Maybe Item
fromHnItem modified created HN.Item{..} =
  case itemId of
    Nothing -> Nothing
    Just (HN.ItemId i) ->
      Just $ Item
        { _itemId = i
        , _itemDeleted = fromDeleted <$> itemDeleted
        , _itemType = fromEnum itemType
        , _itemBy = fromUserName <$> itemBy
        , _itemTime = fromTime <$> itemTime
        , _itemText = fromItemText <$> itemText
        , _itemDead = fromDead <$> itemDead
        , _itemParent = fromParent <$> itemParent
        , _itemURL = fromURL <$> itemURL
        , _itemScore = fromScore <$> itemScore
        , _itemTitle = fromTitle <$> itemTitle
        , _itemModifiedUtc = modified
        , _itemCreatedUtc = created
        }

fromDeleted :: HN.Deleted -> Bool
fromDeleted (HN.Deleted d) = d

fromUserName :: HN.UserId -> Text
fromUserName (HN.UserId un) = un

fromTime :: HN.Time -> Int64
fromTime (HN.Time t) = fromIntegral t

fromItemText :: HN.ItemText ->Text
fromItemText (HN.ItemText t) = t

fromDead :: HN.Dead -> Bool
fromDead (HN.Dead d) = d

fromParent :: HN.Parent -> Int
fromParent (HN.Parent (HN.ItemId i)) = i

fromURL :: HN.URL -> Text
fromURL (HN.URL t) = t

fromScore :: HN.Score -> Int
fromScore (HN.Score s) = s

fromTitle :: HN.Title -> Text
fromTitle (HN.Title t) = t
