{-# LANGUAGE NoImplicitPrelude,
             OverloadedStrings #-}
module DataAccess.Memcache
  ( getLinkAndComments
  ) where

import ClassyPrelude
import Data.Aeson
import qualified DataAccess as DA
import qualified Database.Memcache.Client as Mem
import qualified Thing.Comment as C
import qualified Thing.Link as L

getLinkAndComments :: Text -> AppHandler (Maybe (L.Link, [C.Comment]))
getLinkAndComments postId = do
    mClient <- getMem
    case mClient of
      Nothing -> DA.getLinkAndComments postId
      Just client -> do
        putStrLn "before gat"
        mResult <- liftIO $ Mem.get client (encodeUtf8 postId)
        putStrLn "after the gat"
        case mResult of
          Nothing -> do
            fetchResult <- DA.getLinkAndComments postId
            void $ liftIO $ Mem.set client (encodeUtf8 postId) (toStrict (encode fetchResult)) 0 expTime
            return fetchResult
          Just (val,_,_) -> do
            putStrLn "Fetched from memcache"
            case decode (fromStrict val) of
              Nothing -> putStrLn "Could not decode memcache result" >> return Nothing
              Just res -> return res
  where
    expTime = 3600
