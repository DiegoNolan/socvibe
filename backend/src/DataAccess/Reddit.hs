{-# LANGUAGE Arrows #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
module DataAccess.Reddit where

import ClassyPrelude
import API
import Control.Arrow (returnA, (<<<))
import Control.Lens hiding ((.>))
import Data.Aeson
import Data.Aeson.TH
import qualified Data.Map as Map
import Data.Profunctor.Product hiding (field)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Database.PostgreSQL.Simple ( Connection
                                  , ConnectInfo(..)
                                  , Query
                                  , Only(..))
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.ToRow
import Lib.Postgres
import qualified Opaleye as O
import Opaleye ( PGText, PGInt4, PGFloat8, Column
               , PGTimestamptz, PGBool, required
               , (.<)
               )
import qualified Thing.Comment as C
import qualified Thing.Link as L
import qualified Thing.Subreddit as S
import qualified Thing.User as U
import DataAccess.Reddit.Types
import qualified RedditTypes as RT

{-
createSubType :: Query
createSubType =
    " create table if not exists reddit_subtype \
    \( sub_id text not null references reddit_subreddit (id) on delete cascade \
    \, sub_type text not null \
    \, unique sub_type)"

getSubType :: HasPostgres m => Text -> m (Maybe Text)
getSubType displayName = do
    subs <- query "select type from reddit_subreddit \
                   \inner join subtypes on subtypes.subId = subreddits.id \
                   \where subreddits.displayName = (?)" (Only displayName)
    case subs of
        [s] -> return (Just (fromOnly s))
        _ -> return Nothing

getAllSubTypes :: HasPostgres m => m [Text]
getAllSubTypes =
    map fromOnly <$> query_ "select type from subtyes distinct"

data SubType = SubType
  { subId :: !Text
  , type_ :: !Text
  } deriving Show

instance FromJSON SubType where
  parseJSON (Object o) =
    SubType <$> o .: "subId"
            <*> o .: "type"
  parseJSON _ = mzero

instance ToRow SubType where
  toRow SubType{..} =
    [ toField subId
    , toField type_
    ]

instance FromRow SubType where
  fromRow =
    SubType
      <$> field
      <*> field

existingSubTypes :: HasPostgres m => m [Text]
existingSubTypes =
  map fromOnly <$> query_ "select distinct type from subtypes order by type"

putSubType :: HasPostgres m => SubType -> m Int64
putSubType =
  execute
    "insert into subtypes \
    \( subId, type) values (?,?)"

getSubWithoutType :: HasPostgres m => m (Maybe (Text,Text))
getSubWithoutType = do
    mSub <- query_
                "select subreddits.id, subreddits.displayName from reddit_subreddit \
                \left join subtypes on subtypes.subId = subreddits.id \
                \where subtypes.subId is null limit 1"
    case mSub of
       [s] -> return (Just s)
       _ -> return Nothing
-}

single :: [a] -> Maybe a
single [x] = Just x
single _   = Nothing

-- | TODO: Inefficient but simple, suffers from race conditions. Change later
insertIfNotExists :: Monad m => m (Maybe a) -> m Int64 -> m ()
insertIfNotExists getFunc putFunc = do
  mV <- getFunc
  case mV of
    Nothing -> void putFunc
    Just _ -> return ()

putUser :: HasPostgres m => U.User -> m Int64
putUser =
  execute "insert into reddit_user \
          \(id, modifiedUtc) \
          \values (?, now())"

getUser :: HasPostgres m => Text -> m (Maybe U.User)
getUser id_ =
    single <$> query "select * from reddit_user where id = (?)" (Only id_)

putSubreddit :: HasPostgres m => S.Subreddit -> m Int64
putSubreddit =
  execute
    "insert into reddit_subreddit \
    \( id, title, name, displayName, url, description, subscribers \
    \, subredditType, over18, publicTraffic, submissionType, submitText \
    \, createdUtc, modifiedUtc) \
    \values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())"

getSubreddit :: HasPostgres m => Text -> m (Maybe S.Subreddit)
getSubreddit id_ =
    single <$> query "select * from reddit_subreddit where id = (?)" (Only id_)

putLink :: HasPostgres m => L.Link -> m Int64
putLink =
  execute
    "insert into reddit_link \
    \( id, name, title, ups, downs, score, gilded, url, domain, permalink \
    \, hidden, isSelf, numComments, over18, selftext, selftextHtml, stickied \
    \, thumbnail, edited, userId, subredditId, createUtc, modifiedUtc) \
    \values \
    \( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? \
    \, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())"

getLink :: HasPostgres m => Text -> m (Maybe L.Link)
getLink id_ = single <$> query "select * from reddit_link where id = (?)" (Only id_)

putComment :: HasPostgres m => C.Comment -> m Int64
putComment =
  execute
    "insert into reddit_comment \
    \( id, name, ups, downs, score, gilded, controversiality \
    \, body, bodyHtml, edited \
    \, scoreHidden, userId, linkId, parentComment, subredditId \
    \, createUtc, modifiedUtc) \
    \values \
    \( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())"

getComment :: HasPostgres m => Text -> m (Maybe C.Comment)
getComment id_ =
  single <$> query "select * from reddit_comment where id = (?)" (Only id_)

getCommentTexts :: HasPostgres m => m [Text]
getCommentTexts =
  map fromOnly <$> query_ "select body from reddit_comment limit 1000"

{-
-- | Get all the subreddits in no particular order
getAllSubreddits :: HasPostgres m => m [S.Subreddit]
getAllSubreddits =
    query_ "select * from reddit_subreddit"

getAllSubredditsOrderBySubs :: HasPostgres m => Maybe Text -> m [S.Subreddit]
getAllSubredditsOrderBySubs Nothing =
    query_ "select * from reddit_subreddit order by subscribers desc"
getAllSubredditsOrderBySubs (Just displayName) =
    query  "select * from reddit_subreddit \
                \where subscribers <= (select subscribers from reddit_subreddit \
                \where displayName = (?) ) \
                \order by subscribers desc" (Only displayName)
-}

listSubredditNames :: HasPostgres m => m [Text]
listSubredditNames =
  map fromOnly <$> query_ "select displayName from reddit_subreddit order by displayName"

getLinkAndComments :: HasPostgres m => Text -> m (Maybe (L.Link, [C.Comment]))
getLinkAndComments postId = do
  mLink <- getLink postId
  case mLink of
    Nothing -> return Nothing
    Just link -> do
      comms <- query "select * from reddit_comment where linkId = (?)" (Only postId)
      return $ Just (link, comms)

-------------------------------------------------------------------------------
-- | Misc
-------------------------------------------------------------------------------

commentCount :: HasPostgres m => m Int
commentCount = fromOnly . unsafeHead <$>
    query_ "select count(*) from reddit_comment"

userCount :: HasPostgres m => m Int
userCount = fromOnly . unsafeHead <$>
    query_ "select count(*) from reddit_user"

subredditCount :: HasPostgres m => m Int
subredditCount =
  fromOnly . unsafeHead <$> query_ "select count(*) from reddit_subreddit"

linkCount :: HasPostgres m => m Int
linkCount = fromOnly . unsafeHead <$> query_ "select count(*) from reddit_link"

-------------------------------------------------------------------------------
-- | Opaleye
-------------------------------------------------------------------------------

redditCommentTable :: O.Table RedditCommentColumn RedditCommentColumn
redditCommentTable = O.Table "reddit_comment" $ pRedditComment RedditComment
  { _rc_id = required "id"
  , _rc_name = required "name"
  , _rc_ups = required "ups"
  , _rc_downs = required "downs"
  , _rc_score = required "score"
  , _rc_gilded = required "gilded"
  , _rc_controversiality = required "controversiality"
  , _rc_body = required "body"
  , _rc_bodyHtml = required "bodyhtml"
  , _rc_edited = required "edited"
  , _rc_scoreHidden = required "scorehidden"
  , _rc_userId = required "userid"
  , _rc_linkId = required "linkid"
  , _rc_parentComment = required "parentcomment"
  , _rc_subredditId = required "subredditid"
  , _rc_createdUtc = required "createutc"
  , _rc_modifiedUtc = required "modifiedutc"
  }

redditCommentQuery :: O.Query RedditCommentColumn
redditCommentQuery = O.queryTable redditCommentTable

getRedditCommentsQuery :: Int -> Int -> UTCTime -> O.Query RedditCommentColumn
getRedditCommentsQuery lim off before =
      O.limit lim
    . O.offset off
    . O.orderBy (O.desc _rc_createdUtc)
    $ q
  where
    q = proc () -> do
          rc <- redditCommentQuery -< ()
          O.restrict -< rc^.rc_createdUtc .< O.pgUTCTime before
          returnA -< rc

getRedditComments :: HasPostgres m
                  => Int -> Int -> UTCTime -> m [RedditComment]
getRedditComments lim off before =
  runQuery (getRedditCommentsQuery lim off before)

redditLinkTable :: O.Table RedditLinkColumn RedditLinkColumn
redditLinkTable = O.Table "reddit_link" $ pRedditLink RedditLink
  { _rl_id = required "id"
  , _rl_name = required "name"
  , _rl_title = required "title"
  , _rl_ups = required "ups"
  , _rl_downs = required "downs"
  , _rl_score = required "score"
  , _rl_gilded = required "gilded"
  , _rl_url = required "url"
  , _rl_domain = required "domain"
  , _rl_permalink = required "permalink"
  , _rl_hidden = required "hidden"
  , _rl_isSelf = required "isself"
  , _rl_numComments = required "numcomments"
  , _rl_over18 = required "over18"
  , _rl_selftext = required "selftext"
  , _rl_selftextHtml = required "selftexthtml"
  , _rl_stickied = required "stickied"
  , _rl_thumbnail = required "thumbnail"
  , _rl_edited = required "edited"
  , _rl_userId = required "userid"
  , _rl_subredditId = required "subredditid"
  , _rl_createdUtc = required "createutc"
  , _rl_modified = required "modifiedutc"
  }

redditLinkQuery :: O.Query RedditLinkColumn
redditLinkQuery = O.queryTable redditLinkTable

getRedditLinksQuery :: Int -> Int -> UTCTime -> O.Query RedditLinkColumn
getRedditLinksQuery lim off before =
      O.limit lim
    . O.offset off
    . O.orderBy (O.desc _rl_createdUtc)
    $ q
  where
    q = proc () -> do
          rl <- redditLinkQuery -< ()
          O.restrict -< rl^.rl_createdUtc .< O.pgUTCTime before
          returnA -< rl

getRedditLinks :: HasPostgres m
               => Int -> Int -> UTCTime -> m [RedditLink]
getRedditLinks lim off before =
  runQuery (getRedditLinksQuery lim off before)

redditSubredditTable :: O.Table RedditSubredditColumn RedditSubredditColumn
redditSubredditTable = O.Table "reddit_subreddit" $ pRedditSubreddit RedditSubreddit
  { _rs_id = required "id"
  , _rs_title = required "title"
  , _rs_name = required "name"
  , _rs_displayName = required "displayname"
  , _rs_url = required "url"
  , _rs_description = required "description"
  , _rs_subscribers = required "subscribers"
  , _rs_subredditType = required "subreddittype"
  , _rs_over18 = required "over18"
  , _rs_publicTraffic = required "publictraffic"
  , _rs_submissionType = required "submissiontype"
  , _rs_submitText = required "submittext"
  , _rs_createdUtc = required "createdutc"
  , _rs_modifiedUtc = required "modifiedutc"
  }

redditSubredditQuery :: O.Query RedditSubredditColumn
redditSubredditQuery = O.queryTable redditSubredditTable

getAllSubreddits :: HasPostgres m => m [RedditSubreddit]
getAllSubreddits = runQuery redditSubredditQuery

makeSubredditMap :: [RedditSubreddit] -> Map Text Text
makeSubredditMap subs = Map.fromList $ map (\s ->
                                             ( s^.rs_id
                                             , s ^.rs_displayName
                                             )
                                           ) subs

convertComment :: Map Text Text -> RedditComment -> RT.RedditComment
convertComment subredditMap rc =
  RT.RedditComment
    { RT.commentId = rc^.rc_id
    , RT.commentAuthor = rc^.rc_userId
    , RT.commentBody = rc^.rc_bodyHtml
    , RT.commentScore = rc^.rc_score
    , RT.commentTime = rc^.rc_createdUtc
    , RT.commentSubreddit = fromMaybe "" $
                            Map.lookup (rc^.rc_subredditId) subredditMap
    }

convertLink :: Map Text Text -> RedditLink -> RT.RedditLink
convertLink subredditMap rl =
  RT.RedditLink
    { RT.linkId = rl^.rl_id
    , RT.linkAuthor = rl^.rl_userId
    , RT.linkTitle = rl^.rl_title
    , RT.linkScore  = rl^.rl_score
    , RT.linkTime = rl^.rl_createdUtc
    , RT.linkSubreddit = fromMaybe "" $
                         Map.lookup (rl^.rl_subredditId) subredditMap
    }

interleaveReddit :: [RT.RedditLink] -> [RT.RedditComment] -> [RedditCommOrLink]
interleaveReddit (l:ls) (c:cs)
  | RT.linkTime l < RT.commentTime c =  RL l : interleaveReddit ls (c:cs)
  | otherwise = RC c : interleaveReddit (l:ls) cs
interleaveReddit ls cs = map RL ls ++ map RC cs
