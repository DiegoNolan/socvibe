{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeSynonymInstances #-}
module DataAccess.Reddit.Types where

import ClassyPrelude
import Control.Lens
import Data.Aeson
import Data.Profunctor.Product hiding (field)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import qualified Opaleye as O
import Opaleye ( PGText, PGInt4, PGFloat8, Column
               , PGTimestamptz, PGBool, required
               , (.<)
               )

data RedditLink' a b c d e f g h i j k l m n o p q r s t u v w = RedditLink
  { _rl_id :: a
  , _rl_name :: b
  , _rl_title :: c
  , _rl_ups :: d
  , _rl_downs :: e
  , _rl_score :: f
  , _rl_gilded :: g
  , _rl_url :: h
  , _rl_domain :: i
  , _rl_permalink :: j
  , _rl_hidden :: k
  , _rl_isSelf :: l
  , _rl_numComments :: m
  , _rl_over18 :: n
  , _rl_selftext :: o
  , _rl_selftextHtml :: p
  , _rl_stickied :: q
  , _rl_thumbnail :: r
  , _rl_edited :: s
  , _rl_userId :: t
  , _rl_subredditId :: u
  , _rl_createdUtc :: v
  , _rl_modified :: w
  } deriving (Show, Generic)

makeLenses ''RedditLink'
instance ToJSON RedditLink
instance FromJSON RedditLink

type RedditLink =
  RedditLink' Text Text Text Int Int Int Int Text Text Text Bool Bool Int Bool
    Text (Maybe Text) Bool Text (Maybe UTCTime) Text Text UTCTime UTCTime
type RedditLinkColumn =
  RedditLink' (Column PGText) (Column PGText) (Column PGText) (Column PGInt4)
    (Column PGInt4) (Column PGInt4) (Column PGInt4) (Column PGText)
    (Column PGText) (Column PGText) (Column PGBool) (Column PGBool)
    (Column PGInt4) (Column PGBool) (Column PGText) (Column (O.Nullable PGText))
    (Column PGBool) (Column PGText) (Column (O.Nullable PGTimestamptz))
    (Column PGText) (Column PGText) (Column PGTimestamptz) (Column PGTimestamptz)

$(makeAdaptorAndInstance "pRedditLink" ''RedditLink')


data RedditComment' a b c d e f g h i j k l m n o p q = RedditComment
  { _rc_id :: a
  , _rc_name :: b
  , _rc_ups :: c
  , _rc_downs :: d
  , _rc_score :: e
  , _rc_gilded :: f
  , _rc_controversiality :: g
  , _rc_body :: h
  , _rc_bodyHtml :: i
  , _rc_edited :: j
  , _rc_scoreHidden :: k
  , _rc_userId :: l
  , _rc_linkId :: m
  , _rc_parentComment :: n
  , _rc_subredditId :: o
  , _rc_createdUtc :: p
  , _rc_modifiedUtc :: q
  } deriving (Show, Generic)

makeLenses ''RedditComment'
instance ToJSON RedditComment
instance FromJSON RedditComment

type RedditComment =
  RedditComment' Text Text Int Int Int Int Double Text Text (Maybe UTCTime)
                 Bool Text Text (Maybe Text) Text UTCTime UTCTime
type RedditCommentColumn =
  RedditComment' (Column PGText) (Column PGText) (Column PGInt4) (Column PGInt4)
    (Column PGInt4) (Column PGInt4) (Column PGFloat8) (Column PGText) (Column PGText)
    (Column (O.Nullable PGTimestamptz)) (Column PGBool) (Column PGText) (Column PGText)
    (Column (O.Nullable PGText)) (Column PGText) (Column PGTimestamptz)
    (Column PGTimestamptz)

$(makeAdaptorAndInstance "pRedditComment" ''RedditComment')

data RedditSubreddit' a b c d e f g h i j k l m n = RedditSubreddit
  { _rs_id :: a
  , _rs_title :: b
  , _rs_name :: c
  , _rs_displayName :: d
  , _rs_url :: e
  , _rs_description :: f
  , _rs_subscribers :: g
  , _rs_subredditType :: h
  , _rs_over18 :: i
  , _rs_publicTraffic :: j
  , _rs_submissionType :: k
  , _rs_submitText :: l
  , _rs_createdUtc :: m
  , _rs_modifiedUtc :: n
  } deriving (Show, Generic)

makeLenses ''RedditSubreddit'
instance ToJSON RedditSubreddit
instance FromJSON RedditSubreddit

type RedditSubreddit =
  RedditSubreddit' Text Text Text Text Text Text Int Text Bool Bool
                   Text Text UTCTime UTCTime
type RedditSubredditColumn =
  RedditSubreddit' (Column PGText) (Column PGText) (Column PGText) (Column PGText)
    (Column PGText) (Column PGText) (Column PGInt4) (Column PGText)
    (Column PGBool) (Column PGBool) (Column PGText) (Column PGText)
    (Column PGTimestamptz) (Column PGTimestamptz)

$(makeAdaptorAndInstance "pRedditSubreddit" ''RedditSubreddit')
