{-# LANGUAGE Arrows            #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE TemplateHaskell   #-}
module DataAccess.URL where

import ClassyPrelude hiding (optional, groupBy)
import Control.Arrow (returnA, (<<<))
import Control.Lens hiding ((.>))
import Data.Int
import Data.Profunctor.Product
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Opaleye hiding ( runQuery
                      , runQueryExplicit
                      , runDelete
                      , runInsertMany
                      , runInsertManyReturning
                      , runUpdate
                      , runUpdateReturning
                      , null
                      )
import Shared.Types
import Snap.Snaplet.PostgresqlSimple.Opaleye
import qualified Lib.URLs as L

type URLColumn = URL' (Column PGInt4) (Column PGText) (Column PGText) (Column PGText)
                      (Column PGText) (Column PGText) (Column PGText) (Column PGTimestamptz)
type URLInsertColumn = URL' (Maybe (Column PGInt4)) (Column PGText) (Column PGText) (Column PGText)
                            (Column PGText) (Column PGText) (Column PGText) (Column PGTimestamptz)

$(makeAdaptorAndInstance "pURL" ''URL')

urlTable :: Table URLInsertColumn URLColumn
urlTable = Table "urls" $ pURL URL
  { _urlId = optional "id"
  , _urlFull = required "full_url"
  , _urlProtocol = required "protocol"
  , _urlDomain = required "domain"
  , _urlPath = required "path"
  , _urlQueryString = required "query_string"
  , _urlAnchor = required "anchor"
  , _urlCreatedUtc = required "created_utc"
  }

urlQuery :: Query URLColumn
urlQuery = queryTable urlTable

getUrlByFullQuery :: Text -> Query URLColumn
getUrlByFullQuery full = proc () -> do
  url <- urlQuery -< ()
  restrict -< url^.urlFull .== pgStrictText full
  returnA -< url

getUrlByFull :: HasPostgres m => Text -> m (Maybe URL)
getUrlByFull full = do
  urls <- runQuery (getUrlByFullQuery full)
  return $ case urls of
             [url] -> Just url
             _ -> Nothing

createUrlIfNotExists :: HasPostgres m => L.URL -> m [URL]
createUrlIfNotExists u = do
  mUrl <- getUrlByFull (L.originalURL u)
  case mUrl of
    Nothing -> createUrl u
    Just url -> return [url]

createUrl :: HasPostgres m => L.URL -> m [URL]
createUrl (url@L.URL{..}) = do
    now <- liftIO $ getCurrentTime
    runInsertManyReturning urlTable [urlInsColumn now] retFunc
  where
    urlInsColumn :: UTCTime -> URLInsertColumn
    urlInsColumn t = URL Nothing (pgStrictText (L.originalURL url)) (pgStrictText protocol)
                       (pgStrictText domain) (pgStrictText path) (pgStrictText queryString)
                       (pgStrictText anchor) (pgUTCTime t)
    retFunc :: URLColumn -> URLColumn
    retFunc = id

type URLRedditCommentColumn = URLRedditComment' (Column PGInt4) (Column PGText)

$(makeAdaptorAndInstance "pURLRedditComment" ''URLRedditComment')

urlRedditCommentTable :: Table URLRedditCommentColumn URLRedditCommentColumn
urlRedditCommentTable = Table "url_reddit_comments" $ pURLRedditComment URLRedditComment
  { _urcUrlId = required "url_id"
  , _urcCommentId = required "comment_id"
  }

urlRedditCommentQuery :: Query URLRedditCommentColumn
urlRedditCommentQuery = queryTable urlRedditCommentTable

getUrlRedditCommentQuery :: Int -> Text -> Query URLRedditCommentColumn
getUrlRedditCommentQuery urlId rcId = proc () -> do
  urlRC <- urlRedditCommentQuery -< ()
  restrict -< urlRC^.urcCommentId .== pgStrictText rcId .&& urlRC^.urcUrlId .== pgInt4 urlId
  returnA -< urlRC

getUrlRedditComment :: HasPostgres m => Int -> Text -> m [URLRedditComment]
getUrlRedditComment urlId rcId =
  runQuery (getUrlRedditCommentQuery urlId rcId)

{-
getLastUrlRedditCommentQuery :: Query URLRedditCommentColumn
getLastUrlRedditCommentQuery = q
  where
    q = proc () -> do
          urc <- urlRedditCommentQuery -< ()
-}

groupOrderUrls :: Query ( Column PGInt4, Column PGText, Column PGText, Column PGText
                        , Column PGText, Column PGText, Column PGText, Column PGTimestamptz
                        , Column a
                        )
               -> Query ( Column PGInt4, Column PGText, Column PGText, Column PGText
                        , Column PGText, Column PGText, Column PGText, Column PGTimestamptz
                        , Column PGInt8
                        )
groupOrderUrls = ord .
    aggregate (p9 ( groupBy -- These should all be the same so group by everything
                  , groupBy
                  , groupBy
                  , groupBy
                  , groupBy
                  , groupBy
                  , groupBy
                  , groupBy
                  , countStar
                  )
              )
  where
    ord :: Query ( Column PGInt4, Column PGText, Column PGText, Column PGText
                 , Column PGText, Column PGText, Column PGText, Column PGTimestamptz
                 , Column PGInt8
                 )
        -> Query ( Column PGInt4, Column PGText, Column PGText, Column PGText
                 , Column PGText, Column PGText, Column PGText, Column PGTimestamptz
                 , Column PGInt8
                 )
    ord = orderBy (desc (\t -> t^._9))

redditCommentUrlQuery :: Query URLColumn
redditCommentUrlQuery = proc () -> do
  url <- urlQuery -< ()
  urc <- urlRedditCommentQuery -< ()
  restrict -< url^.urlId .== urc^.urcUrlId
  returnA -< url

getMostPopularRedditUrlsQuery :: Query (URLColumn, Column PGInt8)
getMostPopularRedditUrlsQuery = proc () -> do
    urls  <- (groupOrderUrls (convertUrl redditCommentUrlQuery)) -< ()
    returnA -< (URL (urls^._1) (urls^._2) (urls^._3) (urls^._4) (urls^._5) (urls^._6)
                    (urls^._7) (urls^._8), urls^._9)
  where
    convertUrl :: Query URLColumn
               -> Query ( Column PGInt4, Column PGText, Column PGText, Column PGText
                        , Column PGText, Column PGText, Column PGText, Column PGTimestamptz
                        , Column PGInt4
                        )
    convertUrl q = proc () -> do
      url <- q -< ()
      returnA -< ( url^.urlId
                 , url^.urlFull
                 , url^.urlProtocol
                 , url^.urlDomain
                 , url^.urlPath
                 , url^.urlQueryString
                 , url^.urlAnchor
                 , url^.urlCreatedUtc
                 , url^.urlId
                 )

    firstQuery = proc () -> do
      url <- urlQuery -< ()
      urc <- urlRedditCommentQuery -< ()
      restrict -< url^.urlId .== urc^.urcUrlId
      returnA -< url

getMostPopularRedditUrls :: HasPostgres m => Int -> Int -> m [(URL, Int64)]
getMostPopularRedditUrls lim off =
  runQuery (limit lim (offset off getMostPopularRedditUrlsQuery))

createUrlRedditComment :: HasPostgres m => Int -> Text -> m Int64
createUrlRedditComment urlId rcId =
  runInsertMany urlRedditCommentTable [ URLRedditComment (pgInt4 urlId) (pgStrictText rcId) ]

createUrlRedditCommentIfNotExists :: HasPostgres m => Int -> Text -> m Int64
createUrlRedditCommentIfNotExists urlId rcId = do
  urcs <- getUrlRedditComment urlId rcId
  if null urcs then createUrlRedditComment urlId rcId else return 0

listRedditUrlsQuery :: Int -> Query URLColumn
listRedditUrlsQuery lim = limit lim $ proc () -> do
  url <- urlQuery -< ()
  urlReddit <- urlRedditCommentQuery -< ()
  restrict -< url^.urlId .== urlReddit^.urcUrlId
  returnA -< url

listRedditUrls :: HasPostgres m => Int -> m [URL]
listRedditUrls lim = runQuery (listRedditUrlsQuery lim)

type RedditCommentColumn =
  RedditComment' (Column PGText) (Column PGText) (Column PGInt4) (Column PGInt4) (Column PGInt4)
                 (Column PGInt4) (Column PGFloat8) (Column PGText) (Column PGText)
                 (Column (Nullable PGTimestamptz)) (Column PGBool) (Column PGText) (Column PGText)
                 (Column (Nullable PGText)) (Column PGText) (Column PGTimestamptz)
                 (Column PGTimestamptz)

$(makeAdaptorAndInstance "pRedditComment" ''RedditComment')

redditCommentTable :: Table RedditCommentColumn RedditCommentColumn
redditCommentTable = Table "comments" $ pRedditComment RedditComment
  { _rcId = required "id"
  , _rcName = required "name"
  , _rcUps = required "ups"
  , _rcDowns = required "downs"
  , _rcScore = required "score"
  , _rcGilded = required "gilded"
  , _rcControversiality = required "controversiality"
  , _rcBody = required "body"
  , _rcBodyHtml = required "bodyhtml"
  , _rcEdited = required "edited"
  , _rcScoreHidden = required "scorehidden"
  , _rcUserId = required "userid"
  , _rcLinkId = required "linkid"
  , _rcParentComment = required "parentcomment"
  , _rcSubredditId = required "subredditid"
  , _rcCreatedUtc = required "createutc"
  , _rcModifiedUtc = required "modifiedutc"
  }

redditCommentQuery :: Query RedditCommentColumn
redditCommentQuery = queryTable redditCommentTable

listRedditCommentsQuery :: UTCTime -> Int -> Query RedditCommentColumn
listRedditCommentsQuery after lim = limit lim . orderBy (asc _rcModifiedUtc) $ q
  where
    q = proc () -> do
          rc <- redditCommentQuery -< ()
          restrict -< rc^.rcModifiedUtc .> pgUTCTime after
          returnA -< rc

listRedditComments :: HasPostgres m => UTCTime -> Int -> m [RedditComment]
listRedditComments after lim = runQuery (listRedditCommentsQuery after lim)

getRedditCommentsQuery :: Column PGInt4 -> Query RedditCommentColumn
getRedditCommentsQuery urlId = proc () -> do
  rc <- redditCommentQuery -< ()
  urc <- urlRedditCommentQuery -< ()
  restrict -< urc^.urcUrlId .== urlId .&& rc^.rcId .== urc^.urcCommentId
  returnA -< rc

getRedditComments' :: HasPostgres m => Int -> m [RedditComment]
getRedditComments' urlId = runQuery (getRedditCommentsQuery (pgInt4 urlId))

-- TODO: see if we can batch these better
getRedditComments :: HasPostgres m => [Int] -> m [RedditComment]
getRedditComments urlIds = concat <$> forM urlIds getRedditComments'

getMostRecentRedditCommentQuery :: Query RedditCommentColumn
getMostRecentRedditCommentQuery =
  limit 1 . orderBy (desc (\c -> c^.rcModifiedUtc)) $
    proc () -> do
      urc <- urlRedditCommentQuery -< ()
      com <- redditCommentQuery -< ()
      restrict -< urc^.urcCommentId .== com^.rcId
      returnA -< com

getMostRecentRedditComment :: HasPostgres m => m (Maybe RedditComment)
getMostRecentRedditComment = do
  comms <- runQuery getMostRecentRedditCommentQuery
  return $ headMay comms
