{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module DataAccessEnvTest
  ( test
  ) where

import ClassyPrelude

import Control.Error

import Database.PostgreSQL.Simple

import DataAccess
import Utility
import qualified Comment as C
import qualified Link as L
import qualified Subreddit as S
import qualified User as U

fakeUser :: U.User
fakeUser = U.User "fake-user" uneededTime

fakeSubreddit :: S.Subreddit
fakeSubreddit =
    S.Subreddit
        { S.id_ = "foo"
        , S.title = "some-sub"
        , S.name = "subber"
        , S.displayName = "subber subs"
        , S.url = "r/subber"
        , S.description = "sub reddit for testing"
        , S.subscribers = 200
        , S.subredditType = "test sub"
        , S.over18 = False
        , S.publicTraffic = True
        , S.submissionType = "idk"
        , S.submitText = "idk"
        , S.createdUtc = uneededTime
        , S.modifiedUtc = uneededTime
        }

fakeLink :: L.Link
fakeLink =
    L.Link
        { L.id_ = "someid"
        , L.name = "t5_someid"
        , L.title = "Look at my cat or something"
        , L.ups = 10
        , L.downs = 3
        , L.score = 7
        , L.gilded = 0
        , L.url = "some url"
        , L.domain = "buzzfeed.com"
        , L.permalink = "http://link.com"
        , L.hidden = False
        , L.isSelf = False
        , L.numComments = 10
        , L.over18 = False
        , L.selftext = ""
        , L.selftextHtml = Nothing
        , L.stickied = False
        , L.thumbnail = "idk"
        , L.edited = Nothing
        , L.userId = U.id_ fakeUser
        , L.subredditId = S.id_ fakeSubreddit
        , L.createdUtc = uneededTime
        , L.modifiedUtc = uneededTime
        }

fakeTopLevelComment :: C.Comment
fakeTopLevelComment =
    C.Comment
        { C.id_ = "something"
        , C.name = "another"
        , C.ups = 3
        , C.downs = 0
        , C.score = 3
        , C.gilded = 0
        , C.controversiality = 0.1
        , C.body = "hurr durr"
        , C.bodyHtml = "le hurr durr"
        , C.edited = Nothing
        , C.scoreHidden = False
        , C.userId = U.id_ fakeUser
        , C.linkId = L.id_ fakeLink
        , C.parentComment = Nothing
        , C.subredditId = S.id_ fakeSubreddit
        , C.createdUtc = uneededTime
        , C.modifiedUtc = uneededTime
        }

fakeReply :: C.Comment
fakeReply =
    C.Comment
        { C.id_ = "something-else"
        , C.name = "another"
        , C.ups = 3
        , C.downs = 0
        , C.score = 3
        , C.gilded = 0
        , C.controversiality = 0.1
        , C.body = "look at this reply"
        , C.bodyHtml = "look at this reply"
        , C.edited = Nothing
        , C.scoreHidden = False
        , C.userId = U.id_ fakeUser
        , C.linkId = L.id_ fakeLink
        , C.parentComment = Just (C.id_ fakeTopLevelComment)
        , C.subredditId = S.id_ fakeSubreddit
        , C.createdUtc = uneededTime
        , C.modifiedUtc = uneededTime
        }

test :: IO ()
test = do

    eConnInfo <- connInfoFromFile "db.conf"

    case eConnInfo of
        Left e -> putStrLn e
        Right ci -> do

            conn <- connect ci

            dropTables conn

            putStrLn "Create tables"
            putStrLn "User created"
            void $ execute_ conn U.createQuery
            putStrLn "User subreddit"
            void $ execute_ conn S.createQuery
            putStrLn "User link"
            void $ execute_ conn L.createQuery
            putStrLn "User comment"
            void $ execute_ conn C.createQuery

            putStrLn "Insert user"
            uRows <- putUser conn fakeUser
            putStrLn "Insert subreddit"
            sRows <- putSubreddit conn fakeSubreddit
            putStrLn "Insert link"
            lRows <- putLink conn fakeLink
            putStrLn "Insert toplevel comment"
            cRows <- putComment conn fakeTopLevelComment
            putStrLn "Insert reply"
            rRows <- putComment conn fakeReply

            putStrLn "Get user"
            mComp <- runMaybeT $ do
                user <- MaybeT $ getUser conn (U.id_ fakeUser)
                putStrLn "Get subreddit"
                subreddit <- MaybeT $ getSubreddit conn (S.id_ fakeSubreddit)
                putStrLn "Get link"
                link <- MaybeT $ getLink conn (L.id_ fakeLink)
                putStrLn "Get toplevel comment"
                comment <- MaybeT $ getComment conn (C.id_ fakeTopLevelComment)
                putStrLn "Get toplevel reply"
                reply <- MaybeT $ getComment conn (C.id_ fakeReply)

                putStrLn "Compare user"
                assert (user == fakeUser) return ()
                putStrLn "Compare subreddit"
                assert (subreddit == fakeSubreddit) return ()
                putStrLn "Compare link"
                assert (link == fakeLink) return ()
                putStrLn "Compare comment"
                assert (comment == fakeTopLevelComment) return ()
                putStrLn "Compare reply"
                assert (reply == fakeReply) return ()
            case mComp of
                Nothing -> putStrLn "Read failures" >> dropTables conn
                Just () -> dropTables conn

dropTables :: Connection -> IO ()
dropTables conn = do
    putStrLn "Clean up tables"
    void $ execute_ conn "drop table comments"
    void $ execute_ conn "drop table links"
    void $ execute_ conn "drop table subreddits"
    void $ execute_ conn "drop table users"



