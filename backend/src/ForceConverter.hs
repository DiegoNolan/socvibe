{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module ForceConverter
  ( Edge
  , nodeA
  , nodeB
  , value
  , toNormDirGraph
  , mkEdge
  , edgeMap
  , addEdges
  , testMap
  , combine
  , normalize
  ) where

import ClassyPrelude
import Data.List (nubBy)
import qualified Data.Map as Map

data Edge = Edge
  { nodeA     :: !Text
  , nodeB     :: !Text
  , value     :: !Float
  } deriving (Show, Eq, Ord)

toNormDirGraph :: Map.Map Text (Map.Map Text Float)
               -> ( [(Text,())], [(Text,Text,Float)] )
toNormDirGraph sims = ( zip (Map.keys sims) (repeat ())
                      , edges -- map (\(Edge a b v) -> (a,b,v)) edges
                      )
  where es =    --(atleastOneHeap .
                (limitTo 17 .
                -- (filter (\e -> value e > 0.000005) .
                combine . normalize) sims
        edges = map (\(Edge na nb v) -> (na,nb,v)) es

atleastOne :: [Edge] -> [Edge]
atleastOne es = nubBy (\(Edge a1 b1 _) (Edge a2 b2 _) -> a1 == a2 && b1 == b2)
                              (Map.elems highest)
  where highest = foldl' (\m e@(Edge a b _) ->
                             Map.insertWith keepHighest b e
                             (Map.insertWith keepHighest a e m)
                         ) Map.empty es
        keepHighest :: Edge -> Edge -> Edge
        keepHighest e1@(Edge _ _ v1) e2@(Edge _ _ v2)
            | v1 > v2 = e1
            | otherwise = e2

limitTo :: Int -> [Edge] -> [Edge]
limitTo n es =
    case heaps of
      Nothing -> error "Should not happen"
      Just hs ->   map unsafeHead . filter (\g -> length g == 2)
                 . group . sort
                 $ foldl' (\acc eh -> edges_ eh ++ acc) [] hs
  where heaps = foldM (\m e@(Edge na nb _) -> do
                           ha <- Map.lookup na m <|> Just (EdgeHeap [])
                           hb <- Map.lookup nb m <|> Just (EdgeHeap [])
                           let nha = insertLimitSize n e ha
                               nhb = insertLimitSize n e hb
                           return (Map.insert nb nhb (Map.insert na nha m))
                      ) Map.empty es

data EdgeHeap = EdgeHeap
  { edges_ :: [Edge]
  }

insertEdge :: Edge -> EdgeHeap -> EdgeHeap
insertEdge edge edgeHeap = EdgeHeap (ins edge (edges_ edgeHeap))
  where ins e (eh:rest)
            | value e <= value eh = e:eh:rest
            | otherwise           = eh : ins e rest
        ins e _ = [e]

insertLimitSize :: Int -> Edge -> EdgeHeap -> EdgeHeap
insertLimitSize n e eh =
    (EdgeHeap . reverse . take n . reverse . edges_)
    (insertEdge  e eh)

mkEdge :: Text -> Text -> Float -> Edge
mkEdge na nb v
    | na > nb       = Edge nb na v
    | otherwise     = Edge na nb v

edgeMap :: (Float -> Float) -> Edge -> Edge
edgeMap f (Edge a b v) = Edge a b (f v) 

addEdges :: Edge -> Edge -> Edge
addEdges (Edge a1 b1 v1) (Edge _ _ v2) = mkEdge a1 b1 (v1+v2)

testMap :: Map.Map Text (Map.Map Text Float)
testMap = Map.fromList
    [ ("Foo", Map.fromList
        [ ("pics", 100)
        , ("funny", 10)
        ]
      ), 
      ("funny", Map.fromList
        [ ("Foo", 5)
        , ("pics", 10)
        ]
      ),
      ("pics", Map.fromList
        [ ("Foo", 15)
        , ("funny", 20)
        ]
      )
    ]

combine :: Map.Map Text (Map.Map Text Float) -> [Edge]
combine m = combined
  where reduced = Map.foldWithKey (\na im es ->
                    Map.elems (Map.mapWithKey (mkEdge na) im) ++ es)
                    [] m
        grouped = groupBy (\(Edge a1 b1 _) (Edge a2 b2 _) -> (a1,b1) == (a2,b2)) $
                        sortBy (\(Edge a1 b1 _) (Edge a2 b2 _) -> compare (a1,b1) (a2,b2)
                               ) reduced
        combined = fmap (\g -> case g of
                                [s] -> s
                                (f:rest) -> foldl' addEdges f rest
                                _ -> error "Empty list : this should be impossible"
                        ) grouped

normalize :: Map.Map Text (Map.Map Text Float) -> Map.Map Text (Map.Map Text Float)
normalize = fmap func
  where func iMap = Map.map (/ total) iMap
          where total = Map.foldl' (+) 0 iMap
