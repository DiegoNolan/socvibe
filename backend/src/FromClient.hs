{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module FromClient
  ( subredditFromClient
  , linkFromClient
  , commentFromClient
  ) where

import ClassyPrelude
import qualified Reddit.Comment as RC
import qualified Reddit.Link as RL
import qualified Reddit.Subreddit as RS
import qualified Thing.Comment as C
import qualified Thing.Link as L
import qualified Thing.Subreddit as S
import Utility (uneededTime)

subredditFromClient :: RS.Subreddit -> S.Subreddit
subredditFromClient RS.Subreddit{..} =
  S.Subreddit
    { S.id_ = id
    , S.title = title
    , S.name = name
    , S.displayName = display_name
    , S.url = url
    , S.description = description
    , S.subscribers = subscribers
    , S.subredditType = subreddit_type
    , S.over18 = over18
    , S.publicTraffic = public_traffic
    , S.submissionType = submission_type
    , S.submitText = submit_text
    , S.createdUtc = created_utc
    , S.modifiedUtc = uneededTime
    }

linkFromClient :: RL.Link -> L.Link
linkFromClient RL.Link{..} =
  L.Link
    { L.id_ = id
    , L.name = name
    , L.title = title
    , L.ups = ups
    , L.downs = downs
    , L.score = score
    , L.gilded = gilded
    , L.url = url
    , L.domain = domain
    , L.permalink = permalink
    , L.hidden = hidden
    , L.isSelf = is_self
    , L.numComments = num_comments
    , L.over18 = over_18
    , L.selftext = selftext
    , L.selftextHtml = selftext_html
    , L.stickied = stickied
    , L.thumbnail = thumbnail
    , L.edited = edited
    , L.userId = author
    , L.subredditId = drop 3 subreddit_id -- drop the thing and underscore, ie t5_uhete
    , L.createdUtc = created_utc
    , L.modifiedUtc = uneededTime
    }

commentFromClient :: RC.FC -> Maybe Text -> C.Comment
commentFromClient RC.FC{..} parentComment =
  C.Comment
    { C.id_ = id
    , C.name = name
    , C.ups = ups
    , C.downs = downs
    , C.score = score
    , C.gilded = gilded
    , C.controversiality = controversiality
    , C.body = body
    , C.bodyHtml = body_html
    , C.edited = edited
    , C.scoreHidden = score_hidden
    , C.userId = author
    , C.linkId = drop 3 link_id
    , C.parentComment = parentComment
    , C.subredditId = drop 3 subreddit_id -- drop the thing and underscore
    , C.createdUtc = created_utc
    , C.modifiedUtc = uneededTime
    }


