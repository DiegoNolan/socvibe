{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
module Handlers where

import ClassyPrelude hiding (for_)
import Control.Lens
import DataAccess.Contact
import Lucid
import Lucid.Base
import Lucid.Bootstrap
import Sitemap

contactRow :: Monad m => HtmlT m ()
contactRow =
  div_ [ class_ "row" ] $ do
    div_ [ class_ "col-lg-8 col-lg-offset-2" ] $ do
      form_ [ name_ "sentMessage", id_ "contactForm", novalidate_ "" ]  $ do
        div_ [ class_ "row control-group" ] $ do
          div_ [ class_ "form-group col-xs-12 floating-label-form-group controls" ] $ do
            label_ [ for_ "name" ] "Name"
            input_ [ type_ "text", class_ "form-control", placeholder_ "Name"
                    , id_ "name", required_ ""
                    , data_validation_required_message_ "Please enter your name."
                    ]
            p_ [ class_ "help-block text-danger" ] ""
        div_ [ class_ "row control-group" ] $ do
          div_ [ class_ "form-group col-xs-12 floating-label-form-group controls" ] $ do
            label_ [ for_ "email" ] "Email Address"
            input_ [ type_ "email", class_ "form-control", placeholder_ "Email Address"
                    , id_ "email", required_ ""
                    , data_validation_required_message_ "Please enter your email address." ]
            p_ [ class_ "help-block text-danger" ] ""
        div_ [ class_ "row control-group" ] $ do
          div_ [ class_ "form-group col-xs-12 floating-label-form-group controls" ] $ do
            label_ [ for_ "message" ] "Message"
            textarea_ [ rows_ "5", class_ "form-control", placeholder_ "Message"
                      , id_ "message"
                      , required_ ""
                      , data_validation_required_message_ "Please enter a message." ] ""
            p_ [ class_ "help-block text-danger" ] ""
        br_ []
        div_ [ id_ "success" ] ""
        div_ [ class_ "row" ] $ do
          div_ [ class_ "form-group col-xs-12" ] $ do
            button_ [ type_ "submit", class_ "btn btn-success btn-lg" ] "Send"

indexHandler :: AppHandler ()
indexHandler = lucid $ masterPage (return ()) endScript $ do
  header_ $ do
    div_ [ class_ "container", id_ "maincontent", tabindex_ "-1" ] $ do
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-lg-12" ] $ do
          -- img_ [ class_ "img-responsive", src_ "img/profile.png", alt_ "" ]
          div_ [ class_ "intro-text" ] $ do
            h1_ [ class_ "name" ] "Social Vibe"
            hr_ [ class_ "star-light" ]
            span_ [ class_ "skills" ] "Social Media Monitoring - Social Media Management"

  section_ [ id_ "platforms" ] $ do
    div_ [ class_ "container" ] $ do
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-lg-12 text-center" ] $ do
          h2_ "Supported Platforms"
          hr_ [ class_ "star-primary" ]
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-sm-4 text-center" ] $ do
          i_ [ class_ "fa fa-twitter fa-icon" ] ""
          h3_ "Twitter"
        div_ [ class_ "col-sm-4 text-center" ] $ do
          i_ [ class_ "fa fa-reddit fa-icon" ] ""
          h3_ "Reddit"
        div_ [ class_ "col-sm-4 text-center" ] $ do
          i_ [ class_ "fa fa-hacker-news fa-icon" ] ""
          h3_ "Hacker News"

  section_ [ class_ "success", id_ "about" ] $ do
    div_ [ class_ "container" ] $ do
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-lg-12 text-center" ] $ do
          h2_ "About"
          hr_ [ class_ "star-light" ]

      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-lg-4 col-lg-offset-2" ] $ do
          p_  "SocVibe allows you track links and keywords on major social media platforms \
              \and receive email alerts linking to the discussion threads about the content \
              \you are tracking."
        div_ [ class_ "col-lg-4" ] $ do
          p_ "Enables companies to track sentiment about their products and sites and respond \
             \accordingly. Helping them improve their business and better fit their customer needs."
        div_ [ class_ "col-lg-8 col-lg-offset-2 text-center" ] $ do
          a_ [ href_ "/signup", class_ "btn btn-lg btn-outline" ] $ do
            i_ [ class_ "fa fa-sign-in" ] ""
            " Signup Now!"

  section_ [ id_ "contact" ] $ do
    div_ [ class_ "container" ] $ do
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-lg-12 text-center" ] $ do
          h2_ "Contact"
          hr_ [ class_ "star-primary" ]
      contactRow

  footer_ [ class_ "text-center" ] $ do
{-
    div_ [ class_ "footer-above" ] $ do
      div_ [ class_ "container" ] $ do
        div_ [ class_ "row" ] $ do
          div_ [ class_ "footer-col col-md-4" ] $ do
            h3_ "Location"
            p_ "3481 Melrose Place"
            br_ []
            p_ "Beverly Hills, CA 90210"
          div_ [ class_ "footer-col col-md-4" ] $ do
            h3_ "Around the Web"
            ul_ [ class_ "list-inline" ] $ do
              li_ $ do
                a_ [ href_ "#", class_ "btn-social btn-outline" ] $ do
                  span_ [ class_ "sr-only"] "Facebook"
                  i_ [ class_ "fa fa-fw fa-facebook" ] ""
              li_ $ do
                a_ [ href_ "#", class_ "btn-social btn-outline" ] $ do
                  span_ [ class_ "sr-only"] "Google Plus"
                  i_ [ class_ "fa fa-fw fa-google-plus" ] ""
          div_ [ class_ "footer-col col-md-4" ] $ do
            h3_ "About Freelancer"
            p_ "Freelance is a free to use, open source Bootstrap theme created by "
            a_ [ href_ "http://startbootstrap.com" ] "Start Bootstrap"
            p_ "."
-}
    div_ [ class_ "footer-below" ] $ do
      div_ [ class_ "container" ] $ do
        div_ [ class_ "row" ] $ do
          div_ [ class_ "col-lg-12" ] $ do
            "Copyright "
            i_ [ class_ "fa fa-copyright" ] ""
            " SocVibe 2016"

  where
    endScript = do
      script_ [ src_ "https://cdnjs.cloudflare.com/ajax/libs/jqBootstrapValidation/1.3.7/jqBootstrapValidation.min.js" ] ""
      script_ [ src_ "/js/contact.js" ] ""

contactHandler :: AppHandler ()
contactHandler = method POST $ do
  putStrLn "contact"
  Just name <- fmap decodeUtf8 <$> getParam "name"
  Just email <- fmap decodeUtf8 <$> getParam "email"
  Just message <- fmap decodeUtf8 <$> getParam "message"
  now <- liftIO $ getCurrentTime
  createContact $ Contact 0 name email message now
  finishWith emptyResponse

signupHandler :: AppHandler ()
signupHandler = lucid $ masterPage (return ()) endScript $ do
    topPadding
    section_ [ id_ "contact" ] $ do
      div_ [ class_ "container" ] $ do
        div_ [ class_ "row", style_ "margin-bottom: 25px;" ] $ do
          div_ [ class_ "col-xs-12 text-center" ] $ do
            img_ [ src_ "/images/construction.jpg", alt_ "under construction"
                 , style_ "max-width: 100%;" ]
        div_ [ class_ "row", style_ "margin-bottom: 25px;" ] $ do
          div_ [ class_ "col-md-8 col-md-offset-2" ] $ do
            p_ "SocVibe is still under developement. We're working quickly to get everything ready. \
               \Please drop us a line and we'll let you know when everything is ready!"

        contactRow
  where
    endScript = do
      script_ [ src_ "https://cdnjs.cloudflare.com/ajax/libs/jqBootstrapValidation/1.3.7/jqBootstrapValidation.min.js" ] ""
      script_ [ src_ "/js/contact.js" ] ""

fourOhFourHandler :: AppHandler ()
fourOhFourHandler = lucid $ masterPage (return ()) (return ()) $ do
  topPadding
  div_ [ class_ "container" ] $
    h3_ "Not found"

sitemapHandler :: AppHandler ()
sitemapHandler = do
  modifyResponse $
      setResponseCode 200
    . setContentType "text/xml"
  writeText (pack sitemap)

robotsHandler :: AppHandler ()
robotsHandler = do
    modifyResponse $
        setResponseCode 200
      . setContentType "text/plain"
    writeText robots
  where
    robots =
      "User-agent: *\n" ++
      "Sitemap: http://socvibe.com/sitemap.xml\n"

redditThreadHandler :: AppHandler ()
redditThreadHandler = lucid $ masterPage (return ()) (return ()) $ do
  topPadding
  div_ [ class_ "container" ] $
    div_ [ id_ "reddit" ] (return ())
  script_  [ src_ "/js/reddit-thread.js" ] ""

urlsHandler :: AppHandler ()
urlsHandler = lucid $ masterPage (return ()) (return ()) $ do
  topPadding
  div_ [ class_ "container" ] $
    div_ [ id_ "urls" ] (return ())
  script_  [ src_ "/js/urls.js" ] ""

graphHandler :: AppHandler ()
graphHandler = lucid $ masterPage (return ()) (return ()) $
  object_ [ type_ "image/svg+xhl", object_data_ "/graph.svg" ] ""

crawlerDashboardHandler :: AppHandler ()
crawlerDashboardHandler = lucid $ masterPage (return ()) (return ()) $ do
  topPadding
  div_ [ class_ "container" ] $
    div_ [ id_ "dashboard" ] (return ())
  script_  [ src_ "/js/crawler-dashboard.js" ] ""

adminContactHandler :: AppHandler ()
adminContactHandler = do
  contacts <- listContacts
  lucid $ masterPage (return ()) (return ()) $ do
    topPadding
    div_ [ class_ "container" ] $ do
      div_ [ class_ "row" ] $ do
        div_ [ class_ "col-xs-12" ] $ do
          table_ [ class_ "table table-striped" ] $ do
            tr_ $ do
              th_ "Id"
              th_ "Name"
              th_ "Email"
              th_ "Message"
              th_ "Time"
            forM_ contacts (\contact -> do
              tr_ $ do
                td_ (toHtml (tshow  (contact^.conId)))
                td_ (toHtml (contact^.conName))
                td_ (toHtml (contact^.conEmail))
                td_ (toHtml (contact^.conMessage))
                td_ (toHtml (tshow (contact^.conCreatedUtc)))
                  )

adminHandler :: AppHandler ()
adminHandler = lucid $ masterPage (return ()) (return ()) $ do
  topPadding
  div_ [ class_ "container" ] $ do
    div_ [ class_ "row" ] $ do
      div_ [ class_ "col-xs-12" ] $
        a_ [ href_ "/admin/crawler-dashboard" ] "Crawler Dashboard"
      div_ [ class_ "col-xs-12" ] $
        a_ [ href_ "/admin/contacts" ] "Contact"

object_data_ :: Text -> Attribute
object_data_ = makeAttribute "data"

topPadding :: Monad m => HtmlT m ()
topPadding = div_ [ style_ "display: block; height: 100px; " ] ""

masterPage :: Monad m => HtmlT m () -> HtmlT m () -> HtmlT m () -> HtmlT m ()
masterPage extraHead endScripts inner =
  html_ $ do
    head_ $ do
      meta_ [ charset_ "utf-8" ]
      meta_ [ name_ "viewport", content_ "width=device-width, initial-scale=1"]

      title_ "SocVibe - Track engagement on social media"

      link_ [ href_ "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css"
            , rel_ "stylesheet"
            ]
      link_ [ href_ "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
            , rel_ "stylesheet"
            ]
      link_ [ href_ "/css/site.css", rel_ "stylesheet" ]
      googleAnalytics
    body_ [ id_ "page-top", class_ "index" ] $ do
      div_ [ id_ "skipnav" ] $ a_ [ href_ "#maincontent"] $ "Skip to main content"

      nav_ [ id_ "mainNav", class_ "navbar navbar-default navbar-fixed-top navbar-custom" ] $ do
        div_ [ class_ "container" ] $ do
          div_ [ class_ "navbar-header page-scroll" ] $ do
            button_ [ type_ "button", class_ "navbar-toggle"
                    , data_toggle_ "collapse", data_target_ "#bs-example-navbar-collapse-1"
                    ] $ do
              span_ [ class_ "sr-only" ] "Toggle navigation"
              "Menu"
              i_ [ class_ "fa fa-bars" ] ""
            a_ [ class_ "navbar-brand",  href_ "/" ] $ do
              span_ [ class_ "soc" ] "Soc"
              span_ "Vibe"

          div_ [ class_ "collapse navbar-collapse", id_ "bs-example-navbar-collapse-1" ] $ do
            ul_ [ class_ "nav navbar-nav navbar-right" ] $ do
              li_ [ class_ "hidden" ] $ a_ [ href_ "#page-top" ] ""
              li_ [ class_ "page-scroll" ] $ a_ [ href_ "/#platforms" ] "Platforms"
              li_ [ class_ "page-scroll" ] $  a_ [ href_ "/#about" ] "About"
              li_ [ class_ "page-scroll" ] $  a_ [ href_ "/#contact" ] "Contact"

      inner

      script_ [ src_ "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" ] ""
      script_ [ src_ "https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js" ] ""
      script_ [ src_ "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"] ""
      script_ [ src_ "/js/site.js" ] ""

      endScripts

googleAnalytics :: Monad m => HtmlT m ()
googleAnalytics = toHtmlRaw (
  "<script>\
  \(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\
  \(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\
  \m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\
  \})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');\

  \ga('create', 'UA-53569956-5', 'auto');\
  \ga('send', 'pageview');\
  \</script>" :: Text)

redditThreadInfoHandler :: AppHandler ()
redditThreadInfoHandler = lucid $ masterPage extraHead (return ()) $ infoRaw
  where
    extraHead :: Monad m => HtmlT m ()
    extraHead =
      link_ [ href_ "/css/markdown.css"
            , rel_ "stylesheet"
            ]

infoRaw :: Monad m => HtmlT m ()
infoRaw = toHtmlRaw (
  "<h1 id=\"how-do-i-interpret-this-visualization\">How do I interpret this visualization?</h1>\
  \<p>The left side of the visualization represents the time the Reddit post was originally created. Each rectangle represents a comment on the reddit post. The width of the rectangle represents how long the user took to respons to either the original post or the parent comment. So comments that were posted very shortly after their parent are very short while comments that are posted well after their parent are long. Since some comments are posted within seconds while others are posted within days it sometimes helps to display the length of the comments in a logrithimic scale so they can fit within the screen.</p>\
  \<p>All comments with no replies have the same unit height. A comment with 2 replies has 2 unit heights. A comment with 3 replies has 3 unit heights. A comment with 2 comments with 2 replies each has a unit height of 4. With this structure the height of the comment represents how many different discussions a parent comment has created. A very long comment chain represents a comment that has sparked a very long discussion.</p>\
  \<p>When the score color option is selected green comments are highly upvoted comments while low score comments are red. For the depth option comments that are close to the parent post are red while comments further from the parent post are purple. The user option gives each user commenting their own color. This option usually isn't that useful for threads with more than 30 comments because the colors become too hard to distinguish.</p>\
  \<p>You can click on the comments rectangles and a modal box will appear with the comment the name of the Reddit user who posted it and a permalink to the comment on Reddit.</p>\
  \<h2 id=\"examples\">Examples</h2>\
  \<p>It is usually really easy to see particularly unliked comments. With the score color option they will appear dark red because they are highly downvoted compared to the other comments in the reddit thread.</p>\
  \<div class=\"figure\">\
  \<img src=\"/images/bad.png\" alt=\"A really bad Reddit comment.\" />\
  \<p class=\"caption\">A really bad Reddit comment.</p>\
  \</div>\
  \<p>Certain threads have different properties. Ask Me Anything threads are interesting. For the most part the comments closer to the parent post have higher scores while older comments have lower probably because move people view them and then can upvote them. AMAs, however, usually always upvote the OPs comments so everyone can see them. This causes replies to have a higher score than their parents.</p>\
  \<div class=\"figure\">\
  \<img src=\"/images/ama.png\" alt=\"A Reddit AMA thread.\" />\
  \<p class=\"caption\">A Reddit AMA thread.</p>\
  \</div>\
  \<p>The vizualization also let's you see comments that sparked a lot of discussion. Here is an emample of a top level comment that has created a ton of replies.</p>\
  \<div class=\"figure\">\
  \<img src=\"/images/spark.png\" alt=\"A Reddit comment sparking a lot of discussion.\" />\
  \<p class=\"caption\">A Reddit comment sparking a lot of discussion.</p>\
  \</div>\
  \<h2 id=\"conclusion\">Conclusion</h2>\
  \<p>This layout allows you visualize the comment layout from a higher level and maybe find some interesting comments you wouldn't find just by browsing the comments normally.</p>" :: Text )
