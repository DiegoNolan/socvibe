{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib.Http where

import ClassyPrelude
import Network.HTTP.Client
import Network.HTTP.Client.TLS

class MonadIO m => HasManager m where
  getManager :: m Manager

instance MonadIO m => HasManager (ReaderT Manager m) where
  getManager = ask

runTls m = do
  mgr <- newManager tlsManagerSettings
  runReaderT m mgr
