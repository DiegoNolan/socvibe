{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.Postgres
  ( HasPostgres (..)
  , Postgres(..)
  , connect
  , runPG
  , execute
  , execute_
  , query
  , query_
  , runQuery
  , runQueryExplicit
  , runInsertMany
  , runInsert_
  , runInsertManyReturning
  , runUpdate
  , runUpdateReturning
  , runUpdate_
  , runDelete
  ) where

import ClassyPrelude
import Control.Lens
import Data.Pool
import Data.Aeson
import Data.Aeson.Lens
import Data.Profunctor.Product.Default
import qualified Database.PostgreSQL.Simple as PG
import qualified Opaleye as O
import Data.Pool

data Postgres =
    PostgresPool (Pool PG.Connection)
  | PostgresConn PG.Connection

class MonadIO m => HasPostgres m where
  getPostgresState :: m Postgres

instance HasPostgres (ReaderT Postgres IO) where
  getPostgresState = ask

connect :: IO PG.Connection
connect = do
  bs <- readFile "config/db.json"
  let Just connectHost = unpack <$> bs ^? key "host" . _String
      Just connectPort = fromIntegral <$> bs ^? key "port" . _Integral
      Just connectUser = unpack <$> bs ^? key "user" . _String
      Just connectPassword = unpack <$> bs ^? key "password" . _String
      Just connectDatabase = unpack <$> bs ^? key "database" . _String
  PG.connect PG.ConnectInfo{..}

runPG :: ReaderT Postgres IO a -> IO a
runPG m = do
  conn <- connect
  runReaderT m (PostgresConn conn)

execute :: (HasPostgres m, PG.ToRow q) => PG.Query -> q -> m Int64
execute q r = liftPG' (\conn -> PG.execute conn q r)

execute_ :: HasPostgres m => PG.Query -> m Int64
execute_ q = liftPG' (\conn -> PG.execute_ conn q)

query :: (HasPostgres m, PG.ToRow q, PG.FromRow r)
      => PG.Query
      -> q
      -> m [r]
query q r = liftPG' (\conn -> PG.query conn q r)

query_ :: (HasPostgres m, PG.FromRow r)
       => PG.Query
       -> m [r]
query_ q = liftPG' (`PG.query_` q)

runQuery :: (HasPostgres m, Default O.QueryRunner columns haskells)
         => O.Query columns
         -> m [haskells]
runQuery q = liftPG' (`O.runQuery` q)

runQueryExplicit :: (HasPostgres m)
                 => O.FromFields columns haskells
                 -> O.Query columns
                 -> m [haskells]
runQueryExplicit qr q = liftPG' (\conn -> O.runQueryExplicit qr conn q)

runInsertMany :: HasPostgres m
              => O.Table columns columns'
              -> [columns]
              -> m Int64
runInsertMany t cols = liftPG' (\conn -> O.runInsertMany conn t cols)

runInsert_  :: HasPostgres m
            => O.Insert haskells
            -> m haskells
runInsert_ ins =
  liftPG' (\conn -> O.runInsert_ conn ins)

runInsertManyReturning :: (HasPostgres m, Default O.QueryRunner returned haskells)
                       => O.Table columnsW columnsR
                       -> [columnsW]
                       -> (columnsR -> returned)
                       -> m [haskells]
runInsertManyReturning t cols f =
  liftPG' (\conn -> O.runInsertManyReturning conn t cols f)


runUpdate_ :: HasPostgres m
           => O.Update haskells
           -> m haskells
runUpdate_ up = liftPG' (\conn -> O.runUpdate_ conn up)

runUpdate :: HasPostgres m
          => O.Table columnsW columnsR
          -> (columnsR -> columnsW)
          -> (columnsR -> O.Column O.PGBool)
          -> m Int64
runUpdate t f g = liftPG' (\conn -> O.runUpdate conn t f g)

runUpdateReturning :: (HasPostgres m, Default O.QueryRunner returned haskells)
                   => O.Table columnsW columnsR
                   -> (columnsR -> columnsW)
                   -> (columnsR -> O.Column O.PGBool)
                   -> (columnsR -> returned)
                   -> m [haskells]
runUpdateReturning t f g r = liftPG' (\conn -> O.runUpdateReturning conn t f g r)

runDelete :: HasPostgres m
          => O.Table a columnsR
          -> (columnsR -> O.Column O.PGBool)
          -> m Int64
runDelete t f = liftPG' (\conn -> O.runDelete conn t f)

liftPG' :: HasPostgres m => (PG.Connection -> IO a) -> m a
liftPG' f = do
  s <- getPostgresState
  withConnection s f

------------------------------------------------------------------------------
-- | Convenience function for executing a function that needs a database
-- connection.
withConnection :: MonadIO m => Postgres -> (PG.Connection -> IO b) -> m b
withConnection (PostgresPool p) f = liftIO (withResource p f)
withConnection (PostgresConn c) f = liftIO (f c)
