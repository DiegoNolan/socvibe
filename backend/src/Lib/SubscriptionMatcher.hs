{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib.SubscriptionMatcher where

import ClassyPrelude
import Control.Lens
import qualified Data.Text as Text
import qualified Data.Text.Internal.Search as Text
import Network.URI
import Text.Regex.PCRE
import Lib.URLs
import API

