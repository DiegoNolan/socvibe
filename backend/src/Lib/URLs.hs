{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.URLs
  ( URL (..)
  , originalURL
  , parseURLs
  ) where

import ClassyPrelude hiding ((<|>), try, many)
import qualified Data.Text as Text
import Network.URI
import Text.Regex.PCRE

printE :: Either String b -> IO ()
printE (Left s) = putStrLn $ "left : " ++ pack s
printE _ = putStrLn "right"

urlRegex :: String
urlRegex = "(?:http|ftp|https)://([\\w_-]+(?:(?:\\.[\\w_-]+)+))([\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-])?"

extractURLs :: String -> [String]
extractURLs str = getAllTextMatches (str =~ urlRegex :: AllTextMatches [] String)

parseURLs :: Text -> [URL]
parseURLs = catMaybes . map (uriToURL <=< parseURI) . extractURLs . unpack
  where
    uriToURL :: URI -> Maybe URL
    uriToURL URI{..} =
      case uriAuthority of
        Nothing -> Nothing
        Just URIAuth{..} -> Just $
          URL (pack uriScheme) (pack $ uriUserInfo ++ uriRegName ++ uriPort)
              (pack uriPath) (pack uriQuery) (pack uriFragment)

originalURL :: URL -> Text
originalURL URL{..} = protocol ++ "//" ++ domain ++ path ++ queryString ++ anchor

data URL = URL
  { protocol :: Text
  , domain :: Text
  , path :: Text
  , queryString :: Text
  , anchor :: Text
  } deriving (Show, Eq)

