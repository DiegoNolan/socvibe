{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.Url
  ( URL (..)
  , originalURL
  , parseURLs
  ) where

import ClassyPrelude hiding ((<|>), many, try, optional)
import qualified Data.Text as Text
import Text.Parsec
import Text.Parsec.Char
import Text.Parsec.Text

data URL = URL
  { protocol :: Text
  , domain :: Text
  , path :: Text
  , queryString :: Text
  , anchor :: Text
  } deriving (Show, Eq)

originalURL :: URL -> Text
originalURL URL{..} = protocol ++ domain ++ path ++ queryString ++ anchor

parseURLs :: Text -> [URL]
parseURLs txt = do
  case parse urlsParser "text" txt of
    Left er -> error (show er)
    Right urls -> urls

data Stage = U URL | C Char | Done

urlsParser :: Parser [URL]
urlsParser = do
  mU <- (U <$> try url) <|> (C <$> try anyChar) <|> return Done
  case mU of
    Done -> return []
    C _ -> urlsParser
    U u -> do us <- urlsParser
              return $ u : us

url :: Parser URL
url = do
  s <- scheme
  _ <- string "://"
  domain <- authority
  path <- pathAbempty
  q <- option "" $ try $ do _ <- char '?'
                            q <- query
                            return $ "?" <> q
  f <- option "" $ try $ do _ <- char '#'
                            f <- fragment
                            return $ "#" <> f
  return $ URL (s <> "://") domain path q f

uri :: Parser Text
uri = do
  s <- scheme
  _ <- char ':'
  hp <- hierPart
  q <- option "" $ try $ do _ <- char '?'
                            q <- query
                            return $ "?" <> q
  f <- option "" $ try $ do _ <- char '#'
                            f <- fragment
                            return $ "#" <> f
  return $ s <> ":" <> hp <> q <> f

hierPart :: Parser Text
hierPart =
  ( try $ do s <- pack <$> string "//"
             a <- authority
             p <- pathAbempty
             return $ s <> a <> p
  )
  <|> try pathAbsolute
  <|> try pathRootless
  <|> try pathEmpty


   -- URI-reference = URI / relative-ref

   -- absolute-URI  = scheme ":" hier-part [ "?" query ]

   -- relative-ref  = relative-part [ "?" query ] [ "#" fragment ]

   -- relative-part = "//" authority path-abempty
                 -- / path-absolute
                 -- / path-noscheme
                 -- / path-empty

star :: Int -> Int -> Parser a -> Parser [a]
star mn mx p = do
    lead <- count mn p
    go (mx-mn) p
  where
    go i p
      | i <= 0    = return []
      | otherwise = do n <- p
                       r <- go (i-1) p
                       return (n : r)

scheme :: Parser Text
scheme = do
  a <- letter
  r <- many $ alphaNum <|> char '+' <|> char '-' <|> char '.'
  return $ pack $ a : r

authority :: Parser Text
authority = do
  ui <- option "" $ try $ do ui <- userInfo
                             _ <- char '@'
                             return $ ui <> "@"
  h <- host
  p <- option "" $ try $ do _ <- char ':'
                            p <- port
                            return $ ":" <> p
  return $ ui <> h <> p

userInfo :: Parser Text
userInfo = mconcat <$> many (
      try (Text.singleton <$> unreserved)
  <|> try pctEncoded
  <|> try (Text.singleton <$> subDelims)
  <|> try (pack <$> string ":")
  )

host :: Parser Text
host = iPLiteral <|> iPv4Address <|> regName

port :: Parser Text
port = pack <$> many digit

iPLiteral :: Parser Text
iPLiteral = do
  ob <- pack <$> string "["
  ip <- try iPv6Address <|> try iPvFuture
  cb <- pack <$> string "]"
  return $ ob <> ip <> cb

iPvFuture :: Parser Text
iPvFuture = do
  v <- string "v"
  hd <- many1 hexDigit
  p <- string "."
  r <- many1 $ unreserved <|> subDelims <|> char ':'
  return $ pack $ v <> hd <> p <> r

iPv6Address :: Parser Text
iPv6Address =
        try (run [ mconcat <$> count 6 hColon, ls32 ])
    <|> try (run [ pack <$> string "::", mconcat <$> count 5 hColon, ls32 ])
    <|> try (run [ pack <$> string "::"
                 , mconcat <$> count 5 hColon
                 , ls32 ])
    <|> try (run [ option "" (try h16)
                 , pack <$> string "::"
                 , mconcat <$> count 4 hColon
                 , ls32 ])
    <|> try (run [ option "" $ try $ run [ mconcat <$> star 0 1 hColon, h16 ]
                 , pack <$> string "::"
                 , mconcat <$> count 3 hColon
                 , ls32 ])
    <|> try (run [ option "" $ try $ run [ mconcat <$> star 0 2 hColon, h16 ]
                 , pack <$> string "::"
                 , mconcat <$> count 2 hColon
                 , ls32 ])
    <|> try (run [ option "" $ try $ run [ mconcat <$> star 0 3 hColon, h16 ]
                 , pack <$> string "::"
                 , hColon
                 , ls32 ])
    <|> try (run [ option "" $ try $ run [ mconcat <$> star 0 4 hColon, h16 ]
                 , pack <$> string "::"
                 , ls32 ])
    <|> try (run [ option "" $ try $ run [ mconcat <$> star 0 5 hColon, h16 ]
                 , pack <$> string "::"
                 , h16 ])
    <|> try (run [ option "" $ try $ run [ mconcat <$> star 0 6 hColon, h16 ]
                 , pack <$> string "::" ])
  where
    hColon = run
               [ h16
               , Text.singleton <$> char ':'
               ]
    --run :: (Monad m, Monoid n) => [m n] -> m n
    run :: [Parser Text] -> Parser Text
    run = fmap mconcat . sequence

h16 :: Parser Text
h16 = pack <$> star 1 4 hexDigit

ls32 :: Parser Text
ls32 =
  ( do h1 <- h16
       c <- pack <$> string ":"
       h2 <- h16
       return $ h1 <> c <> h2
  )
  <|> iPv4Address

iPv4Address :: Parser Text
iPv4Address = do
  d1 <- decOctet
  _ <- char '.'
  d2 <- decOctet
  _ <- char '.'
  d3 <- decOctet
  _ <- char '.'
  d4 <- decOctet
  return $ d1 <> "." <> d2 <> "." <> d3 <> "." <> d4

decOctet :: Parser Text
decOctet =
      try ( do h <- string "25"
               o <- oneOf ['0'..'5']
               return $ pack $ h ++ [o]
          )
  <|> try ( do h <- char '2'
               t <- oneOf ['0'..'4']
               o <- digit
               return $ pack [h,t,o]
          )
  <|> try ( do h <- char '1'
               t <- count 2 digit
               return $ pack $ h : t
          )
  <|> try (Text.singleton <$> digit)

regName :: Parser Text
regName = mconcat <$> many (
      try (Text.singleton <$> unreserved)
  <|> try pctEncoded
  <|> try (Text.singleton <$> subDelims)
  )

{-
path :: Parser Text
path =
      try pathAbempty
  <|> try pathAbsolute
  <|> try pathNoscheme
  <|> try pathRootless
  <|> try pathEmpty
-}
pathAbempty :: Parser Text
pathAbempty = mconcat <$> many ( do
  _ <- char '/'
  s <- segment
  return $ "/" <> s
  )

pathAbsolute :: Parser Text
pathAbsolute = do
  _ <- char '/'
  r <- option "" $ try $ do nz <- segmentNz
                            ss <- many slashSegment
                            return $ nz <> mconcat ss
  return $ "/" <> r

slashSegment :: Parser Text
slashSegment = do
  _ <- char '/'
  s <- segment
  return $ "/" <> s

pathNoscheme :: Parser Text
pathNoscheme = do
  nc <- segmentNzNc
  ss <- many slashSegment
  return $ nc <> mconcat ss

pathRootless :: Parser Text
pathRootless = do
  nz <- segmentNz
  ss <- many slashSegment
  return $ nz <> mconcat ss

pathEmpty :: Parser Text
pathEmpty = return ""

segment :: Parser Text
segment = mconcat <$> many pchar

segmentNz :: Parser Text
segmentNz = mconcat <$> many1 pchar

segmentNzNc :: Parser Text
segmentNzNc = mconcat <$> many1 (
      try (Text.singleton <$> unreserved)
  <|> try pctEncoded
  <|> try (Text.singleton <$> subDelims)
  <|> try (pack <$> string "@")
  )

pchar :: Parser Text
pchar =
      try (Text.singleton <$> unreserved)
  <|> try pctEncoded
  <|> try (Text.singleton <$> subDelims)
  <|> try (Text.singleton <$> char ':')
  <|> try (Text.singleton <$> char '@')

query :: Parser Text
query = mconcat <$> many
  (   pchar
  <|> (Text.singleton <$> char '/')
  <|> (Text.singleton <$> char '?')
  )

fragment :: Parser Text
fragment = query

pctEncoded :: Parser Text
pctEncoded = pack <$> sequence
  [ char '%'
  , hexDigit
  , hexDigit
  ]

unreserved :: Parser Char
unreserved = alphaNum <|> char '-' <|> char '.' <|> char '_' <|> char '~'

reserved :: Parser Char
reserved = genDelims <|> subDelims

genDelims :: Parser Char
genDelims =
  char ':' <|> char '|' <|> char '?' <|> char '#' <|> char '[' <|>
  char ']' <|> char '@'

subDelims :: Parser Char
subDelims =
  char '!' <|> char '$' <|> char '&' <|> char '\'' <|> char '(' <|> char ')' <|>
  char '*' <|> char '+' <|> char ',' <|> char ';' <|> char '='
