{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Links
  ( runApp
  ) where

import ClassyPrelude
import AppConfig
import Control.Lens
import DataAccess.URL
import Data.Time.Clock (addUTCTime)
import qualified Lib.URLs as L
import Shared.Types
import System.IO (getChar)

runApp :: IO ()
runApp = do
    eRes <- readAppConfig "app-config.yaml"
    case eRes of
      Left ex -> putStrLn (tshow ex)
      Right appConf -> do
        pg <- initHelper $ pgsDefaultConfig (acConnectionString appConf)
        putStrLn "Start"
        runReaderT ( do
          mRC <- getMostRecentRedditComment
          startTime <- case mRC of
                        Nothing -> do
                          putStrLn $ "Could not find start time"
                          liftIO $ addUTCTime diff <$> getCurrentTime
                        Just rc -> do
                          putStrLn $ "Starting off at : " ++ tshow (rc^.rcModifiedUtc)
                          return $ rc^.rcModifiedUtc
          go 1 startTime
                   ) pg
  where
    diff = negate $ 60*60*24*365*5

go :: Int -> UTCTime -> ReaderT PG.Connection IO ()
go iter startTime = do
  putStrLn $ "Iteration : " ++ tshow iter ++ " time " ++ tshow startTime
  comms <- listRedditComments startTime 1000
  putStrLn $ "Fetched " ++ tshow (length comms) ++ " comments"
  forM_ comms $ \comm -> do
    let urls = L.parseURLs (comm^.rcBody)
    forM_ urls $ \url -> do
      dbUrls <- createUrlIfNotExists url
      forM_ dbUrls $ \dbUrl ->
        when (dbUrl^.urlFull == L.originalURL url) $
          void $ createUrlRedditCommentIfNotExists (dbUrl^.urlId) (comm^.rcId)
  case fromNullable comms of
    Nothing -> return ()
    Just nonNullComms -> go (iter+1) ((last nonNullComms)^.rcModifiedUtc)
