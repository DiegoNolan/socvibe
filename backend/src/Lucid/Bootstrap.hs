{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Lucid.Bootstrap where

import ClassyPrelude
import Lucid
import Lucid.Base

data_toggle_ :: Text -> Attribute
data_toggle_ = makeAttribute "data-toggle"

data_target_ :: Text -> Attribute
data_target_ = makeAttribute "data-target"

data_validation_required_message_ :: Text -> Attribute
data_validation_required_message_ = makeAttribute "data-validation-required-message"
