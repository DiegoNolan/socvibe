{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Page.Index where

import ClassyPrelude
import Lucid

data IndexPage = IndexPage

instance ToHtml IndexPage where
  toHtml _ = masterPage
  toHtmlRaw a = toHtml a

masterPage :: Monad m => HtmlT m ()
masterPage = do
  -- doctypehtml_ (return ())
  html_ $ do
    head_ $ do
      link_ [ rel_  "stylesheet"
            , href_ "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
            ]
      -- script_ [] (return ())
      -- script_ [ src_ "https://code.jquery.com/jquery-2.1.3.min.js" ] $ undefined
      -- script_ [ src_ "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" ] (return ())
    body_ $ do
      div_ [ class_ "container" ] $ do
        return ()
