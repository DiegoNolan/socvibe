{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExtendedDefaultRules      #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE NoImplicitPrelude         #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeOperators             #-}
module Site
  ( runApp
  ) where

import           API
import           AppConfig
import           ClassyPrelude               hiding (Handler)
import           Control.Exception           (AsyncException (..))
import           Control.Lens
import           Control.Monad.Throttled
import qualified Data.HashMap.Strict         as HM
import qualified Data.Map                    as Map
import           Data.Pool
import           Data.Time.Clock
import           Data.Time.Clock.POSIX       (utcTimeToPOSIXSeconds)
import           DataAccess.Reddit
import qualified Database.PostgreSQL.Simple  as PG
import           Lib.Http
import           Lib.Postgres
import           Network.HTTP.Client         (Manager, newManager)
import           Network.HTTP.Client.TLS     (tlsManagerSettings)
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Network.Wai.Middleware.Cors
import           Page.Index
import qualified Reddit.Throttled            as R
import           Servant
import           Servant.Client
import           Servant.HTML.Lucid
import qualified SubscriptionExtractor       as Sub
import qualified Thing.Comment               as C
import qualified Thing.Link                  as L
import qualified Thing.Subreddit             as S


{-
runClient :: HasManager m => String -> ClientM a -> m (Either ServantError a)
runClient host func = do
  man <- getManager
  myRunClient man host func
-}
{-
myRunClient :: HasManager m
            => Manager -> String -> ClientM a -> m (Either ServantError a)
myRunClient man host func =
  liftIO $ runClientM func ClientEnv
                     { manager = man
                     , baseUrl = BaseUrl Http host 8000 "/api"
                     }
  -}

data Config = Config
  { cPostgres :: Postgres
  -- TODO: This should be pooled as well
  , cManager  :: Manager
  }

instance MonadIO m => HasPostgres (ReaderT Config m) where
  getPostgresState = cPostgres <$> ask

instance MonadIO m => HasManager (ReaderT Config m) where
  getManager = cManager <$> ask

runApp :: IO ()
runApp = do
  -- TODO: connect should probably fetch the cfg first then pass it to the
  -- actual PG conneciton function
  pool <- createPool connect PG.close 10 360 10
  m <- newManager tlsManagerSettings
  now <- getCurrentTime
  tvar <- newTVarIO (now, 0)

  let cfg = Config
              { cPostgres = PostgresPool pool
              , cManager = m
              }
  run 8080 (servantApp cfg)

wholeApi :: Proxy WholeAPI
wholeApi = Proxy

-- | Allow Content-Type header with values other then allowed by simpleCors.
corsWithContentType :: Middleware
corsWithContentType = cors (const $ Just policy)
    where
      policy = simpleCorsResourcePolicy
        { corsRequestHeaders = ["Content-Type"] }

servantApp :: Config -> Application
-- TODO: probably don't want CORS for prod
servantApp cfg = corsWithContentType $ serve wholeApi $
  hoistServer wholeApi (`runReaderT` cfg) server

type WholeAPI =
       "api" :> API
  :<|> Get '[HTML] IndexPage

server :: (HasPostgres m, HasManager m) => ServerT WholeAPI m
server =
       serverApi
  :<|> return IndexPage

serverApi :: (HasPostgres m, HasManager m) => ServerT API m
serverApi =
    (    createSubscription
    :<|> testSubscription
    :<|> getSubscriptions
    :<|> getHnResults
    :<|> getRedditResults
    )
  where
    -- TODO: get user from Auth
    user = 1
    createSubscription csr = do
      now <- liftIO getCurrentTime
      void $ Sub.createSubscription Subscription
          { _subId = 0
          , _subUserId = user
          , _subSites = csr ^. csrSites
          , _subType = csr ^. csrType
          , _subText = csr ^. csrText
          , _subCreatedUtc = now
          }

    testSubscription sub = Sub.runSubscriptionOnDB sub

    getSubscriptions = do
      putStrLn "get subscriptions"
      now <- liftIO getCurrentTime
      let oneDayAgo = addUTCTime (-86400*1) now
          toEpoch = round . realToFrac . utcTimeToPOSIXSeconds
      subs <- Sub.getUserSubscriptions user
      -- TODO: Probably can do this in one query
      forM subs $ \sub -> do
        hnCounts <- Sub.getUserHnSubscriptionCount (sub^.subId) (toEpoch now)
                                                   (toEpoch oneDayAgo)
        linkCounts <- Sub.getUserRedditLinkSubscriptionCount
                        (sub^.subId) now oneDayAgo
        commCounts <- Sub.getUserRedditCommentSubscriptionCount
                        (sub^.subId) now oneDayAgo
        return $ SubscriptionWithCounts
                  { _swc_subscription = sub
                  , _swc_redditCounts = linkCounts + commCounts
                  , _swc_hnCounts = hnCounts
                  }

    -- TODO: authenticate so only the owner can fetch
    getHnResults subId beforeTime lim = do
      items <- Sub.getUserHnSubscription subId beforeTime lim
      return $ HnItems items

    getRedditResults subId beforeTime lim = do
      links <- Sub.getUserRedditLinkSubscription subId beforeTime lim
      comms <- Sub.getUserRedditCommentSubscription subId beforeTime lim
      subredditMap <- makeSubredditMap <$> getAllSubreddits
      return $ take lim $ interleaveReddit
        (map (convertLink subredditMap) links)
        (map (convertComment subredditMap) comms)
