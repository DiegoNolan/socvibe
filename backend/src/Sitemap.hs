{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Sitemap
  ( sitemap
  ) where

import ClassyPrelude hiding (Element)
import Text.XML.Light

sitemap :: String
sitemap = ppTopElement . urlSetNode . urlsNode . map (domain ++) $ urls

domain :: String
domain = "http://socvibe.com"

urlSetNode :: Node t => t -> Element
urlSetNode t =
  add_attr (Attr (QName "xmlns" Nothing Nothing) "http://www.sitemaps.org/schemas/sitemap/0.9")
  (add_attr (Attr (QName "image" Nothing (Just "xmlns")) "http://www.google.com/schemas/sitemap-image/1.1")
  (add_attr (Attr (QName "xhtml" Nothing (Just "xmlns")) "http://www.w3.org/1999/xhtml")
  (node (QName "urlset" Nothing Nothing) t)))

urlsNode :: [String] -> [Element]
urlsNode =
  map $ \url -> node (QName "url" Nothing Nothing)
                  [ node (QName "loc" Nothing Nothing) url ]

urls :: [String]
urls =
  [ "/"
  , "/reddit-thread"
  , "/reddit-thread/info"
  , "/sitemap.xml"
  , "/robots.txt"
  ]
