{-# LANGUAGE Arrows                #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
module SubscriptionExtractor where

import           API                                  hiding (RedditComment,
                                                       RedditLink)
import qualified API
import           ClassyPrelude                        hiding (optional)
import           Control.Arrow                        (returnA, (<<<))
import           Control.Lens                         hiding ((.>))
import           Data.Aeson
import           Data.Aeson.TH
import           Data.Either                          (fromRight)
import qualified Data.Map                             as Map
import           Data.Profunctor.Product
import           Data.Profunctor.Product.TH           (makeAdaptorAndInstance)
import           Data.Time.Clock                      (addUTCTime)
import           Data.Time.Clock.POSIX                (utcTimeToPOSIXSeconds)
import           DataAccess.HackerNews
import           DataAccess.Reddit
import           DataAccess.Reddit.Types
import qualified Database.PostgreSQL.Simple           as PG
import qualified Database.PostgreSQL.Simple.FromField as PG
import           Lib.Http
import           Lib.Postgres
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS
import           Opaleye                              hiding (max, min, not,
                                                       null, runDelete,
                                                       runInsertMany,
                                                       runInsertManyReturning,
                                                       runInsert_, runQuery,
                                                       runQueryExplicit,
                                                       runUpdate,
                                                       runUpdateReturning)
import           Opaleye.Internal.Manipulation
import qualified Streamly.Prelude                     as S
import qualified Thing.Comment                        as C
import qualified Thing.Link                           as L
import qualified Thing.User                           as U

instance PG.FromField SubscriptionType where
  fromField f mbs = do
    (s :: Text) <- PG.fromField f mbs
    case readMay s of
      Just st -> return st
      Nothing -> mzero

instance QueryRunnerColumnDefault PGText SubscriptionType where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

instance PG.FromField Site where
  fromField f mbs = do
    (s :: Text) <- PG.fromField f mbs
    case readMay s of
      Just st -> return st
      Nothing -> mzero

instance QueryRunnerColumnDefault PGText Site where
  queryRunnerColumnDefault = fieldQueryRunnerColumn

$(makeAdaptorAndInstance "pSubscription" ''Subscription')

type SubscriptionColumn =
  Subscription' (Column PGInt8) (Column PGInt8) (Column (PGArray PGText))
                (Column PGText) (Column PGText) (Column PGTimestamptz)

type SubscriptionInsertColumn =
  Subscription' (Maybe (Column PGInt8)) (Column PGInt8) (Column (PGArray PGText))
                (Column PGText) (Column PGText) (Column PGTimestamptz)

subscriptionTable :: Table SubscriptionInsertColumn SubscriptionColumn
subscriptionTable = Table "subscription" $ pSubscription Subscription
  { _subId = optional "id"
  , _subUserId = required "user_id"
  , _subSites = required "sites"
  , _subType = required "sub_type"
  , _subText = required "sub_text"
  , _subCreatedUtc = required "created_utc"
  }

subscriptionQuery :: Query SubscriptionColumn
subscriptionQuery = queryTable subscriptionTable

runAllSubscriptions :: HasPostgres m => (Subscription -> m ()) -> m ()
runAllSubscriptions f = go 0
  where
    go offset = do
      subs <- getSubscriptions batchSize offset
      forM_ subs f
      let len = length subs
      if len < batchSize
        then return ()
        else go (offset+len)
    batchSize = 1000

runAllSubscriptionsOnItems :: HasPostgres m => [Item] -> m ()
runAllSubscriptionsOnItems items =
  runAllSubscriptions (\sub -> runSubscriptionOnItems sub items)

runSubscriptionOnItems :: HasPostgres m => Subscription -> [Item] -> m ()
runSubscriptionOnItems sub items = do
  -- TODO: run on URL and title maybe?
  let matchedItems = filter (\item ->
                          case runSubscription (sub^.subType) (sub^.subText)
                                (fromMaybe "" $ item^.itemText) of
                            Left _ -> False
                            Right matches -> not (null matches)
                            ) items
  void $ createHnSubscriptionMatches $
    map (\item -> (sub^.subId, fromIntegral $ item^.itemId)) matchedItems

runSubscriptionOnDB :: HasPostgres m
                    => CreateSubscriptionRequest
                    -> m SubscriptionResult
runSubscriptionOnDB csr = do
    now <- liftIO getCurrentTime

    -- TODO: refactor
    let goHn offset prevMatches = do
          items <- getItems batchSize offset now
          let matchedItems =
                filter (subMatchesOnItem (csr^.csrType) (csr^.csrText)) items
              totalMatches = prevMatches ++ matchedItems
          if length totalMatches > maxMatches || length items < batchSize
            then return $ take maxMatches totalMatches
            else goHn (offset+batchSize) totalMatches
        goRl offset prevMatches = do
          links <- getRedditLinks batchSize offset now
          let matchedLinks =
                filter (subMatchesOnLink (csr^.csrType) (csr^.csrText)) links
              totalMatches = prevMatches ++ matchedLinks
          if length totalMatches > maxMatches || length links < batchSize
            then return $ take maxMatches totalMatches
            else goRl (offset+batchSize) totalMatches
        goRc offset prevMatches = do
          comments <- getRedditComments batchSize offset now
          let matchedComments =
                filter (subMatchesOnComment (csr^.csrType) (csr^.csrText)) comments
              totalMatches = prevMatches ++ matchedComments
          if length totalMatches > maxMatches || length comments < batchSize
            then return $ take maxMatches totalMatches
            else goRc (offset+batchSize) totalMatches
    items <- goHn 0 []
    links <- goRl 0 []
    comms <- goRc 0 []
    subredditMap <- makeSubredditMap <$> getAllSubreddits
    let apiComms = map (convertComment subredditMap) comms
        apiLinks = map (convertLink subredditMap) links
    return $ SubscriptionResult
               { _srHackerNews = items
               , _srReddit = interleaveReddit apiLinks apiComms
               }
  where
    maxMatches = 20
    batchSize = 1000

userTable :: Table (Maybe (Column PGInt8), Column PGTimestamptz)
                   (Column PGInt8, Column PGTimestamptz)
userTable = table "users" $ p2
  ( tableColumn "id"
  , tableColumn "created_utc"
  )

userQuery :: Query (Column PGInt8, Column PGTimestamptz)
userQuery = queryTable userTable

createUser :: HasPostgres m => m Int64
createUser = do
  now <- liftIO getCurrentTime
  headEx <$> runInsertManyReturning userTable [ (Nothing, pgUTCTime now) ] fst

createSubscription :: HasPostgres m => Subscription -> m Int64
createSubscription sub =
  runInsertMany subscriptionTable [subToInsert sub]

subToInsert :: Subscription -> SubscriptionInsertColumn
subToInsert sub =
  sub & subId .~ Nothing
      & subUserId %~ pgInt8
      & subSites %~ pgArray (pgStrictText . tshow)
      & subType %~ (pgStrictText . tshow)
      & subText %~ pgStrictText
      & subCreatedUtc %~ pgUTCTime

getUserSubscriptions :: HasPostgres m => Int64 -> m [Subscription]
getUserSubscriptions userId = runQuery (getUserSubscriptionsQuery userId)

getUserSubscriptionsQuery :: Int64 -> Query SubscriptionColumn
getUserSubscriptionsQuery userId = proc () -> do
  sub <- subscriptionQuery -< ()
  restrict -< sub^.subUserId .== pgInt8 userId
  returnA -< sub

-- TODO: should be able to limit to sites
getSubscriptions :: HasPostgres m => Int -> Int -> m [Subscription]
getSubscriptions lim off = runQuery (getSubscriptionsQuery lim off)

getSubscriptionsQuery :: Int -> Int -> Query SubscriptionColumn
getSubscriptionsQuery lim off =
    limit lim
  . offset off
  . orderBy (asc _subId)
  $ subscriptionQuery

-- Subscripton matches
-- HN
hnSubscriptionMatchTable :: Table (Column PGInt8, Column PGInt4)
                                  (Column PGInt8, Column PGInt4)
hnSubscriptionMatchTable = table "hn_subscription_match" $ p2
  ( tableColumn "sub_id"
  , tableColumn "hn_item_id"
  )

hnSubscriptionMatchQuery :: Query (Column PGInt8, Column PGInt4)
hnSubscriptionMatchQuery = queryTable hnSubscriptionMatchTable

createHnSubscriptionMatches :: HasPostgres m => [(Int64, Int)] -> m Int64
createHnSubscriptionMatches xs =
  runInsert_ Insert
               { iTable = hnSubscriptionMatchTable
               , iRows = map (over _1 pgInt8 . over _2 pgInt4) xs
               , iReturning = Count
               , iOnConflict = Just DoNothing
               }

getUserHnSubscriptionCount :: HasPostgres m => Int64 -> Int64 -> Int64 -> m Int64
getUserHnSubscriptionCount subId startTime endTime = do
    rows <- runQuery (aggregate countStar q)
    return $ fromMaybe 0 (headMay rows)
  where
    q = proc () -> do
          hnMatch <- hnSubscriptionMatchQuery -< ()
          item <- itemQuery -< ()
          restrict -< hnMatch ^. _1 .== pgInt8 subId .&&
                      hnMatch ^. _2 .== item ^. itemId .&&
                      Opaleye.fromNullable (pgInt8 0) (item ^. itemTime)
                        .< pgInt8 (max startTime endTime) .&&
                      Opaleye.fromNullable (pgInt8 0) (item ^. itemTime)
                        .>= pgInt8 (min startTime endTime)
          returnA -< item

getUserHnSubscription :: HasPostgres m => Int64 -> Int64 -> Int -> m [Item]
getUserHnSubscription subId beforeTime lim =
  runQuery (getUserHnSubscriptionQuery subId beforeTime lim)

getUserHnSubscriptionQuery :: Int64 -> Int64 -> Int -> Query ItemColumn
getUserHnSubscriptionQuery subId beforeTime lim =
      limit lim
    . orderBy (desc _itemTime)
    $ q
  where
    q = proc () -> do
          hnMatch <- hnSubscriptionMatchQuery -< ()
          item <- itemQuery -< ()
          restrict -< hnMatch ^. _1 .== pgInt8 subId .&&
                      hnMatch ^. _2 .== item ^. itemId .&&
                      Opaleye.fromNullable (pgInt8 0) (item ^. itemTime)
                        .< pgInt8 beforeTime
          returnA -< item

itemMatchLastTimeTable :: Table (Column PGInt8) (Column PGInt8)
itemMatchLastTimeTable = Table "hn_item_match_last_time" $ p1
  (tableColumn "lasttime")

itemMatchLastTimeQuery :: Query (Column PGInt8)
itemMatchLastTimeQuery =
    limit 1
  . orderBy (desc id)
  $ queryTable itemMatchLastTimeTable

getItemMatchLastTime :: HasPostgres m => m (Maybe Int64)
getItemMatchLastTime =
  listToMaybe <$> runQuery itemMatchLastTimeQuery

updateItemMatchLastTime :: HasPostgres m => Int64 -> m ()
updateItemMatchLastTime newtime = do
  -- TODO: race condition wait for upsert
  runDelete itemMatchLastTimeTable (\_ -> pgBool True)
  runInsertMany itemMatchLastTimeTable [ pgInt8 newtime ]
  return ()

-- Reddit Link
redditLinkSubscriptionMatchTable :: Table (Column PGInt8, Column PGText)
                                          (Column PGInt8, Column PGText)
redditLinkSubscriptionMatchTable = table "reddit_link_match" $ p2
  ( tableColumn "sub_id"
  , tableColumn "reddit_link_id"
  )

redditLinkSubscriptionMatchQuery :: Query (Column PGInt8, Column PGText)
redditLinkSubscriptionMatchQuery = queryTable redditLinkSubscriptionMatchTable

createRedditLinkSubscriptionMatches :: HasPostgres m => [(Int64, Text)] -> m Int64
createRedditLinkSubscriptionMatches xs =
  runInsert_ Insert
               { iTable = redditLinkSubscriptionMatchTable
               , iRows = map (over _1 pgInt8 . over _2 pgStrictText) xs
               , iReturning = Count
               , iOnConflict = Just DoNothing
               }

getUserRedditLinkSubscriptionCount :: HasPostgres m
                                   => Int64
                                   -> UTCTime
                                   -> UTCTime
                                   -> m Int64
getUserRedditLinkSubscriptionCount subId startTime endTime = do
    rows <- runQuery (aggregate countStar q)
    return $ fromMaybe 0 (headMay rows)
  where
    q =  proc () -> do
          linkMatch <- redditLinkSubscriptionMatchQuery -< ()
          link <- redditLinkQuery -< ()
          restrict -< linkMatch ^. _1 .== pgInt8 subId .&&
                      linkMatch ^. _2 .== link ^. rl_id .&&
                      link ^. rl_createdUtc .< pgUTCTime startTime .&&
                      link ^. rl_createdUtc .>= pgUTCTime endTime
          returnA -< link

getUserRedditLinkSubscription :: HasPostgres m
                              => Int64 -> UTCTime -> Int -> m [RedditLink]
getUserRedditLinkSubscription subId beforeTime lim =
  runQuery (getUserRedditLinkSubscriptionQuery subId beforeTime lim)

getUserRedditLinkSubscriptionQuery :: Int64 -> UTCTime -> Int -> Query RedditLinkColumn
getUserRedditLinkSubscriptionQuery subId beforeTime lim =
      limit lim
    . orderBy (desc _rl_createdUtc)
    $ q
  where
    q =  proc () -> do
          linkMatch <- redditLinkSubscriptionMatchQuery -< ()
          link <- redditLinkQuery -< ()
          restrict -< linkMatch ^. _1 .== pgInt8 subId .&&
                      linkMatch ^. _2 .== link ^. rl_id .&&
                      link ^. rl_createdUtc .< pgUTCTime beforeTime
          returnA -< link

-- Reddit Comment
redditCommentSubscriptionMatchTable :: Table (Column PGInt8, Column PGText)
                                             (Column PGInt8, Column PGText)
redditCommentSubscriptionMatchTable = table "reddit_comment_match" $ p2
  ( tableColumn "sub_id"
  , tableColumn "reddit_comment_id"
  )

redditCommentSubscriptionMatchQuery :: Query (Column PGInt8, Column PGText)
redditCommentSubscriptionMatchQuery = queryTable redditCommentSubscriptionMatchTable

createRedditCommentSubscriptionMatches :: HasPostgres m => [(Int64, Text)] -> m Int64
createRedditCommentSubscriptionMatches xs =
  runInsert_ Insert
               { iTable = redditCommentSubscriptionMatchTable
               , iRows = map (over _1 pgInt8 . over _2 pgStrictText) xs
               , iReturning = Count
               , iOnConflict = Just DoNothing
               }

getUserRedditCommentSubscriptionCount :: HasPostgres m
                                      => Int64 -> UTCTime -> UTCTime -> m Int64
getUserRedditCommentSubscriptionCount subId startTime endTime = do
    rows <- runQuery (aggregate countStar q)
    return $ fromMaybe 0 (headMay rows)
  where
    q =  proc () -> do
          commentMatch <- redditCommentSubscriptionMatchQuery -< ()
          comment <- redditCommentQuery -< ()
          restrict -< commentMatch ^. _1 .== pgInt8 subId .&&
                      commentMatch ^. _2 .== comment ^. rc_id .&&
                      comment ^. rc_createdUtc .< pgUTCTime startTime .&&
                      comment ^. rc_createdUtc .>= pgUTCTime endTime
          returnA -< comment

getUserRedditCommentSubscription :: HasPostgres m
                                 => Int64 -> UTCTime -> Int -> m [RedditComment]
getUserRedditCommentSubscription subId beforeTime lim =
  runQuery (getUserRedditCommentSubscriptionQuery subId beforeTime lim)

getUserRedditCommentSubscriptionQuery :: Int64
                                      -> UTCTime
                                      -> Int
                                      -> Query RedditCommentColumn
getUserRedditCommentSubscriptionQuery subId beforeTime lim =
      limit lim
    . orderBy (desc _rc_createdUtc)
    $ q
  where
    q =  proc () -> do
          commentMatch <- redditCommentSubscriptionMatchQuery -< ()
          comment <- redditCommentQuery -< ()
          restrict -< commentMatch ^. _1 .== pgInt8 subId .&&
                      commentMatch ^. _2 .== comment ^. rc_id .&&
                      comment ^. rc_createdUtc .< pgUTCTime beforeTime
          returnA -< comment

subMatchesOnLink :: SubscriptionType -> Text -> RedditLink -> Bool
subMatchesOnLink subType subText link = not (null (fromRight [] matches))
  where
    matches = do
      titleMatch <- runSubscription subType subText (link^.rl_title)
      urlMatch <- runSubscription subType subText (link^.rl_url)
      textMatch <- runSubscription subType subText
                (fromMaybe (link^.rl_selftext) (link^.rl_selftextHtml))
      return $ concat [ titleMatch, urlMatch, textMatch ]

subMatchesOnLink' :: SubscriptionType -> Text -> L.Link -> Bool
subMatchesOnLink' subType subText link = not (null (fromRight [] matches))
  where
    matches = do
      titleMatch <- runSubscription subType subText (L.title link)
      urlMatch <- runSubscription subType subText (L.url link)
      textMatch <- runSubscription subType subText
                   (fromMaybe (L.selftext link) (L.selftextHtml link))
      return $ concat [ titleMatch, urlMatch, textMatch ]

subMatchesOnComment :: SubscriptionType -> Text -> RedditComment -> Bool
subMatchesOnComment subType subText comment = not (null (fromRight [] matches))
  where
    matches = runSubscription subType subText (comment^.rc_bodyHtml)

subMatchesOnComment' :: SubscriptionType -> Text -> C.Comment -> Bool
subMatchesOnComment' subType subText comment = not (null (fromRight [] matches ))
  where
    matches = runSubscription subType subText (C.bodyHtml comment)

subMatchesOnItem :: SubscriptionType -> Text -> Item -> Bool
subMatchesOnItem subType subText item =
  -- TODO: How to handle title and text
  not . null $ fromRight []
  (runSubscription subType subText (fromMaybe "" (item^.itemText)))
