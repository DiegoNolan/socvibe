{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
module Thing.Comment
    ( Comment (..)
    ) where

import ClassyPrelude
import Data.Aeson
import Data.Aeson.TH
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.ToRow

data Comment = Comment
    { id_               :: !Text
    , name              :: !Text
    , ups               :: !Int
    , downs             :: !Int
    , score             :: !Int
    , gilded            :: !Int
    , controversiality  :: !Float
    , body              :: !Text
    , bodyHtml          :: !Text
    , edited            :: !(Maybe UTCTime)
    -- , replies           :: ![Comment]
    , scoreHidden       :: !Bool
    -- , subreddit         :: !Text
    , userId            :: !Text
    , linkId            :: !Text
    , parentComment     :: !(Maybe Text)
    , subredditId       :: !Text
    , createdUtc        :: !UTCTime
    -- Not reddit related
    , modifiedUtc       :: !UTCTime     -- When we last updated this record in PSQL
    } deriving Show

instance Eq Comment where
  a == b =    id_ a == id_ b &&
              name a == name b &&
              ups a == ups b &&
              downs a == downs b &&
              score a == score b &&
              gilded a == gilded b &&
              controversiality a == controversiality b &&
              body a == body b &&
              bodyHtml a == bodyHtml b &&
              edited a == edited b &&
              scoreHidden a == scoreHidden b &&
              userId a == userId b &&
              linkId a == linkId b &&
              parentComment a == parentComment b &&
              subredditId a == subredditId b &&
              createdUtc a == createdUtc b

instance FromRow Comment where
    fromRow =
        Comment
            <$> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field

instance ToRow Comment where
    toRow Comment{..} =
        [ toField id_
        , toField name
        , toField ups
        , toField downs
        , toField score
        , toField gilded
        , toField controversiality
        , toField body
        , toField bodyHtml
        , toField edited
        , toField scoreHidden
        , toField userId
        , toField linkId
        , toField parentComment
        , toField subredditId
        , toField createdUtc
        ]


$(deriveJSON defaultOptions ''Comment)
