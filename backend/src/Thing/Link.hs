{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
module Thing.Link
  ( Link (..)
  ) where

import ClassyPrelude
import Data.Aeson
import Data.Aeson.TH
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.ToRow

data Link = Link
  { id_           :: !Text
  , name          :: !Text
  , title         :: !Text
  , ups           :: !Int
  , downs         :: !Int
  , score         :: !Int
  , gilded        :: !Int
  , url           :: !Text
  , domain        :: !Text
  , permalink     :: !Text
  , hidden        :: !Bool
  , isSelf        :: !Bool
  , numComments   :: !Int
  , over18        :: !Bool
  , selftext      :: !Text
  , selftextHtml  :: !(Maybe Text)
  , stickied      :: !Bool
  , thumbnail     :: !Text
  , edited        :: !(Maybe UTCTime)
  , userId        :: !Text
  , subredditId   :: !Text
  , createdUtc    :: !UTCTime
  -- Not reddit related
  , modifiedUtc   :: !UTCTime     -- When we last updated this record in PSQL
  } deriving Show

instance Eq Link where
    a == b =    id_ a == id_ b &&
                name a == name b &&
                title a == title b &&
                ups a == ups b &&
                downs a == downs b &&
                score a == score b &&
                gilded a == gilded b &&
                url a == url b &&
                domain a == domain b &&
                permalink a == permalink b &&
                hidden a == hidden b &&
                isSelf a == isSelf b &&
                numComments a == numComments b &&
                over18 a == over18 b &&
                selftext a == selftext b &&
                selftextHtml a == selftextHtml b &&
                stickied a == stickied b &&
                thumbnail a == thumbnail b &&
                edited a == edited b &&
                userId a == userId b &&
                subredditId a == subredditId b &&
                createdUtc a == createdUtc b

instance FromRow Link where
    fromRow =
        Link
            <$> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field

instance ToRow Link where
    toRow Link{..} =
        [ toField id_
        , toField name
        , toField title
        , toField ups
        , toField downs
        , toField score
        , toField gilded
        , toField url
        , toField domain
        , toField permalink
        , toField hidden
        , toField isSelf
        , toField numComments
        , toField over18
        , toField selftext
        , toField selftextHtml
        , toField stickied
        , toField thumbnail
        , toField edited
        , toField userId
        , toField subredditId
        , toField createdUtc
        ]


$(deriveJSON defaultOptions ''Link)
