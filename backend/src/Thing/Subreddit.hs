{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Thing.Subreddit
  ( Subreddit (..)
  ) where

import ClassyPrelude

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.ToRow

data Subreddit = Subreddit
  { id_               :: !Text
  , title             :: !Text
  , name              :: !Text
  , displayName       :: !Text
  , url               :: !Text
  , description       :: !Text
  , subscribers       :: !Int
  , subredditType     :: !Text
  , over18            :: !Bool
  , publicTraffic     :: !Bool
  , submissionType    :: !Text
  , submitText        :: !Text
  , createdUtc        :: !UTCTime
  -- Not related to the reddit site
  , modifiedUtc       :: !UTCTime     -- When we last updated this record in PSQL
  } deriving Show

instance Eq Subreddit where
  a == b =    id_ a == id_ b &&
              title a == title b &&
              name a == name b &&
              displayName a == displayName b &&
              url a == url b &&
              description a == description b &&
              subscribers a == subscribers b &&
              subredditType a == subredditType b &&
              over18 a == over18 b &&
              publicTraffic a == publicTraffic b &&
              submissionType a == submissionType b &&
              submitText a == submitText b &&
              createdUtc a == createdUtc b

createQuery :: Query
createQuery =
    "create table if not exists reddit_subreddit \
    \( id text primary key \
    \, title text not null \
    \, name text not null \
    \, displayName text not null \
    \, url text not null \
    \, description text not null \
    \, subscribers integer not null \
    \, subredditType text not null \
    \, over18 boolean not null \
    \, publicTraffic boolean not null \
    \, submissionType text not null \
    \, submitText text not null \
    \, createdUtc timestamptz not null \
    \, modifiedUtc timestamptz not null)"

instance FromRow Subreddit where
    fromRow =
        Subreddit
            <$> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field

instance ToRow Subreddit where
    toRow Subreddit{..} =
        [ toField id_
        , toField title
        , toField name
        , toField displayName
        , toField url
        , toField description
        , toField subscribers
        , toField subredditType
        , toField over18
        , toField publicTraffic
        , toField submissionType
        , toField submitText
        , toField createdUtc
        ]



