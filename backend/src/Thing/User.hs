{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Thing.User
  ( User (..)
  ) where

import ClassyPrelude
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.ToRow

data User = User
  { id_           :: !Text
  , modifiedUtc   :: !UTCTime
  } deriving Show

instance Eq User where
  a == b = id_ a == id_ b

instance FromRow User where
  fromRow = User <$> field <*> field

instance ToRow User where
  toRow User{..} = [ toField id_ ]


