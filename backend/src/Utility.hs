{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Utility
  ( uneededTime
  -- , catchAndRetry
  ) where

import ClassyPrelude
import Control.Monad.Catch
import Control.Retry

uneededTime :: UTCTime
uneededTime = UTCTime (ModifiedJulianDay 0) 0

{-
catchAndRetry :: (MonadCatch m, MonadIO m) => m a -> m (Either String a)
catchAndRetry action =
  retrying
    (constantDelay 10000000 <> limitRetries 5)
    (\_ e -> case e of
                    Left _ -> return True
                    Right _ -> return False
    )
    (\_ -> handleErrors action)

-}

