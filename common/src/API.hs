{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NoImplicitPrelude    #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PolyKinds            #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeSynonymInstances #-}
module API where

import           ClassyPrelude
import           Control.Lens
import           Data.Aeson
import           Data.Aeson.TH
import qualified Data.Text.Internal.Search as Text
import qualified GHC.Arr                   as Arr
import           RedditTypes
import           Servant.API
import           Text.Regex.PCRE

data Site
  = HackerNews
  | Reddit
  deriving (Show, Read, Generic, Eq)

instance ToJSON Site
instance FromJSON Site

data SubscriptionType =
    Keyword
  | Regex
  deriving (Show, Read, Generic, Ord, Eq)

data CreateSubscriptionRequest = CreateSubscriptionRequest
  { _csrSites :: [Site]
  , _csrType  :: SubscriptionType
  , _csrText  :: Text
  } deriving (Generic, Show)

makeLenses ''CreateSubscriptionRequest
instance ToJSON CreateSubscriptionRequest
instance FromJSON CreateSubscriptionRequest

runSubscription :: SubscriptionType -> Text -> Text -> Either Text [(Int, Int)]
runSubscription subtype needle haystack =
  case subtype of
    Keyword -> let idxs = Text.indices (toLower needle) (toLower haystack)
               in Right (map (\i -> (i, length needle)) idxs)
    Regex  ->
      case makeRegexM (unpack needle) of
        Nothing -> Left "Invalid regex"
        Just regex ->
          let res = map Arr.elems $ matchAllText (regex :: Regex)
                                                 (encodeUtf8 haystack)
          in Right (map snd $ concat res)

validateCreateSubscriptionRequest :: CreateSubscriptionRequest
                                  -> Either Text CreateSubscriptionRequest
validateCreateSubscriptionRequest csr
-- TODO: improve validation
  | null (csr^.csrSites) = Left "Must select at least one site"
  | length (csr^.csrText) <= 2 = Left "Keyword must be longer"
  | otherwise = Right csr

data Subscription' a b c d e f = Subscription
  { _subId         :: a
  , _subUserId     :: b
  , _subSites      :: c
  , _subType       :: d
  , _subText       :: e
  , _subCreatedUtc :: f
  } deriving (Generic, Show)

type Subscription =
  Subscription' Int64 Int64 [Site] SubscriptionType Text UTCTime

makeLenses ''Subscription'
instance ToJSON SubscriptionType
instance FromJSON SubscriptionType
instance ToJSON Subscription
instance FromJSON Subscription

data Item' a b c d e f g h i j k l m = Item
  { _itemId          :: a
  , _itemDeleted     :: b
  , _itemType        :: c
  , _itemBy          :: d
  , _itemTime        :: e
  , _itemText        :: f
  , _itemDead        :: g
  , _itemParent      :: h
  , _itemURL         :: i
  , _itemScore       :: j
  , _itemTitle       :: k
  , _itemModifiedUtc :: l
  , _itemCreatedUtc  :: m
  } deriving (Show, Generic)

makeLenses ''Item'
instance ToJSON Item
instance FromJSON Item

type Item =
  Item' Int (Maybe Bool) Int (Maybe Text) (Maybe Int64) (Maybe Text)
        (Maybe Bool) (Maybe Int) (Maybe Text) (Maybe Int) (Maybe Text) UTCTime UTCTime

data RedditCommOrLink
  = RC RedditComment
  | RL RedditLink
  deriving (Show, Generic)

instance ToJSON RedditCommOrLink
instance FromJSON RedditCommOrLink

data SubscriptionResult = SubscriptionResult
  { _srHackerNews :: [Item]
  , _srReddit     :: [RedditCommOrLink]
  } deriving (Show, Generic)

makeLenses ''SubscriptionResult
instance ToJSON SubscriptionResult
instance FromJSON SubscriptionResult

newtype HnItems = HnItems { unItems :: [Item] } deriving (Show, Generic)

instance ToJSON HnItems
instance FromJSON HnItems

getRedditCreatedUtc :: RedditCommOrLink -> UTCTime
getRedditCreatedUtc (RC rc) = commentTime rc
getRedditCreatedUtc (RL rl) = linkTime rl

validateSubscriptionResult :: SubscriptionResult -> Either Text SubscriptionResult
validateSubscriptionResult sr
    | length (sr ^. srHackerNews) > maxResults = Left "Too many results"
    | otherwise = Right sr
  where
    -- TODO: figure out a good number. Also, we need to return all returns so this works
    maxResults = 30

data SubscriptionWithCounts = SubscriptionWithCounts
  { _swc_subscription :: Subscription
  , _swc_redditCounts :: Int64
  , _swc_hnCounts     :: Int64
  } deriving (Show, Generic)

makeLenses ''SubscriptionWithCounts
instance ToJSON SubscriptionWithCounts
instance FromJSON SubscriptionWithCounts

type API =
       "createSubscription" :> ReqBody '[JSON] CreateSubscriptionRequest
                            :> Post '[JSON] ()
  :<|> "testSubscription" :> ReqBody '[JSON] CreateSubscriptionRequest
                          :> Post '[JSON] SubscriptionResult
  :<|> "getSubscriptions" :> Get '[JSON] [SubscriptionWithCounts]
  :<|> "getHnResults" :> Capture "subId" Int64
                      :> Capture "beforeTime" Int64
                      :> Capture "limit" Int
                      :> Get '[JSON] HnItems
  :<|> "getRedditResults" :> Capture "subId" Int64
                          :> Capture "beforeTime" UTCTime
                          :> Capture "limit" Int
                          :> Get '[JSON] [RedditCommOrLink]
