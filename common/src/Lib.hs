{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib
  ( secondsToFriendly
  , utcToEpoch
  ) where

import ClassyPrelude
import Data.Time.Clock
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)

secondsToFriendly :: Int -> Text
secondsToFriendly i =
    toStr y "year" "years" ++
    toStr w "week" "weeks" ++
    toStr d "day" "days" ++
    toStr h "hour" "hours" ++
    toStr ((hr + 30) `div` 60) "minute" "minutes"
  where
    (y, yr) = divMod i 31536000
    (w, wr) = divMod yr 604800
    (d, dr) = divMod wr 86400
    (h, hr) = divMod dr 3600
    toStr 0 _ _ = ""
    toStr 1 singular _ = "1 " ++ singular ++ " "
    toStr v _ plural = tshow v ++ " " ++ plural ++ " "

utcToEpoch :: UTCTime -> Int
utcToEpoch utc = round (realToFrac (utcTimeToPOSIXSeconds utc))
