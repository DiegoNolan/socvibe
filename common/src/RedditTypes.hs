{-# LANGUAGE TemplateHaskell,
             RecordWildCards,
             DeriveGeneric,
             FlexibleContexts,
             NoImplicitPrelude,
             OverloadedStrings #-}
module RedditTypes where

import ClassyPrelude
import Data.Aeson
import Data.Aeson.TH
import qualified Data.Set as Set

redditBaseUrl :: Text
redditBaseUrl = "https://reddit.com"

userLink :: Text -> Text
userLink uid = redditBaseUrl ++ "/user/" ++ uid

postLink :: Text -> Text -> Text
postLink subreddit postId =
  redditBaseUrl ++ "/r/" ++ subreddit ++ "/comments/" ++ postId

commentLink :: Text -> Text -> Text -> Text
commentLink subreddit postId cId =
  redditBaseUrl ++ "/r/" ++ subreddit ++ "/comments/" ++ postId ++ "/-/" ++ cId

data RedditLink = RedditLink
  { linkId :: Text
  , linkAuthor :: Text
  , linkTitle :: Text
  , linkScore :: Int
  , linkTime :: UTCTime
  , linkSubreddit :: Text
  } deriving (Show, Generic)

instance ToJSON RedditLink
instance FromJSON RedditLink

data RedditComment = RedditComment
  { commentId :: Text
  , commentAuthor :: Text
  , commentBody :: Text
  , commentScore :: Int
  , commentTime :: UTCTime
  , commentSubreddit :: Text
  } deriving (Show, Generic)

instance ToJSON RedditComment
instance FromJSON RedditComment

data RedditSubreddit = RedditSubreddit
  { subId :: Text
  , subTitle :: Text
  , subName :: Text
  , subDisplayName :: Text
  , subUrl :: Text
  , subDescription :: Text
  , subSubscribers :: Int
  , subType :: Text
  , subOver18 :: Bool
  , subPublicTraffic :: Bool
  , subSubmissionType :: Text
  , subSubmitText :: Text
  } deriving (Show, Generic)

instance ToJSON RedditSubreddit
instance FromJSON RedditSubreddit
