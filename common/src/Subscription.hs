{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Subscription where

import ClassyPrelude
import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Data.Time.Clock

data SubscriptionMatcher =
    Keyword Text
  | Domain Text
  | Regex Text
  deriving Show
