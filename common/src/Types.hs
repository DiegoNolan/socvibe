{-# LANGUAGE TemplateHaskell #-}
module Types where

import Control.Lens
import Data.Aeson
import Data.Aeson.TH
import Data.Time.Clock
import Data.Text (Text)

data URL' a b c d e f g h = URL
  { _urlId :: a
  , _urlFull :: b
  , _urlProtocol :: c
  , _urlDomain :: d
  , _urlPath :: e
  , _urlQueryString :: f
  , _urlAnchor :: g
  , _urlCreatedUtc :: h
  } deriving Show

type URL = URL' Int Text Text Text Text Text Text UTCTime

data URLRedditComment' a b = URLRedditComment
  { _urcUrlId :: a
  , _urcCommentId :: b
  } deriving Show

type URLRedditComment = URLRedditComment' Int Text

data RedditComment' a b c d e f g h i j k l m n o p q = RedditComment
  { _rcId :: a
  , _rcName :: b
  , _rcUps :: c
  , _rcDowns :: d
  , _rcScore :: e
  , _rcGilded :: f
  , _rcControversiality :: g
  , _rcBody :: h
  , _rcBodyHtml :: i
  , _rcEdited :: j
  , _rcScoreHidden :: k
  , _rcUserId :: l
  , _rcLinkId :: m
  , _rcParentComment :: n
  , _rcSubredditId :: o
  , _rcCreatedUtc :: p
  , _rcModifiedUtc :: q
  } deriving Show

type RedditComment =
  RedditComment' Text Text Int Int Int Int Double Text Text (Maybe UTCTime) Bool
                 Text Text (Maybe Text) Text UTCTime UTCTime

makeLenses ''URL'
makeLenses ''URLRedditComment'
makeLenses ''RedditComment'

$(deriveJSON defaultOptions ''URL')
$(deriveJSON defaultOptions ''URLRedditComment')
$(deriveJSON defaultOptions ''RedditComment')
