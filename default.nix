{}:

(import ./reflex-platform {}).project ({ pkgs, ... }: {

  useWarp = true;

  overrides = self: super: {
    servant-reflex = self.callPackage ./servant-reflex.nix {};
    reddit-client = self.callCabal2nix "reddit-client" ../reddit-client {};
    hackernews = pkgs.haskell.lib.dontCheck
               (self.callCabal2nix "hackernews" ../hackernews {});
    conduit-extra = pkgs.haskell.lib.dontCheck super.conduit-extra;
  };

  packages = {
    common = ./common;
    backend = ./backend;
    frontend = ./frontend;
  };

  android.frontend = {
    executableName = "frontend";
    applicationId = "org.example.frontend";
    displayName = "Example Android App";
  };

  ios.frontend = {
    executableName = "frontend";
    bundleIdentifier = "org.example.frontend";
    bundleName = "Example iOS App";
  };

  shells = {
    ghc = ["common" "backend" "frontend"];
    ghcjs = ["common" "frontend"];
  };
})
