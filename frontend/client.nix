{ mkDerivation, aeson, base, containers
, friendly-time, ghcjs-base, ghcjs-dom, lens, reflex, reflex-dom
, safe, servant, servant-reflex, stdenv, text, time, vector
}:
mkDerivation {
  pname = "ghcjs";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base containers friendly-time ghcjs-base
    ghcjs-dom lens reflex reflex-dom safe servant servant-reflex text time
    vector
  ];
  executableHaskellDepends = [ base ghcjs-base ];
  description = "Initial project template from stack description: Please see README.md homepage: https://github.com/githubuser/ghcjs#readme";
  license = stdenv.lib.licenses.bsd3;
}
