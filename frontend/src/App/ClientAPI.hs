{-# LANGUAGE RankNTypes,
             NoImplicitPrelude,
             FlexibleContexts,
             OverloadedStrings,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             TypeFamilies,
             NoMonomorphismRestriction #-}
module App.ClientAPI
  ( getComments
  , listSubreddits
  , listSubredditNames
  , addCrawlerHost
  , getCrawlerState
  , removeCrawlerHost
  , addCrawlerSubreddits
  , removeCrawlerSubreddits
  , listRedditUrls
  , getMostPopularRedditUrls
  , getRedditUrlReferences
  ) where

import Data.Proxy
import Reflex
import Reflex.Dom
import Servant.API
import Servant.Reflex
import API

api :: Proxy API
api = Proxy

(
  (    getComments
  :<|> listSubreddits
  :<|> listSubredditNames
  :<|> (    addCrawlerHost
       :<|> getCrawlerState
       :<|> removeCrawlerHost
       :<|> addCrawlerSubreddits
       :<|> removeCrawlerSubreddits
       )
  )
  :<|> listRedditUrls
  :<|> getMostPopularRedditUrls
  :<|> getRedditUrlReferences
  )
  = client (Proxy :: Proxy API)
             (Proxy :: MonadWidget t m => Proxy m)
             (Proxy :: Proxy ())
             (constDyn (BasePath "/api"))
