{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             OverloadedStrings,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             LambdaCase,
             NoMonomorphismRestriction #-}
module App.CrawlerDashboard
  ( runApp
  ) where

import App.ClientAPI
import Control.Lens
import Control.Monad
import Data.Aeson
import Data.Either
import qualified Data.Map as Map
import Data.Monoid
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Time.Clock.POSIX
import Data.Time.Format.Human
import GHCJS.DOM
import GHCJS.DOM.Element hiding (drop)
import GHCJS.DOM.Document hiding (select, drop)
import GHCJS.DOM.Node
import GHCJS.DOM.Types hiding (Event, Text, Rect)
import GHCJS.Types
import JavaScript.Web.Location
import Lib.Widgets.Loader
import Lib.Widgets.Popup
import Reflex
import Reflex.Dom
import Reflex.Host.Class
import Servant.Reflex
import Shared.API
import Shared.CrawlerAPI
import Shared.RedditTypes


runApp :: IO ()
runApp = mainWidgetInElementById "dashboard" dashboardWidget

dashboardWidget :: MonadWidget t m => m ()
dashboardWidget = mdo

  evHostName <- addHostWidget
  hostname <- holdDyn (Left "") (Right <$> evHostName)
  -- TODO: This is stupid
  addHostEv <- delay 0.0001 (tag (constant ()) evHostName)
  addHostResp <- addCrawlerHost hostname addHostEv


  dynSubreddits <- dyn ((\case
                           Nothing -> return never
                           Just hs -> subredditsWidget hs
                       ) <$> hostsState)

  onLoad <- getPostBuild
  crawlerStateResp <- getCrawlerState onLoad
  hostsState <- foldDyn (\r _ -> case r of
                                   ResponseSuccess _ v _ -> Just v
                                   _ -> Nothing
                        ) Nothing
                        (leftmost [ crawlerStateResp, addHostResp, dashEventsResp ])
  renderHostsStateEvent <- dyn (renderMaybeHostsState <$> hostsState)
  dashboardEvents <- switchPromptly never renderHostsStateEvent
  subredditsEvents <- switchPromptly never dynSubreddits

  dynEventStream <- widgetHold (return never)
                      (dashboardEventMapper <$> leftmost [ dashboardEvents
                                                         , subredditsEvents ])
  let dashEventsResp = switchPromptlyDyn dynEventStream
  return ()

hostsStateAssignedSubreddits :: HostsState -> [Text]
hostsStateAssignedSubreddits (HostsState{..}) =
  concatMap csSubreddits (rights (Map.elems hsHosts))

subredditsWidget :: MonadWidget t m => HostsState -> m (Event t DashboardEvent)
subredditsWidget hostsState = do
    reqRes <- listSubredditNames =<< getPostBuild
    dynSubreddits <- foldDyn (\r _ -> case r of
                                        ResponseSuccess _ v _ -> v
                                        _ -> []
                            ) [] reqRes

    innerEv <- elClass "div" "row" $ do
      elClass "div" "col-xs-6" $ text "Total Subreddits"
      elClass "div" "col-xs-6" $ do
        dyn ((text . Text.pack . show . length) <$> dynSubreddits)

      elClass "div" "col-xs-6" $ text "Unassigned Subreddits"
      elClass "div" "col-xs-6" $ do
        dyn ((\subs -> do
               let unassigned = findUnassigned subs
               text $ Text.pack (show (length unassigned)) <> " Unassigned Subreddits"
               (aaEl, _) <- elAttr' "button" ("class" =: "btn btn-primary") (text "Auto Assign")
               return $ tag (constant (AutoAssign (length subs) unassigned hostsState))
                            (domEvent Click aaEl)
            ) <$> dynSubreddits)

    switchPromptly never innerEv
  where
    assignedSubreddits = hostsStateAssignedSubreddits hostsState

    findUnassigned :: [Text] -> [Text]
    findUnassigned = filter (`notElem` assignedSubreddits)

dashboardEventMapper :: MonadWidget t m => DashboardEvent -> m (Event t (ReqResult () HostsState))
dashboardEventMapper (RemoveHost hostname) = do
  postBuild <- getPostBuild
  removeCrawlerHost (constDyn (Right hostname)) postBuild
dashboardEventMapper (AutoAssign totalSubs unassigned hostsState) = do
    postBuild <- getPostBuild
    endEvent <- if Map.null workingCrawlers
                  then return never
                  else runner postBuild (Map.assocs toBeAdded)
    getCrawlerState endEvent
  where
    runner :: MonadWidget t m => Event t () -> [(Text, [Text])] -> m (Event t ())
    runner fireEvent ((hostname, []):rest) = return fireEvent
    runner fireEvent ((hostname, toAssign):rest) = do
      resEv <- addCrawlerSubreddits (constDyn (Right (ModifySubredditsReq hostname toAssign))) fireEvent
      runner (tag (constant ()) resEv) rest
    runner fireEvent _ = return fireEvent
    workingCrawlers = Map.foldrWithKey'
                        (\k v m -> case v of
                                     Left _ -> m
                                     Right cs -> Map.insert k cs m
                        ) Map.empty (hsHosts hostsState)
    subCount = Map.map (\CrawlerState{..} -> length csSubreddits) workingCrawlers
    toBeAdded = snd $ Map.mapAccum
                   (\remainingSubs subCount ->
                      let n = max 0 (averageSubs - subCount)
                          s = take n remainingSubs
                          ss = drop n remainingSubs
                      in (ss, s)
                   ) unassigned subCount
    averageSubs = totalSubs `div` Map.size workingCrawlers + 1

data DashboardEvent =
    RemoveHost Text
  | AutoAssign Int [Text] HostsState

addHostWidget :: MonadWidget t m => m (Event t Text)
addHostWidget = do
  elClass "div" "row" $ do
    elClass "div" "col-sm-3 col-xs-0" (return ())
    (textValue, submitEv) <- elClass "div" "col-sm-6 col-xs-12" $ do
      elClass "div" "input-group" $ do
        tValue <- textInput $ def
                    & textInputConfig_attributes .~ constDyn
                          (Map.fromList
                            [ ("class", "form-control")
                            , ("placeholder", "Host Name")
                            ])
        (submitEl, _) <- elClass "div" "input-group-btn" $ do
          elAttr' "button" ("class" =: "btn btn-primary") (text "Add")
        return $ (tValue, domEvent Click submitEl)
    elClass "div" "col-sm-2 col-xs-0" (return ())
    let enterEvent = ffilter (== 13) (_textInput_keyup textValue)
    return $ tagDyn (_textInput_value textValue)
                    (leftmost [ enterEvent, tag (constant 0) submitEv ])

renderMaybeHostsState :: MonadWidget t m => Maybe HostsState -> m (Event t DashboardEvent)
renderMaybeHostsState Nothing = text "fetching" >> return never
renderMaybeHostsState (Just hs) = renderHostsState hs

renderHostsState :: MonadWidget t m => HostsState -> m (Event t DashboardEvent)
renderHostsState (HostsState{..}) = do
  elClass "table" "table" $ do
    el "thead" $ do
      el "tr" $ do
        el "th" $ text "Host"
        el "th" $ text "Host State"
        el "th" $ text "Actions"
    el "tbody" $ do
      dashboardEvents <- forM (Map.assocs hsHosts) $ \(hostname, eCS) ->
        el "tr" $ do
          el "td" $ text hostname
          el "td" $
            case eCS of
              Left er -> text $ "Could not fetch host state : " <> er
              Right cs -> renderCrawlerState cs
          el "td" $ do
            (remEl, _) <- elAttr' "button" ("class" =: "btn btn-danger") (text "Remove")
            return $ tag (constant (RemoveHost hostname)) (domEvent Click remEl)
      return $ leftmost dashboardEvents


renderCrawlerState :: MonadWidget t m => CrawlerState -> m ()
renderCrawlerState (CrawlerState{..}) = do
  elClass "div" "row" $ do
    elClass "div" "col-xs-6" $ text "Current Subreddit"
    elClass "div" "col-xs-6" $
      case csCurrentSubreddit of
        Nothing -> text "No subreddit"
        Just sub -> text sub

  elClass "div" "row" $ do
    elClass "div" "col-xs-6" $ text "Cycle Length"
    elClass "div" "col-xs-6" $
      case csCycleLength of
        Nothing -> text "No cycle"
        Just t -> text $ Text.pack (show t) <> "s"

  elClass "div" "row" $ do
    elClass "div" "col-xs-6" $ text "TPS"
    elClass "div" "col-xs-6" $
      case csTPS of
        Nothing -> text "No TPS"
        Just t -> text $ Text.pack (show (fromRational t :: Float))

  elClass "div" "row" $ do
    elClass "div" "col-xs-6" $ text "Subreddits"
    elClass "div" "col-xs-6" $ do
      elClass "div" "row" $ do
        elClass "div" "col-xs-12" $ text (Text.pack (show (length csSubreddits)))
      elClass "div" "row" $ do
        forM_ (take 10 csSubreddits) $ \sub -> do
          elClass "div" "col-xs-12" $ text sub

