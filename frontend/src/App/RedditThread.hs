{-# LANGUAGE RankNTypes,
             OverloadedStrings,
             FlexibleContexts,
             RecursiveDo,
             PartialTypeSignatures,
             LambdaCase,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module App.RedditThread
  ( runApp
  ) where

import App.ClientAPI
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Data.Aeson
import Data.JSString.Text
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Monoid
import Data.Proxy
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Time.Clock.POSIX
import Data.Time.Format.Human
import GHCJS.DOM
import GHCJS.DOM.Element
import GHCJS.DOM.Document hiding (select, getLocation)
import GHCJS.DOM.Location
import GHCJS.DOM.Node
import GHCJS.DOM.Window (getLocation, getInnerWidth)
import GHCJS.Types
import Lib.Util
import Lib.Widgets.Alerts
import Lib.Widgets.Loader
import Lib.Widgets.Popup
import Lib.Widgets.Reddit
import Reflex
import Reflex.Dom
import Reflex.Host.Class
import Safe
import Servant.Reflex
import Shared.Lib
import Shared.RedditTypes

clampHue :: Int -> Int
clampHue i = min (max 0 i) 360

createHSLColor :: Int -> String
createHSLColor i = "hsl(" ++ show i ++ ", 50%, 50%)"

runApp :: IO ()
runApp = mainWidgetInElementById "reddit" threadWidget

displayInt :: MonadWidget t m => Dynamic t Int -> m ()
displayInt = display

data CommentOpenClosed =
    Open (Either RedditPost RedditComment)
  | Close

data FetchEvent =
    Resp XhrResponse
  | StartFetch

threadWidget :: MonadWidget t m => m ()
threadWidget = mdo
    textEv <- elClass "div" "row" $ do
      elClass "div" "col-sm-3 col-xs-1" (return ())
      (textValue, submitEv) <- elClass "div" "col-sm-6 col-xs-10" $ do
        elClass "div" "input-group" $ do
          tValue <- textInput $ def
                      & textInputConfig_attributes .~ constDyn
                            (Map.fromList
                              [ ("class", "form-control")
                              , ("placeholder", "Reddit Thread Id")
                              ])
                      & textInputConfig_setValue .~ threadIdEvent
          (submitEl, _) <- elClass "div" "input-group-btn" $
            elAttr' "button" ("class" =: "btn btn-default") (text "Go!")
          return $ (tValue, domEvent Click submitEl)
      elClass "div" "col-sm-2 col-xs-1" $
        elAttr "div" ("style" =: "display: table;") $
          elAttr "a" (Map.fromList [ ("href", "/reddit-thread/info")
                                   , ("style", "display: table-cell; vertical-align: center;")
                                   ]) $
            elAttr "span" (Map.fromList
                          [ ("class", "glyphicon glyphicon-info-sign")
                          , ("title", "How do I interpret this data?")
                          ]) (return ())
      let enterEvent = ffilter (== 13) (_textInput_keyup textValue)
      return $ tagPromptlyDyn (_textInput_value textValue)
                              (leftmost [ enterEvent, tag (constant 0) submitEv ])
    void $ performEvent $
        fmap (\tvalue -> do loc <- getLocation =<< currentWindowUnchecked
                            setPathname loc ("/reddit-thread/" <> tvalue)
             ) textEv


    -- Options
    dispOptions <- displayOptionsWidget

    onLoad <- getPostBuild
    path <- ( do loc <- getLocation =<< currentWindowUnchecked
                 getPathname loc )
    let threadId = Text.reverse $ Text.takeWhile (/= '/') (Text.reverse path)
        dontFetch = "reddit-thread" == threadId
        fetchEvent = ffilter (\_ -> not dontFetch) onLoad
        threadIdEvent = tag (constant threadId) fetchEvent
    resp <- getComments (constDyn (Right threadId)) fetchEvent
    redditPostDyn <- foldDyn (\r _ ->
                                case r of
                                  ResponseSuccess _ v _ -> Value v
                                  _ -> Fail
                             )
                             (if dontFetch
                                 then None
                                 else Loading)
                             resp

    postRenderEvent <- dyn (redditPostWidget <$> dispOptions <*> redditPostDyn)
    commentClicked <- switchPromptly never postRenderEvent
    elAttr "div" ("style" =: "margin-bottom: 20px;") (return ())
    commentOrPost <- foldDyn (\ev _ -> case ev of
                                          Open x -> Just x
                                          Close  -> Nothing
                              ) Nothing $ leftmost [ Open <$> commentClicked
                                                   , closeEvent
                                                   ]
    let postComm = (\mC mP ->
                    case mC of
                      Just (Left rp) -> Just (Left rp)
                      Just (Right c) -> case mP of
                                          Value p -> Just (Right $ CommentAndContext
                                                                   (postSubreddit p)
                                                                   (postId p) c)
                                          _ -> Nothing
                      _ -> Nothing
                   ) <$> commentOrPost <*> redditPostDyn
    -- Overlay
    clEv <- dyn =<< mapDyn (\case
                               Just (Left rp) -> displayPost rp
                               Just (Right cc) -> displayCurrentComment (Just cc)
                               Nothing -> return never
                           ) postComm
    closeEvent <- tag (constant Close) <$> switchPromptly never clEv
    return ()

data Loader a =
    Loading
  | Fail
  | None
  | Value a deriving Show


-- Height and width are flipped here because of reasons
redditPostWidget :: MonadWidget t m
                 => DisplayOptions
                 -> Loader RedditPost
                 -> m (Event t (Either RedditPost RedditComment))
redditPostWidget _ None = return never
redditPostWidget _ Fail =
  elAttr "div" ("style" =: "text-align: center;") $ do
    elAttr "div" ("style" =: "display: inline-block;") $ do
      elAttr "p" ("style" =: ("height: 200px; display: table-cell; " <>
                            "vertical-align: middle; text-align: center; width: 100%;")) $ do
        errorWidget
          (text "Failed fetching comments. Make sure the thread ID is correct or try again later")
        return never
redditPostWidget _ Loading =
  elAttr "div" ("style" =: "text-align: center;") $ do
    elAttr "div" ("style" =: "display: inline-block;") $ do
      elAttr "p" ("style" =: ("height: 200px; display: table-cell; " <>
                            "vertical-align: middle; text-align: center; width: 100%;")) $ do
        loader
        return never
redditPostWidget (opts@DisplayOptions{..}) (Value rp) = do
  windowWidth <- getInnerWidth =<< currentWindowUnchecked
  (postEl, _) <- elAttr "div" ("style" =: "text-align: center") $
    elAttr' "h3" ("style" =: "cursor: pointer;") $ text (Text.pack (postTitle rp))
  let postClickedEv = tag (constant rp) $ domEvent Click postEl
  commEv <-
    if null (postComments rp)
      then el "span" (text "No comments") >> return never
      else elAttr "div" ("style" =: "display: block; overflow-x: visible; white-space: nowrap") $
             commentDivs opts colorFunc (postWidth rp)
               height totalTime (width windowWidth) timeNormalizedComments
  return $ leftmost [ Left <$> postClickedEv, Right <$> commEv ]
  where
    timeNormalizedComments = postComments (normalizeTimes rp)
    height = max (10 * fromIntegral (postWidth rp)) 700
    -- Should combine min and max so we don't have to traverse twice
    totalTime = maximum (map oldestComment (postComments rp)) - postTime rp
    minTime = minimumTime timeNormalizedComments

    totalDepth = depthOfComments (postComments rp)
    userSet = allUsers timeNormalizedComments
    minS = fromIntegral $ minScore timeNormalizedComments
    maxS = fromIntegral $ maxScore timeNormalizedComments
    -- scoreSpread = max (maxS - minS) 1
    colorFunc :: Int -> Int -> String -> String
    colorFunc depth score user =
      case colorOptions of
        Depth -> createHSLColor (div (depth * 360) totalDepth)
        User  -> let totalUsers = Set.size userSet
                     idx = fromMaybe 0 (Set.lookupIndex user userSet)
                 in createHSLColor (div (idx * 360) totalUsers)
        Score -> let f :: Double -> Double
                     f x = ( 120 / ( log (maxS - minS + 1) ) ) * log (x - minS + 1)
                 in createHSLColor $ round (f (fromIntegral score))

    minWidth :: Int
    minWidth = 1000
    width :: Int -> Float
    width windowWidth = case soType scalingOptions of
      Linear -> min (fromIntegral windowWidth)
                    (max 800 (10 * (fromIntegral totalTime / fromIntegral minTime)))
      Logarithmic -> fromIntegral minWidth * soValue scalingOptions


commentDivs :: MonadWidget t m
            => DisplayOptions
            -> (Int -> Int -> String -> String)
            -> Int
            -> Float
            -> Int
            -> Float
            -> [RedditComment]
            -> m (Event t RedditComment)
commentDivs (opts@DisplayOptions{..}) colorFunc
                      count height totalTime width comments = do
    commentEvents <- forM comments $ \comment -> do
      let w = scaleX (fromIntegral (commentTime comment))
          h = scaleY (fromIntegral (commentWidth comment))
      elAttr "div" ("style" =: "white-space: nowrap; overflow-x: visible; font-size: 0px;") $ do
        (comDiv, _) <-
          elAttr "div" ("style" =: "display: inline-block") $
            elAttr' "div"
              (Map.fromList
                [ ("style",
                    "outline: solid 1px gray; " <>
                    "display: block; cursor: pointer; " <>
                    "width: " <> Text.pack (show w) <> "px; " <>
                    "height: " <> Text.pack (show h) <> "px;" <>
                    "background-color: " <>
                    Text.pack (colorFunc 0 (commentScore comment)
                                           (commentAuthor comment))
                  )
                ]) (return ())

        childClicks <- elAttr "div" ("style" =: "display: inline-block") $
              commentDivs opts (\d s a -> colorFunc (d+1) s a)
                         count height totalTime width
                         (commentChildren comment)
        let commentClicked = domEvent Click comDiv
        return $ leftmost [ tag (constant comment) commentClicked, childClicks ]
      -- return evs
    return $ leftmost commentEvents
  where
    scaleY v = height * (v / fromIntegral count)
    scaleX v = case soType scalingOptions of
                 Linear -> width * (v / fromIntegral totalTime)
                 -- This is a bit sloppy
                 Logarithmic -> 50 * log ( width * (v / fromIntegral totalTime) )

normalizeTimes :: RedditPost -> RedditPost
normalizeTimes rp = rp { postTime = 0
                       , postComments = map (normalizeComment (postTime rp))
                                            (postComments rp)
                       }
  where
    normalizeComment prevTime rc =
      -- The smallest time frame they have is 4 minutes and 16 seconds
      rc { commentTime = max (commentTime rc - prevTime) 256
         , commentChildren = map (normalizeComment (commentTime rc)) (commentChildren rc)
         }

data DisplayOptions = DisplayOptions
  { colorOptions :: ColorOptions
  , scalingOptions :: ScalingOptions
  }

data ColorOptions =
    Depth
  | User
  | Score deriving (Show, Eq, Enum)

data ScalingOptions = ScalingOptions
  { soValue :: Float
  , soType :: ScalingType
  } deriving (Show, Eq)

data ScalingType =
    Linear
  | Logarithmic deriving (Show, Eq, Enum)

displayOptionsWidget :: MonadWidget t m => m (Dynamic t DisplayOptions)
displayOptionsWidget = do
    elClass "div" "row" $ do
      elAttr "div" ("style" =: "text-align: center;") $ do
        elAttr "div" ("style" =: "display: inline-block; text-align: center") $ do
          co <- colorOpts
          so <- scalingOpts
          return $ DisplayOptions <$> co <*> so
  where
    divWrapper = elAttr "div" ("style" =: "margin: 5px; display: inline-block;")

    colorOpts :: MonadWidget t m => m (Dynamic t ColorOptions)
    colorOpts = do
      elAttr "div" ("style" =: "display: inline-block; margin: 10px;") $ mdo

        elAttr "div" ("style" =: "font-size: 18px;") (text "Color Options")

        (scoreEl, _) <- divWrapper $ elDynAttr' "span" (mkAttrs Score <$> currentColor)
                                                       (text "Score")

        (depthEl, _) <- divWrapper $ elDynAttr' "span" (mkAttrs Depth <$> currentColor)
                                                       (text "Depth")

        (userEl, _) <- divWrapper $ elDynAttr' "span" (mkAttrs User <$> currentColor)
                                                      (text "User")

        let clickEvents = leftmost
                            [ tag (constant Depth) (domEvent Click depthEl)
                            , tag (constant User) (domEvent Click userEl)
                            , tag (constant Score) (domEvent Click scoreEl)
                            ]
        currentColor <- holdDyn Score clickEvents
        return currentColor

    scalingOpts :: MonadWidget t m => m (Dynamic t ScalingOptions)
    scalingOpts = mdo
      elAttr "div" ("style" =: "display: inline-block; margin: 10px;") $ mdo

        elAttr "div" ("style" =: "font-size: 18px;") (text "Scaling Options")

        (linearEl, _) <- divWrapper $ elDynAttr' "span" (mkAttrs Linear <$> scalingType)
                                                        (text "Linear")

        (logEl, _) <- divWrapper $ elDynAttr' "span" (mkAttrs Logarithmic <$> scalingType)
                                                     (text "Logarithmic")

        let clickEvents = leftmost
                            [ tag (constant Linear) (domEvent Click linearEl)
                            , tag (constant Logarithmic) (domEvent Click logEl)
                            ]

        {-
        tValue <- textInput $ def
                    & textInputConfig_attributes .~ constDyn
                        (Map.fromList
                           [ ("class", "form-control")
                           , ("type", "number")
                           ])
                    -- & textInputConfig_setValue .~ (Text.pack . show <$> updated scalingValue)
                    & textInputConfig_initialValue .~ "1"
        scalingValue <- foldDyn (\strVal p -> case readMay (Text.unpack strVal) of
                                                Nothing -> p
                                                Just nv -> nv
                                ) 1
                                (updated (_textInput_value tValue))
        -}
        scalingType <- holdDyn Linear clickEvents
        return $ ScalingOptions <$> constDyn 1 <*> scalingType

    mkAttrs thisOption current =
        if thisOption == current
           then "style" =: "color: #009933; cursor: pointer; font-size: 16px; font-weight: bold;"
           else "style" =: "cursor: pointer; font-size: 16px; font-weight: bold;"

