{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE LambdaCase                #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE NoImplicitPrelude         #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE RecursiveDo               #-}
module App.Subscription where

import           API
import           ClassyPrelude
import           Control.Lens
import           Data.Either                      (isLeft, isRight)
import qualified Data.Map                         as Map
import qualified Data.Text                        as Text
import           Language.Javascript.JSaddle
import           Language.Javascript.JSaddle.Warp (run)
import qualified Lib.Client                       as Client
import           Lib.Util
import           Lib.Widgets.Bootstrap
import           Lib.Widgets.HackerNews
import           Lib.Widgets.Popup
import           Lib.Widgets.MatchRow
import           Lib.Widgets.SubscriptionsWidget
import           Reflex
import           Reflex.Dom                       hiding (mainWidget,
                                                   mainWidgetInElementById, run)
import           Reflex.Dom.Main
import           Servant.Reflex

main :: IO ()
main = run 8000 $ mainWidget widgetWrapper

widgetWrapper :: MonadWidget t m => m ()
widgetWrapper = do
  el "html" $ do
    el "head" $ do
      elAttr "link" (  "rel" =: "stylesheet"
                    <> "href" =: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" ) blank
      elAttr "script" (  "src" =: "https://code.jquery.com/jquery-2.1.3.min.js" ) blank

      pb <- delay 1 =<< getPostBuild
      widgetHold blank $
        (elAttr "script" (  "src" =: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" ) blank) <$ pb
    el "body" $ do
      elClass "div" "container" $ do
        pb <- delay 2 =<< getPostBuild
        widgetHold blank (subscriptionWidget <$ pb)
        return ()

data SubEvent
  = Loading
  | DisplaySubs [SubscriptionWithCounts]
  | FailedLoadSubs
  | CreateNewSub [SubscriptionWithCounts]
  | DisplaySub Subscription [SubscriptionWithCounts]

loadingSubsWidget :: MonadWidget t m => m (Event t SubEvent)
loadingSubsWidget = do
  text "loading subscriptions"
  pb <- getPostBuild
  resp <- Client.getSubscriptions pb
  return $ (\case
              ResponseSuccess _ subs _ -> DisplaySubs subs
              _ -> FailedLoadSubs
           ) <$> resp

subscriptionWidget :: MonadWidget t m
                   => m ()
subscriptionWidget = mdo
  dynEv <- widgetHold loadingSubsWidget (mapSubEvent <$> ev)
  let ev = switchPromptlyDyn dynEv
  return ()

mapSubEvent :: MonadWidget t m => SubEvent -> m (Event t SubEvent)
mapSubEvent Loading = loadingSubsWidget
mapSubEvent (DisplaySubs subs) = subsWidget subs
mapSubEvent FailedLoadSubs = text "failed" >> return never
mapSubEvent (CreateNewSub subs) = createSubscriptionWidget subs
mapSubEvent (DisplaySub sub subs) = do
  backEv <- button "Back"
  displaySubscriptionWidget sub
  return $ DisplaySubs subs <$ backEv

subsWidget :: MonadWidget t m
           => [SubscriptionWithCounts]
           -> m (Event t SubEvent)
subsWidget subs = do
  createEv <- button "Create Sub"
  subIdEv <- subscriptionsWidget subs
  return $ leftmost [ CreateNewSub subs <$ createEv
                    , fmapMaybe
                        (\i ->
                           case find (\s -> s^.swc_subscription.subId == i) subs of
                             Nothing -> Nothing
                             Just sub -> Just $ DisplaySub (sub^.swc_subscription) subs
                        )
                        subIdEv
                    ]

validatedCSRWidget :: MonadWidget t m
                   => m (Dynamic t (Either Text CreateSubscriptionRequest))
validatedCSRWidget = do
  let initialType = Keyword
  redditDyn <- siteOption "Reddit"
  hnDyn <- siteOption "Hacker News"
  -- twitterDyn <- siteOption "Twitter"
  subTypeDD <- dropdown initialType (constDyn $ Map.fromList
                                    [ (Keyword, "Keyword")
                                    -- , (Domain, "Domain")
                                    , (Regex, "Regex")
                                    ]) $ def
  ti <- textInput def
  -- TODO: make these strings enums
  let subType = subTypeDD ^. dropdown_value
      subTxt = ti ^. textInput_value
      sitesDyn = (\reddit hn ->
                    (if reddit then [Reddit] else []) ++
                    (if hn then [HackerNews] else [])
                 ) <$> redditDyn <*> hnDyn
      csrDyn = CreateSubscriptionRequest <$> sitesDyn
                                         <*> subType
                                         <*> subTxt
      validatedCSR = validateCreateSubscriptionRequest <$> csrDyn
  searchTester subType subTxt
  dyn ((\case
          Right _  -> return ()
          Left txt -> text txt
      ) <$> validatedCSR)
  return validatedCSR

searchTester :: MonadWidget t m
             => Dynamic t SubscriptionType
             -> Dynamic t Text
             -> m ()
searchTester subscriptionType needle = do
  ta <- textArea $ def
  let haystack = ta ^. textArea_value
  dyn $ matchRow <$> subscriptionType <*> needle <*> haystack
  return ()

-- TODO: add failure state
data SubTestingState
  = None
  | TestCSR CreateSubscriptionRequest
  | TestResult CreateSubscriptionRequest SubscriptionResult

subscriptionTester :: MonadWidget t m
                   => Bool
                   -> m (Event t ()) -- ^ post creation event
subscriptionTester saveable = mdo
  validatedCSR <- validatedCSRWidget

  testSubEv <- btnPrimaryDisabled (isRight <$> validatedCSR) "Test Subscription"
  createSubEv <- if saveable
                 then btnSuccessDisabled (isRight <$> saveableCSR)
                      "Save Subscription"
                 else return never
  testRes <- widgetHold (return ( Left "Must test subscription first"
                                , never
                                )
                        ) (mapTestingState <$> testEv)
  let testEv = leftmost [ switchPromptlyDyn (snd <$> testRes)
                        , fmapMaybe (\case
                                        Left _ -> Nothing
                                        Right csr -> Just (TestCSR csr)
                                    ) (tagPromptlyDyn validatedCSR testSubEv)
                        ]
      saveableCSR = fst <$> testRes

  -- TODO: handle failures
  resp <- Client.createSubscription saveableCSR createSubEv
  return $ fmapMaybe (\case
                         ResponseSuccess _ _ _ -> Just ()
                         _ -> Nothing
                     ) resp

mapTestingState :: MonadWidget t m
                => SubTestingState
                -> m ( Either Text CreateSubscriptionRequest
                     , Event t SubTestingState
                     )
mapTestingState None =
  return ( Left "Must test subscription first"
         , never
         )
mapTestingState (TestCSR csr) = do
  text "Loading Results"
  pb <- getPostBuild
  testSubRes <- Client.testSubscription (constDyn (Right csr)) pb
  return ( Left "Must test subscription first"
         , fmapMaybe (\case
                         ResponseSuccess _ subRes _ -> Just (TestResult csr subRes)
                         _ -> Nothing
                     ) testSubRes
         )
mapTestingState (TestResult csr subRes) = do
  renderHNItems (csr^.csrType) (csr^.csrText) (subRes^.srHackerNews)
  -- TODO: validate number of results
  return ( Right csr
         , never
         )

createSubscriptionWidget :: MonadWidget t m
                         => [SubscriptionWithCounts]
                         -> m (Event t SubEvent)
createSubscriptionWidget subs = mdo
  cancel <- button "Cancel"
  createEv <- subscriptionTester True
  return $ leftmost [ DisplaySubs subs <$ cancel
                    , Loading <$ createEv
                    ]

siteOption :: MonadWidget t m
           => Text
           -> m (Dynamic t Bool)
siteOption site = do
  el "div" $ do
    cb <- checkbox True $ def & checkboxConfig_attributes .~ constDyn ("id" =: site)
    elAttr "label" ("for" =: site) (text site)
    return $ cb ^. checkbox_value
