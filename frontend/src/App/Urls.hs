{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             OverloadedStrings,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             LambdaCase,
             NoMonomorphismRestriction #-}
module App.Urls
  ( runApp
  ) where

import App.ClientAPI
import Control.Lens
import Control.Monad
import Data.Int
import qualified Data.Map as Map
import Data.Text
import Lib.Util
import Reflex
import Reflex.Dom
import Reflex.Host.Class
import Servant.Reflex
import Shared.API
import Shared.RedditTypes (commentLink)
import Shared.Types

runApp :: IO ()
runApp = mainWidgetInElementById "urls" urlsWidget

data NextPrev = Next | Prev

urlsWidget :: MonadWidget t m => m ()
urlsWidget = mdo
  let perPage = 25
  postBuild <- getPostBuild
                               -- QNone
  offset <- foldDyn (\ev p ->
                       case ev of
                         Next -> p + perPage
                         Prev -> p - perPage
                    ) 0 pageEvent
  reqResult <- getMostPopularRedditUrls (constDyn (QParamSome perPage))
                                        (QParamSome <$> offset)
                                        (leftmost [ postBuild, nextClick, prevClick ])
  widgetHold (text "loading") (handleServerResult displayUrls <$> reqResult)
  (prevButton, _) <- elAttr' "button" ("class" =: "btn btn-default") (text "Previous")
  (nextButton, _) <- elAttr' "button" ("class" =: "btn btn-default") (text "Next")
  let prevClick = domEvent Click prevButton
      nextClick = domEvent Click nextButton
      pageEvent = leftmost [ tag (constant Next) nextClick, tag (constant Prev) prevClick ]
  return ()

displayUrls :: MonadWidget t m => [(URL, Int64)] -> m ()
displayUrls urls = forM_ urls displayUrl
  where
    displayUrl :: MonadWidget t m => (URL, Int64) -> m ()
    displayUrl (url, count) = do
      (r, _) <- elAttr' "div" (Map.fromList
                                [ ("class", "row")
                                , ("style", "border: solid 1px black;")
                                ])$ do
        elClass "div" "col-xs-8" $ text (url^.urlFull)
        elClass "div" "col-xs-4" $ text (pack (show count))
      let rowClicked = domEvent Click r
      reqResult <- getRedditUrlReferences (constDyn [url^.urlId]) rowClicked
      widgetHold (return ()) (handleServerResult displayRedditComments <$> reqResult)
      return ()

displayRedditComments :: MonadWidget t m => [RedditComment] -> m ()
displayRedditComments comments =
  forM_ comments $ \comment -> do
    displayRedditCommentBody (comment^.rcBodyHtml)

handleServerResult :: MonadWidget t m
                   => (a -> m ())
                   -> ReqResult tag a
                   -> m ()
handleServerResult f (ResponseSuccess _ a _) = f a
handleServerResult _ _ = text "Failed"
