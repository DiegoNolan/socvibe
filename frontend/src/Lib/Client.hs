{-# LANGUAGE RankNTypes,
             OverloadedStrings,
             FlexibleContexts,
             RecursiveDo,
             NoImplicitPrelude,
             PartialTypeSignatures,
             LambdaCase,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Client where

import ClassyPrelude
import API
import Reflex
import Reflex.Dom
import Servant
import Servant.Reflex


createSubscription :: MonadWidget t m
                   => Dynamic t (Either Text CreateSubscriptionRequest)
                   -> Event t ()
                   -> m (Event t (ReqResult () ()))

testSubscription :: MonadWidget t m
                 => Dynamic t (Either Text CreateSubscriptionRequest)
                 -> Event t ()
                 -> m (Event t (ReqResult () SubscriptionResult))

getSubscriptions :: MonadWidget t m
                 => Event t ()
                 -> m (Event t (ReqResult () [SubscriptionWithCounts]))

getHnResults :: MonadWidget t m
             => Dynamic t (Either Text Int64)
             -> Dynamic t (Either Text Int64)
             -> Dynamic t (Either Text Int)
             -> Event t ()
             -> m (Event t (ReqResult () HnItems))

getRedditResults :: MonadWidget t m
                 => Dynamic t (Either Text Int64)
                 -> Dynamic t (Either Text UTCTime)
                 -> Dynamic t (Either Text Int)
                 -> Event t ()
                 -> m (Event t (ReqResult () [RedditCommOrLink]))

(createSubscription
  :<|> testSubscription
  :<|> getSubscriptions
  :<|> getHnResults
  :<|> getRedditResults
  ) = client (Proxy :: Proxy API)
            (Proxy)
            (Proxy :: Proxy ())
            (constDyn (BaseFullUrl Http "localhost" 8080 "api"))
