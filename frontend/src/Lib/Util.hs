{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib.Util where

import ClassyPrelude
import Control.Lens
import Control.Monad.IO.Class
import Control.Monad
import Data.JSString.Text
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Data.Text as Text
import Data.Tree
import GHCJS.Types
import Reflex
import Reflex.Dom
import qualified Text.HTML.Parser as Html
import qualified Text.HTML.Tree as Html
import Language.Javascript.JSaddle

decodeHtml :: Text -> JSM Text
decodeHtml txt = do
  ta <- jsg ("document"::JSString) ^. js1 ("createElement"::JSString)
                                          ("textarea"::JSString)
  ta ^. jss ("innerHTML"::JSString) txt
  res <- ta ^. js ("value"::JSString)
  fromJSValUnchecked res

insertMarks :: Text -> [(Int, Int)] -> Text
insertMarks txt tups = go txt (reverse tups)
  where
    go t [] = t
    go t ((i, l):rest) = go start rest ++ "<mark>" ++ h ++ "</mark>" ++ end
      where
        (startWithH, end) = Text.splitAt (i+l) t
        (start, h) = Text.splitAt i startWithH


truncateMarkedHtml :: Text -> Text
truncateMarkedHtml html =
    case tokenTree of
      Left _ -> toStrict $ Html.renderTokens tokens
      Right trees ->
        case trimForest (map processTree trees) of
          Nothing -> toStrict $ Html.renderTokens tokens
          Just remTokens ->
            let newForest :: Forest Html.Token
                newForest = map (map fst) remTokens
            in toStrict $ Html.renderTokens (concatMap Html.tokensFromTree newForest)
  where
    tokens = Html.parseTokens html
    tokenTree = Html.tokensToForest tokens
    processTree :: Tree Html.Token -> Tree (Html.Token, Int)
    processTree (Node v []) =
      case v of
        Html.ContentText t -> Node (v, length t) []
        Html.ContentChar _ -> Node (v, 1) []
        _ -> Node (v, 0) []
    processTree (Node v children) =
      case v of
        Html.ContentText txt -> Node (v, childSum + length txt) childResults
        Html.ContentChar ch  -> Node (v, childSum+1) childResults
        _                    -> Node (v, childSum) childResults
     where
       childResults = map processTree children
       childSum = sum $ map (\case
                                Node (_, s) _ -> s
                            ) childResults

trimToMark :: Tree (Html.Token, Int) -> Maybe (Tree (Html.Token, Int))
trimToMark (Node (v, s) []) = Nothing
trimToMark (Node (v, s) children) = do
    case v of
      Html.TagOpen tagname attrs ->
        if tagname == "mark"
        then Just $ Node (v,s) children
        else runChildren
      _ -> runChildren
  where
    runChildren = do mark <- trimForest children
                     return $ Node (v, s) mark

getCount :: Tree (Html.Token, Int) -> Int
getCount (Node (_,c) _) = c

trimForest :: [Tree (Html.Token, Int)] -> Maybe [Tree (Html.Token, Int)]
trimForest trees = go trees []
  where
    go (t:ts) rs =
      case trimToMark t of
        Just node@(Node (v, len) cs) -> Just $ expandMark node [] [] rs ts
        Nothing -> go ts (t:rs)
    go _ _ = Nothing

    expandMark :: Tree (Html.Token, Int)
               -> [Tree (Html.Token, Int)]
               -> [Tree (Html.Token, Int)]
               -> [Tree (Html.Token, Int)]
               -> [Tree (Html.Token, Int)]
               -> [Tree (Html.Token, Int)]
    expandMark node before after [] [] = before ++ [node] ++ reverse after
    expandMark node before after [] (r:rs)
        | beforeCount + afterCount + markCount > targetHtmlSize =
              before ++ [node] ++ reverse after
        | otherwise = expandMark node before (r:after) [] rs
      where
        beforeCount = sum $ map getCount before
        afterCount = sum $ map getCount after
        markCount = getCount node
    expandMark node before after (p:ps) []
        | beforeCount + afterCount + markCount > targetHtmlSize =
              before ++ [node] ++ reverse after
        | otherwise = expandMark node (p:before) after ps []
      where
        beforeCount = sum $ map getCount before
        afterCount = sum $ map getCount after
        markCount = getCount node
    expandMark node before after (p:ps) (r:rs)
        | beforeCount + afterCount + markCount > targetHtmlSize =
              before ++ [node] ++ reverse after
        | beforeCount > afterCount =
              expandMark node (p:before) after ps (r:rs)
        | otherwise =
              expandMark node before (r:after) (p:ps) rs
      where
        beforeCount = sum $ map getCount before
        afterCount = sum $ map getCount after
        markCount = getCount node

targetHtmlSize :: Int
targetHtmlSize = 240


