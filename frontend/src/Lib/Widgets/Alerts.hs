{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             RecursiveDo,
             PartialTypeSignatures,
             OverloadedStrings,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Alerts
  ( errorWidget
  , warningWidget
  ) where

import Control.Lens
import Control.Monad.IO.Class
import Data.Aeson
import qualified Data.Map as Map
import Data.Text hiding (count)
import Data.Time.Clock
import GHCJS.DOM
import GHCJS.DOM.Element
import GHCJS.DOM.Document hiding (select)
import GHCJS.DOM.Node
import GHCJS.DOM.Types hiding (Event, Text)
-- import JavaScript.Web.Location
import Reflex
import Reflex.Dom
import Reflex.Dom.Time
import Reflex.Host.Class

errorWidget :: MonadWidget t m => m () -> m ()
errorWidget inner =
  elAttr "div" (Map.fromList
                 [ ("class", "alert alert-danger")
                 , ("role", "alert")
                 ]) inner


warningWidget :: MonadWidget t m => m () -> m ()
warningWidget inner =
  elAttr "div" (Map.fromList
                 [ ("class", "alert alert-warning")
                 , ("role", "alert")
                 ]) inner
