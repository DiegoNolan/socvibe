{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             NoImplicitPrelude,
             OverloadedStrings,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Bootstrap where

import ClassyPrelude
import Control.Lens
import Data.Aeson
import qualified Data.Map as Map
import Reflex
import Reflex.Dom
import Reflex.Host.Class

btnSuccessDisabled :: MonadWidget t m => Dynamic t Bool -> Text -> m (Event t ())
btnSuccessDisabled isEnabled str = do
  let attrs = (\en ->
                 if en
                 then "class" =: "btn btn-success"
                 else "disabled" =: "true" <> "class" =: "btn btn-success"
              ) <$> isEnabled
  (e, _) <- elDynAttr' "button" attrs (text str)
  return $ domEvent Click e


btnPrimaryDisabled :: MonadWidget t m => Dynamic t Bool -> Text -> m (Event t ())
btnPrimaryDisabled isEnabled str = do
  let attrs = (\en ->
                 if en
                 then "class" =: "btn btn-primary"
                 else "disabled" =: "true" <> "class" =: "btn btn-primary"
              ) <$> isEnabled
  (e, _) <- elDynAttr' "button" attrs (text str)
  return $ domEvent Click e

btnPrimary :: MonadWidget t m => Text -> m (Event t ())
btnPrimary str = do
  (e, _) <- elClass' "button" "btn btn-primary" (text str)
  return $ domEvent Click e
