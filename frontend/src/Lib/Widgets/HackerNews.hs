{-# LANGUAGE RankNTypes,
             OverloadedStrings,
             FlexibleContexts,
             RecursiveDo,
             NoImplicitPrelude,
             PartialTypeSignatures,
             LambdaCase,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.HackerNews where

import ClassyPrelude
import API
import Control.Lens
import qualified Data.Map as Map
import qualified Data.Text as Text
import Reflex
import Reflex.Dom
import Language.Javascript.JSaddle
import Lib.Util
import Lib.Widgets.MatchRow

renderHNItems :: MonadWidget t m
              => SubscriptionType
              -> Text
              -> [Item]
              -> m ()
renderHNItems subType subText items = do
  text $ tshow (length items) ++ " instances found on Hacker News in the last day"
  forM_ items $ \item -> do
    let txt = fromMaybe "" (item^.itemText)
    matchRow subType subText txt
