{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             RecursiveDo,
             PartialTypeSignatures,
             OverloadedStrings,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Loader
  ( loader
  ) where

import Control.Lens
import Control.Monad.IO.Class
import Data.Aeson
import qualified Data.Map as Map
import Data.Text hiding (count)
import Data.Time.Clock
import GHCJS.DOM
import GHCJS.DOM.Element
import GHCJS.DOM.Document hiding (select)
import GHCJS.DOM.Node
import GHCJS.DOM.Types hiding (Event, Text)
-- import JavaScript.Web.Location
import Reflex
import Reflex.Dom
import Reflex.Dom.Time
import Reflex.Host.Class

loader :: MonadWidget t m => m ()
loader = do
  el "h3" (text "Loading")
  elClass "span" "fa fa-spinner fa-spin fa-3x fa-fw" (return ())
