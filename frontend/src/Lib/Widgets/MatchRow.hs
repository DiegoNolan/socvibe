{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE LambdaCase                #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE NoImplicitPrelude         #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE RecursiveDo               #-}
module Lib.Widgets.MatchRow where

import           API
import           ClassyPrelude
import           Control.Lens
import           Data.Either                 (fromRight)
import qualified Data.Map                    as Map
import qualified Data.Text                   as Text
import           Language.Javascript.JSaddle
import           Lib
import           Lib.Util
import           Reflex
import           Reflex.Dom

matchRow :: MonadWidget t m
         => SubscriptionType
         -> Text
         -> Text
         -> m ()
matchRow subType subText txt = do
  decoded <- liftJSM $ decodeHtml txt
  let idxs = fromRight [] (runSubscription subType subText decoded)
      withMarks = insertMarks decoded idxs
      truncated = truncateMarkedHtml withMarks
      isTruncated = length truncated /= length withMarks
      small = void $ elDynHtml' "div" $ constDyn truncated
      expanded = void $ elDynHtml' "div" $ constDyn withMarks
  elAttr "div" (  "class" =: "row"
               <> "style" =: "border: solid black 1px;"
               ) $ mdo
    elClass "div" "col-xs-11" $ do
      togDyn <- toggle True expEv
      widgetHold small ((\case
                            True -> small
                            False -> expanded
                        ) <$> updated togDyn)
    expEv <- elClass "div" "col-xs-1" $
      if isTruncated
      then button "expand"
      else return never
    return ()

