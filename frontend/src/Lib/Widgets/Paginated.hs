{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             LambdaCase,
             NoImplicitPrelude,
             OverloadedStrings,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Paginated where

import ClassyPrelude
import Control.Lens
import Reflex
import Reflex.Dom
import Reflex.Host.Class
import Servant.Reflex

data PageStatus a = PageStatus [a] a [a]

data NavStatus = NavStatus
  { canNext :: Bool
  , canPrev :: Bool
  }

data PageAction = Next | Prev

paginated :: MonadWidget t m
          => (Event t ()
             -> Maybe (PageStatus a, PageAction)
             -> m (Event t (PageStatus a))
             )
          -> (PageStatus a -> NavStatus)
          -> (a -> m ())
          -> m ()
paginated updateStatus calcNavStatus renderPage = mdo
    res <- widgetHold loadingWidget $
      (\case
          ps@(PageStatus before currentPage after) -> do

            renderPage currentPage

            let ns = calcNavStatus ps
            prevEv <- if canPrev ns
                      then do
                        prevEv <- button "Prev"
                        updateStatus prevEv (Just (ps, Prev))
                      else return never
            nextEv <- if canNext ns
                      then do
                        nextEv <- button "Next"
                        updateStatus nextEv (Just (ps, Next))
                      else return never
            return $ leftmost [ nextEv, prevEv ]
      ) <$> pageStatusEv
    let pageStatusEv = switchPromptlyDyn res
    return ()
  where
    loadingWidget = do
      text "loading"
      pb <- getPostBuild
      updateStatus pb Nothing
