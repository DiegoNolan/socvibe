{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             OverloadedStrings,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Popup
  ( popup
  ) where

import Control.Lens
import Data.Aeson
import qualified Data.Map as Map
import Data.Monoid
import Data.Text
import GHCJS.DOM
import GHCJS.DOM.Element
import GHCJS.DOM.Document hiding (select)
import GHCJS.DOM.Node
import GHCJS.DOM.Types hiding (Event, Text)
-- import JavaScript.Web.Location
import Reflex
import Reflex.Dom
import Reflex.Host.Class

popup :: MonadWidget t m => Int -> m a -> m (Event t (), a)
popup zIndex inner = do
  (bgEl, (overlayEl, innerRes)) <- elAttr' "div"
      ("style" =:
         ("position: fixed; top: 0; left: 0; right: 0; bottom: 0; " <>
          "z-index: " <> pack (show zIndex) <> "; " <>
          "background-color: rgba(255, 255, 255, 0.4); padding: 15px;" <>
          "text-align: center;")
      ) $
    elAttr' "div"
        ("style" =:
            ("background-color: white; display: inline-block; padding: 10px;" <>
             "text-align: left; z-index: " <> pack (show (zIndex + 1)) <> "; " <>
             "border-radius: 5px; border: solid 1px black; margin-top: 150px;")
        ) inner
  let outerClicked = domEvent Click bgEl
      inner = tag (constant False) $ domEvent Mouseenter overlayEl
      outer = tag (constant True) $ domEvent Mouseleave overlayEl
  isOut <- hold True $ leftmost [ inner, outer ]
  return ( gate isOut outerClicked
         , innerRes
         )


displayInt :: MonadWidget t m => Dynamic t Int -> m ()
displayInt = display
