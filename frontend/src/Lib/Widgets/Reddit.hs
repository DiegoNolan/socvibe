{-# LANGUAGE RankNTypes,
             OverloadedStrings,
             FlexibleContexts,
             RecursiveDo,
             NoImplicitPrelude,
             PartialTypeSignatures,
             LambdaCase,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Reddit where

import ClassyPrelude
import API
import Control.Lens
import qualified Data.Map as Map
import qualified Data.Text as Text
import Data.Time.Clock.POSIX
import Data.Time.Format.Human
import Reflex
import Reflex.Dom
import Language.Javascript.JSaddle
import Lib
import Lib.Util
import Lib.Widgets.Popup
import RedditTypes hiding (subType)
import Lib.Widgets.MatchRow

data CommentAndContext = CommentAndContext
  { ccSub  :: Text
  , ccPost :: Text
  , ccComm :: RedditComment
  } deriving Show

displayRedditCommentBody :: MonadWidget t m => Subscription -> Text -> m ()
displayRedditCommentBody sub = matchRow (sub^.subType) (sub^.subText)

displayCurrentComment :: MonadWidget t m
                      => Subscription
                      -> Maybe CommentAndContext
                      -> m (Event t ())
displayCurrentComment _ Nothing = return never
displayCurrentComment sub (Just CommentAndContext{..}) = do
  (closeEvent, _) <- popup 1000 $ do
    elAttr "div" ("style" =: "max-width: 700px; min-width: 400px; color: black; ") $ do
      elClass "div" "row" $ do
        elAttr "div" (  "style" =: "text-align: left"
                     <> "class" =: "col-xs-6" ) $ do
          elAttr "a" ("href" =: userLink (commentAuthor ccComm))
                     (text (commentAuthor ccComm))
          el "span" $ text "   "
          elClass "span" "text-muted" $
            text $ (tshow . commentScore $ ccComm) <> " points"
        elAttr "div" (Map.fromList [ ("style", "text-align: right")
                                   , ("class", "col-xs-6") ] ) $ do
          t <- liftIO . humanReadableTime $ commentTime ccComm
          elClass "span" "text-muted" (text $ pack t)
      elClass "div" "row" $ do
        elAttr "div" (  "class" =: "col-xs-12"
                     <> "style" =: "max-height: 400px; overflow-y: scroll;"
                     ) $ do
          displayRedditCommentBody sub (commentBody ccComm)
      elClass "div" "row" $ do
        elAttr "div" (  "style" =: "text-align: right"
                     <> "class" =: "col-xs-12") $
          elAttr "a" ("href" =: commentLink ccSub ccPost (commentId ccComm))
              (text "permalink")
  return closeEvent

displayPost :: MonadWidget t m
            => RedditLink
            -> m ()
displayPost post@RedditLink{..} = do

  elAttr "div" ("style" =: "max-width: 700px; min-width: 400px; color: black; ") $ do
    elClass "div" "row" $ do
      elAttr "div" (  "style" =: "text-align: left"
                   <> "class" =: "col-xs-6"
                   ) $ do
        elAttr "a" ("href" =: userLink linkAuthor) (text linkAuthor)
        el "span" $ text "   "
        elClass "span" "text-muted" $
          text $ pack (show linkScore ++ " points")
      elAttr "div" (  "style" =: "text-align: right"
                   <> "class" =: "col-xs-6"
                   ) $ do
        t <- liftIO . humanReadableTime $ linkTime
        elClass "span" "text-muted" (text (pack t))
    elClass "div" "row" $ do
      elAttr "div" (  "class" =: "col-xs-12"
                    <> "style" =: "max-height: 400px; overflow-y: scroll;"
                    ) $ do
        text linkTitle
    elClass "div" "row" $ do
      elAttr "div" (  "style" =: "text-align: right"
                   <> "class" =: "col-xs-12"
                   ) $
        elAttr "a" ("href" =: postLink linkSubreddit linkId) (text "permalink")
