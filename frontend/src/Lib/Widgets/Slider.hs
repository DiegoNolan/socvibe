{-# LANGUAGE RankNTypes,
             FlexibleContexts,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             OverloadedStrings,
             LambdaCase,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.Slider
  ( slider
  ) where

import Control.Lens
import Control.Monad.IO.Class
import Data.Aeson
import qualified Data.Map as Map
import Data.Monoid
import qualified Data.Text as Text
import GHCJS.DOM
import GHCJS.DOM.Element
import GHCJS.DOM.Document hiding (select)
import GHCJS.DOM.Node
import GHCJS.DOM.Types hiding (Event, Text)
import GHCJS.DOM.MouseEvent
import JavaScript.Web.Location
import Reflex
import Reflex.Dom
import Reflex.Host.Class

slider :: MonadWidget t m => m (Dynamic t Double)
slider = mdo
    (outerEl, (sliderEl, _)) <- elAttr' "div"
          ("style" =: "display: block; border: solid black 1px; position: relative; height: 50px;") $ do
      elDynAttr' "div" (mkAttrs <$> leftDyn <*> xDyn) (return ())
    let mouseDown = domEvent Mousedown sliderEl
        mouseUp = domEvent Mouseup sliderEl
        mouseMove = domEvent Mousemove outerEl

    onLoad <- getPostBuild
    loadDyn <- holdDyn () onLoad
    leftEvent <- dyn ((\_ -> getOffsetLeft (_el_element outerEl)) <$> loadDyn)
    -- leftEvent <- performEvent (\tag (getClientLeft (_el_element outerEl)) onLoad)
    {-
    width <- getOffsetWidth (_el_element outerEl)
    liftIO $ do print left
                print width -}
    leftDyn <- holdDyn 0 leftEvent
    -- display leftDyn
    let -- right = left + width
        clamp left x = max 0 (x - round left)
        mkAttrs left x = Map.fromList
          [ ("style", "display: block; width: 50px; height: 50px; background-color: red;" <>
                      "position: absolute; left: " <> Text.pack (show (clamp left x)) <> ";"
                      )
          ]

    currAndPrevX <- foldDyn (\(x,_) (x',_) -> (x, x')) (0,0) mouseMove
    let xDiff = (\(c,p) -> c - p) <$> currAndPrevX
    xDyn <- foldDyn (+) 0 (updated xDiff)
    display currAndPrevX
    return (constDyn 0)
