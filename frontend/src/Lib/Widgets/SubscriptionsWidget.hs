{-# LANGUAGE RankNTypes,
             OverloadedStrings,
             FlexibleContexts,
             RecursiveDo,
             NoImplicitPrelude,
             PartialTypeSignatures,
             GADTs,
             LambdaCase,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.Widgets.SubscriptionsWidget where

import ClassyPrelude
import API
import Control.Lens
import qualified Data.Map as Map
import Data.List.Split (chunksOf)
import qualified Data.Text as Text
import Data.Time.Clock
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import Reflex
import Reflex.Dom
import Servant.Reflex
import qualified Lib.Client as Client
import Lib.Util
import Lib.Widgets.HackerNews
import Lib.Widgets.Reddit
import Lib.Widgets.Paginated
import Lib.Widgets.Popup
import RedditTypes hiding (subId, subType)

subscriptionsWidget :: MonadWidget t m
                    => [SubscriptionWithCounts]
                    -> m (Event t Int64)
subscriptionsWidget swcs = do
  evs <- elAttr "table" (  "class" =: "table table-condensed"
                        ) $ do
    el "thead" $ do
      el "tr" $ do
        el "th" $ text "Type"
        el "th" $ text "Search String"
        el "th" $ text "Hacker News"
        el "th" $ text "Reddit"

    el "tbody" $ do
      forM (zip [0..] swcs) $ \(idx, swc) -> do
        let hnId = "hackernews" ++ tshow idx
            redId = "reddit" ++ tshow idx
            sub = swc^.swc_subscription
        viewResultsEv <- el "tr" $ mdo
          el "td" (text (tshow (sub^.subType)))
          el "td" (text (sub^.subText))

          el "td" $ do
            el "span" (text (tshow (swc^.swc_hnCounts) ++
                            " results in the last day"))
          el "td" $ do
            el "span" (text (tshow (swc^.swc_redditCounts) ++
                            " results in the last day"))

          el "td" $ do
            (e, _) <- el' "a" (text "View Results")
            return $ domEvent Click e

        return $ sub^.subId <$ viewResultsEv
  return $ leftmost evs

displaySubscriptionWidget :: MonadWidget t m => Subscription -> m ()
displaySubscriptionWidget sub = mdo
    hnRes <- dyn $ (\case
                       Just HackerNews -> return never
                       _ -> button "Hacker News"
                   ) <$> selectedSite
    rdRes <- dyn $ (\case
                       Just Reddit -> return never
                       _ -> button "Reddit"
                   ) <$> selectedSite
    hnEvent <- switchHold never hnRes
    redditEvent <- switchHold never rdRes
    selectedSite <- holdDyn initialSite $ leftmost [ Just HackerNews <$ hnEvent
                                                   , Just Reddit <$ redditEvent
                                                   ]
    display selectedSite
    elDynAttr "div" (mkAttrs HackerNews <$> selectedSite) $
      paginatedHackerNews sub
    elDynAttr "div" (mkAttrs Reddit <$> selectedSite) $
      paginatedReddit sub

    return ()
  where
    mkAttrs site selSite =
      if Just site == selSite
      then Map.empty
      else "style" =: "display: none"
    subs = sub^.subSites
    initialSite = headMay subs

paginatedHackerNews :: MonadWidget t m
                    => Subscription
                    -> m ()
paginatedHackerNews sub = paginated updateStatus calcNavStatus renderPage
  where
    renderPage items = renderHNItems (sub^.subType) (sub^.subText) items
    calcNavStatus (PageStatus before reddits after) =
      NavStatus
        { canNext = not (length reddits < pageSize)
        , canPrev = not (null before)
        }
    updateStatus ev Nothing = do
      farInTheFuture <- addUTCTime (86400*30) <$> liftIO getCurrentTime
      let asEpochUnix = round (realToFrac (utcTimeToPOSIXSeconds farInTheFuture))
      rqRes <- getResults asEpochUnix ev
      return $ fmapMaybe
          (\case
              ResponseSuccess _ (HnItems items) _ ->
                let (page, rest) = splitAt pageSize items
                in Just (PageStatus [] page (chunksOf pageSize rest))
              _ -> Nothing
          ) rqRes
    updateStatus ev (Just (PageStatus before page after, Prev)) =
      case before of
        (b:bs) -> return $ PageStatus bs b (page : after) <$ ev
        _      -> return $ PageStatus before page after <$ ev
    updateStatus ev (Just (PageStatus before page after, Next)) = do
      case after of
        (a:as) -> return $ PageStatus (page : before) a as <$ ev
        _ -> do
          rq <- getResults (fromMaybe 0 $ lastEx page ^. itemTime) ev
          return $ fmapMaybe
              (\case
                  ResponseSuccess _ (HnItems ni) _ ->
                    let (page, rest) = splitAt pageSize ni
                    in Just (PageStatus (page : before) page (chunksOf pageSize rest))
                  _ -> Nothing
              ) rq
    getResults t ev =
      Client.getHnResults (constDyn (Right (sub^.subId)))
                                   (constDyn (Right t))
                                   (constDyn (Right limToFetch)) ev
    limToFetch = 100
    pageSize = 10

paginatedReddit :: MonadWidget t m => Subscription -> m ()
paginatedReddit sub = paginated updateStatus calcNavStatus renderPage
  where
    renderPage reddits =
      forM_ reddits $ \case
        RC rc -> displayRedditCommentBody sub (commentBody rc)
        RL rl -> displayPost rl
    calcNavStatus (PageStatus before reddits after) =
      NavStatus
        { canNext = not (length reddits < pageSize)
        , canPrev = not (null before)
        }
    updateStatus ev Nothing = do
      farInTheFuture <- addUTCTime (86400*30) <$> liftIO getCurrentTime
      resp <- getResults farInTheFuture ev
      return $ fmapMaybe
          (\case
              ResponseSuccess _ nr _ ->
                let (page, rest) = splitAt pageSize nr
                in Just (PageStatus [] page (chunksOf pageSize rest))
              _ -> Nothing
          ) resp
    updateStatus ev (Just (PageStatus before page after, Prev)) =
      case before of
        (b:bs) -> return $ PageStatus bs b (page : after) <$ ev
        _      -> return $ PageStatus before page after <$ ev
    updateStatus ev (Just (PageStatus before page after, Next)) = do
      case after of
        (a:as) -> return $ PageStatus (page : before) a as <$ ev
        _ -> do
          rq <- getResults (getRedditCreatedUtc (lastEx page)) ev
          return $ fmapMaybe
              (\case
                  ResponseSuccess _ nr _ ->
                    let (page, rest) = splitAt pageSize nr
                    in Just (PageStatus (page : before) page (chunksOf pageSize rest))
                  _ -> Nothing
              ) rq
    getResults begin ev =
      Client.getRedditResults (constDyn (Right (sub^.subId)))
                              (constDyn (Right begin))
                              (constDyn (Right limToFetch)) ev
    limToFetch = 100
    pageSize = 10


