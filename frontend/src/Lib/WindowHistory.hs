{-# LANGUAGE RankNTypes,
             NoImplicitPrelude,
             FlexibleContexts,
             RecursiveDo,
             PartialTypeSignatures,
             MultiWayIf,
             RecordWildCards,
             NoMonomorphismRestriction #-}
module Lib.WindowHistory
  (
  ) where

import ClassyPrelude hiding (Element)
import GHCJS.DOM
import GHCJS.DOM.Element
import GHCJS.DOM.Document hiding (select)
import GHCJS.DOM.Node
import GHCJS.DOM.Types hiding (Event, Text, Rect)
import GHCJS.Types

