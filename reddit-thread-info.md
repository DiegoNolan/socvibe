

# How do I interpret this visualization?

The left side of the visualization represents the time the Reddit post was originally created. Each rectangle
represents a comment on the reddit post. The width of the rectangle represents how long the user took
to respons to either the original post or the parent comment. So comments that were posted very
shortly after their parent are very short while comments that are posted well after their parent are
long. Since some comments are posted within seconds while others are posted within days it sometimes
helps to display the length of the comments in a logrithimic scale so they can fit within the screen.

All comments with no replies have the same unit height. A comment with 2 replies has 2 unit heights. A
comment with 3 replies has 3 unit heights. A comment with 2 comments with 2 replies each has a unit
height of 4. With this structure the height of the comment represents how many different discussions
a parent comment has created. A very long comment chain represents a comment that has sparked a very
long discussion.

When the score color option is selected green comments are highly upvoted comments while low score
comments are red. For the depth option comments that are close to the parent post are red while comments
further from the parent post are purple. The user option gives each user commenting their own color.
This option usually isn't that useful for threads with more than 30 comments because the colors
become too hard to distinguish.

You can click on the comments rectangles and a modal box will appear with the comment the name
of the Reddit user who posted it and a permalink to the comment on Reddit.

## Examples

It is usually really easy to see particularly unliked comments. With the score color option
they will appear dark red because they are highly downvoted compared to the other comments in the
reddit thread.

![A really bad Reddit comment.](/images/bad.png)

Certain threads have different properties. Ask Me Anything threads are interesting. For the most part
the comments closer to the parent post have higher scores while older comments have lower probably
because move people view them and then can upvote them. AMAs, however, usually always upvote the OPs
comments so everyone can see them. This causes replies to have a higher score than their parents.

![A Reddit AMA thread.](/images/ama.png)

The vizualization also let's you see comments that sparked a lot of discussion. Here is an emample of a 
top level comment that has created a ton of replies.

![A Reddit comment sparking a lot of discussion.](/images/spark.png)

## Conclusion

This layout allows you visualize the comment layout from a higher level and maybe find some interesting
comments you wouldn't find just by browsing the comments normally.
