CREATE TABLE IF NOT EXISTS users (
id bigserial PRIMARY KEY NOT NULL,
created_utc TIMESTAMPTZ NOT NULL
);

-- Crawler State
CREATE TABLE IF NOT EXISTS crawler_host_state (
       host_name text PRIMARY KEY NOT NULL,
       crawler_state json
);

-- Hacker News
CREATE TABLE IF NOT EXISTS hn_item (
       id bigint PRIMARY KEY NOT NULL,
       deleted BOOLEAN,
       type INTEGER NOT NULL,
       by TEXT,
       time BIGINT,
       text TEXT,
       dead BOOLEAN,
       parent INT REFERENCES hn_item (id),
       url TEXT,
       score INT,
       title TEXT,
       modified_utc TIMESTAMPTZ NOT NULL,
       created_utc TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS hn_item_lock (
       id INTEGER PRIMARY KEY NOT NULL,
       created_utc TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS subscription (
       id bigserial PRIMARY KEY NOT NULL,
       user_id bigint NOT NULL references users (id),
       sites text[] NOT NULL,
       sub_type text NOT NULL,
       sub_text text NOT NULL,
       created_utc TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS hn_subscription_match (
       sub_id bigint NOT NULL references subscription (id),
       hn_item_id bigint NOT NULL references hn_item (id),
       PRIMARY KEY (sub_id, hn_item_id)
);

CREATE TABLE IF NOT EXISTS hn_item_match_last_time (
       lasttime bigint NOT NULL
);

-- Reddit
CREATE TABLE IF NOT EXISTS reddit_user (
       id text PRIMARY KEY,
       modifiedUtc timestamptz NOT NULL
);

CREATE TABLE IF NOT EXISTS reddit_subreddit (
        id text PRIMARY KEY,
        title text NOT NULL,
        name text NOT NULL,
        displayName text NOT NULL,
        url text NOT NULL,
        description text NOT NULL,
        subscribers integer NOT NULL,
        subredditType text NOT NULL,
        over18 boolean NOT NULL,
        publicTraffic boolean NOT NULL,
        submissionType text NOT NULL,
        submitText text NOT NULL,
        createdUtc timestamptz NOT NULL,
        modifiedUtc timestamptz NOT NULL
);

CREATE TABLE IF NOT EXISTS reddit_link (
       id text PRIMARY KEY,
       name text NOT NULL,
       title text NOT NULL,
       ups integer NOT NULL,
       downs integer NOT NULL,
       score integer NOT NULL,
       gilded integer NOT NULL,
       url text NOT NULL,
       domain text NOT NULL,
       permalink text NOT NULL,
       hidden boolean NOT NULL,
       isself boolean NOT NULL,
       numcomments integer NOT NULL,
       over18 boolean NOT NULL,
       selftext text NOT NULL,
       selftexthtml text,
       stickied boolean NOT NULL,
       thumbnail text NOT NULL,
       edited timestamptz,
       userid text NOT NULL references reddit_user (id),
       subredditid text NOT NULL references reddit_subreddit (id),
       createutc timestamptz NOT NULL,
       modifiedutc timestamptz NOT NULL
);

CREATE TABLE IF NOT EXISTS reddit_comment (
       id text PRIMARY KEY,
       name text NOT NULL,
       ups integer NOT NULL,
       downs integer NOT NULL,
       score integer NOT NULL,
       gilded integer NOT NULL,
       controversiality real NOT NULL,
       body text NOT NULL,
       bodyhtml text NOT NULL,
       edited timestamptz,
       scorehidden boolean NOT NULL,
       userid text NOT NULL references reddit_user (id),
       linkid text NOT NULL references reddit_link (id) on delete cascade,
       parentcomment text references reddit_comment (id) on delete cascade,
       subredditid text not null references reddit_subreddit (id) on delete cascade,
       createutc timestamptz NOT NULL,
       modifiedutc timestamptz NOT NULL
);

CREATE TABLE IF NOT EXISTS reddit_link_match (
       sub_id bigint NOT NULL references subscription (id),
       reddit_link_id text NOT NULL references reddit_link (id),
       PRIMARY KEY (sub_id, reddit_link_id)
);

CREATE TABLE IF NOT EXISTS reddit_comment_match (
       sub_id bigint NOT NULL references subscription (id),
       reddit_comment_id text NOT NULL references reddit_comment (id),
       PRIMARY KEY (sub_id, reddit_comment_id)
);
